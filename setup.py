#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name="vispa_augerofflineeventbrowser",
    version="0.0.1",
    description="VISPA Auger Event Browser - Inspect ADST files",
    author="VISPA Project",
    author_email="vispa@lists.rwth-aachen.de",
    url="http://vispa.physik.rwth-aachen.de/",
    license="GNU GPL v2",
    packages=find_packages(),
    package_dir={"vispa_augerofflineeventbrowser": "vispa_augerofflineeventbrowser"},
    package_data={"vispa_augerofflineeventbrowser": [
        "workspace/*",
        "static/css/*",
        "static/js/*",
        "static/html/*",
        "static/vendor/*",
    ]},
    # install_requires = ["vispa"],
)
