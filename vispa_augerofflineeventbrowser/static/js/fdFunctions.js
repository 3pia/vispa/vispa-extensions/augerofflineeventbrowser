require.config({
  paths: {
    "mpld3": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/mpld3.v0.3"),
    "augeroffline_d3": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/d3.v3.min"),
    "gridstack": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/gridstack/gridstack"),
    "lodash": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/lodash/lodash"),
    },
  shim: {
    gridstack: ["jquery", "lodash"],
    mpld3: {
      exports: "mpld3"
    }
  }
});
define([ "jclass",
  "async",
  "jquery",
  "augeroffline_d3",
  "text!../html/fd_time_fit.html",
  "text!../html/fd_event_info.html",
  "text!../html/fd_shower_profile.html",
  "text!../html/fd_camera.html",
  "text!../html/fd_pixel.html",
  "gridstack",
  "css!./../css/augerofflineeventbrowser",
  "css!./../vendor/gridstack/gridstack.css" ], function(jClass, async, $, d3,fdTimeFitTemplate,fdEventInfoTemplate,fdShowerProfileTemplate,fdCameraTemplate,fdPixelTemplate) {
  var fdFunctions = jClass._extend({
    init: function init(view) {
      var self = this;
      init._super.call(this, arguments);
      self.view = view;
    },

    addFdTimeFit: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].fdTimeFitTile==null){
          self.view.gridstackNodes[tab].data("gridstack").add_widget(fdTimeFitTemplate,x,y,width,height);
          self.view.tabs[tab].fdTimeFitTile=$(".fd-time-fit-tile",self.view.tabs[tab]);
          self.drawFdTimeFit(tab);
      }else{
        console.log("fdtimefit already exists");
      }
    },

    drawFdTimeFit: function (tab) {
      var self = this;
      $(".fd-time-fit-time",self.view.tabs[tab].fdTimeFitTile).prop("name",tab+"_fd-time-fit-color-code"+self.view.id);
      $(".fd-time-fit-charge",self.view.tabs[tab].fdTimeFitTile).prop("name",tab+"_fd-time-fit-color-code"+self.view.id);
      $(".remove-fd-time-fit",self.view.tabs[tab]).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].fdTimeFitTile);
        self.view.tabs[tab].fdTimeFitTile=null;
      });
      $(".save-plot",self.view.tabs[tab].fdTimeFitTile).click(function(){
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].fdTimeFitTile);
        var plotWidth=plotSize[0];
        var plotHeight=plotSize[1];
        self.view.savePlot("fd_time_fit",self.view.tabs[tab].fdTimeFitTile,plotWidth,plotHeight);
      });
      $(".fd-time-fit-plot",self.view.tabs[tab]).attr("id",tab+"_fdTimeFitPlot"+self.view.id);
      self.view.tabs[tab].fdTimeFitTile.on("resizestop",function(){
        self.updateFdTimeFit(tab,false,false);
      });
      $(".fd-time-fit-color-code",self.view.tabs[tab].fdTimeFitTile).click(function(){
        self.updateFdTimeFit(tab,false);
      });
      $(".fd-time-fit-residuals",self.view.tabs[tab]).click(function(){
        self.updateFdTimeFit(tab,true,false);
      });
      self.updateFdTimeFit(tab,false,true);
    },

    updateFdTimeFit: function(tab,changeZoom,changeEye) {
      var self = this;
      self.view.tabs[tab].fdTimeFitTile.data("changeZoom",changeZoom);
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].fdTimeFitTile);
      var plotWidth = plotSize[0];
      var plotHeight = plotSize[1];
      var eye;
      if (changeEye == true) {
        eye = -1;
      } else {
        if ($("#select-eye", self.view.tabs[tab]) == null) {
          eye = -1;
        } else {
          eye = $("#select-eye", self.view.tabs[tab]).val();
        }
      }
      $(".fd-time-fit-eye",self.view.tabs[tab].fdTimeFitTile).attr("data-value",eye);
      if ((plotWidth > 100) && (plotHeight > 100)) {
        $(".panel-overlay", self.view.tabs[tab].fdTimeFitTile).show();
        self.view.GET("get_plot", JSON.stringify({
          "plot_name": "fd_time_fit",
          "plot_height": plotHeight,
          "plot_width": plotWidth,
          "eye": eye,
          "draw_charge": $(".fd-time-fit-charge", self.view.tabs[tab]).prop("checked"),
          "draw_residuals": $(".fd-time-fit-residuals", self.view.tabs[tab]).prop("checked"),
          "zoom_level": null,
          "tab": tab
        }));
      }
    },

    renderFdTimeFit: function(tab,data){
      var self=this;
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].fdTimeFitTile);
      var plotWidth=plotSize[0];
      var plotHeight=plotSize[1];
      var changeZoom=self.view.tabs[tab].fdTimeFitTile.data("changeZoom");
      var zoomLevel=self.view.getZoomLevel(self.view.tabs[tab].fdTimeFitTile,changeZoom);
      if(data!=null){
        d3.select("#"+tab+"_fdTimeFitPlot"+self.view.id).selectAll("*").remove();
        var commands=data;
        commands["height"]=plotHeight;
        commands["width"]=plotWidth;
        !function(mpld3) {
          mpld3.register_plugin("identifyStations", identifyStations);
          identifyStations.prototype = Object.create(mpld3.Plugin.prototype);
          identifyStations.prototype.constructor = identifyStations;
          identifyStations.requiredProps=["id","station_numbers"];
          function identifyStations(fig, props) {
            mpld3.Plugin.call(this, fig, props);
          }
          identifyStations.prototype.draw = function () {
            var obj=mpld3.get_element(this.props.id[0]);
            var station_ids=this.props.id[1];
            obj.elements().on("mousedown",function(d,i){
              self.selectPixel(tab,station_ids[i])
            });
          };
        }(mpld3);
        self.view.tabs[tab].fdTimeFitTile.graph=mpld3.draw_figure(tab+"_fdTimeFitPlot"+self.view.id,commands);
        if(zoomLevel!=null){
          self.view.tabs[tab].fdTimeFitTile.graph.axes[0].set_axlim([zoomLevel[0],zoomLevel[1]],[zoomLevel[2],zoomLevel[3]],0,false)
        }
      }else{
        $(".fd-time-fit-plot",self.view.tabs[tab]).html("<h3>No Data to display</h3>");
      }
      $(".panel-overlay",self.view.tabs[tab].fdTimeFitTile).hide();
    },

    addFdEventInfo: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].fdEventInfoTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(fdEventInfoTemplate,x,y,width,height);
        self.view.tabs[tab].fdEventInfoTile=$(".fd-event-info-tile",self.view.tabs[tab]);
        self.drawFdEventInfo(tab);
      }else{
        console.log("fdeventinfo already exists");
      }
    },

    drawFdEventInfo: function(tab){
      var self=this;
      $(".remove-fd-event-info",self.view.tabs[tab]).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].fdEventInfoTile);
        self.view.tabs[tab].fdEventInfoTile=null;
      });
      self.updateFdEventInfo(tab,-1);
      self.updateFdEyeSelector(tab,false);
    },

    updateFdEventInfo: function(tab,eye) {
      var self = this;
      $(".panel-overlay", self.view.tabs[tab].fdEventInfoTile).show();
      self.view.GET("get_fd_event_info", JSON.stringify({
        "eye": eye,
        "tab": tab
      }));
    },

    renderFdEventInfo: function(tab,res){
      var self=this;
      if(res!=null) {
        var tableDirectives = {
          "fd-event-run": {
            text: function () {
              return this.runId;
            }
          },
          "fd-event-id": {
            text: function () {
              return this.eventId;
            }
          },
          "fd-utc-time": {
            text: function () {
              return this.utcDate
            }
          },
          "fd-gps-time": {
            text: function () {
              return this.gpsSecond + " s, " + this.gpsNanoSecond + " ns"
            }
          },
          "fd-energy": {
            text: function () {
              return Number(this.energy.toPrecision(4)).toExponential() + " +/- " + Number(this.energyError.toPrecision(4)).toExponential();
            }
          },
          "fd-azimuth": {
            text: function () {
              return this.azimuth + " +/- " + this.azimuthError;
            }
          },
          "fd-zenith": {
            text: function () {
              return this.zenith + " +/- " + this.zenithError;
            }
          },
          "fd-core-x": {
            text: function () {
              return this.coreX;
            }
          },
          "fd-core-y": {
            text: function () {
              return this.coreY;
            }
          },
          "fd-event-trigger": {
            text: function () {
              return this.eventType + " , " + this.eventClass;
            }
          },
          "fd-x-max": {
            text: function () {
              return this.xMax + " +/- " + this.xMaxError;
            }
          },
          "fd-dE-dX": {
            text: function () {
              return this.dEdXMax + " +/- " + this.dEdXMaxError;
            }
          },
          "fd-shower-profile":{
            text: function() {
              return this.showerProfile;
            }
          }
        };
        $(".fd-event-info-table-body", self.view.tabs[tab]).render(res, tableDirectives);
      }else{
        var directive={
          "fd-event-run": {
            text: function(){
              return "No FD data in event";
            }
          }
        };
        $(".fd-event-info-table-body",self.view.tabs[tab]).render({},directive);
      }
      $(".panel-overlay",self.view.tabs[tab].fdEventInfoTile).hide();
    },

    updateFdEyeSelector:function(tab) {
      var self = this;
      self.view.GET("get_fd_eyes", function (err, res) {
        self.renderFdEyeSelector(tab,res);
      });
    },

    renderFdEyeSelector: function(tab,res){var self=this;
      var selector=$("#select-eye",self.view.tabs[tab]);
      selector.empty();
      if(res.length>0){
        for(var i= 0;i<res.length;i++){
          var newItem=$("<option class='eye-option'></option>").html(res[i].name).val(res[i].id);
          newItem.value=res[i].id;
          selector.append(newItem);
        }
      }else{
        selector.prop("title","No FD Eyes");
      }
      selector.selectpicker("refresh");
      selector.unbind().change(function(){
        selector.selectpicker("refresh");
        self.updateFdEventInfo(tab, selector.val());
        if(self.view.tabs[tab].fdShowerProfileTile!=null) {
          self.updateFdShowerProfile(tab,true,false);
        }
        if(self.view.tabs[tab].fdCameraTile!=null){
          self.updateFdCamera(tab,true,false);
        }
        if(self.view.tabs[tab].fdTimeFitTile!=null) {
          self.updateFdTimeFit(tab, true,false);
        }
        if(self.view.tabs[tab].fdPixelHistogramTile!=null) {
          self.updateFdPixelHisto(tab, -1, true,false);
        }
      });
    },

    addFdShowerProfile: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].fdShowerProfileTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(fdShowerProfileTemplate,x,y,width,height);
        self.view.tabs[tab].fdShowerProfileTile=$(".fd-shower-profile-tile",self.view.tabs[tab]);
        self.drawFdShowerProfile(tab);
      }else{
        console.log("fdshowerprofile already exists");
      }
    },

    drawFdShowerProfile: function(tab){
      var self=this;
      $(".fd-shower-profile-depth",self.view.tabs[tab].fdShowerProfileTile).prop("name",tab+"_fd-shower-profile-x"+self.view.id);
      $(".fd-shower-profile-age",self.view.tabs[tab].fdShowerProfileTile).prop("name",tab+"_fd-shower-profile-x"+self.view.id);
      $(".fd-shower-profile-deposit",self.view.tabs[tab].fdShowerProfileTile).prop("name",tab+"_fd-shower-profile-y"+self.view.id);
      $(".fd-shower-profile-electrons",self.view.tabs[tab].fdShowerProfileTile).prop("name",tab+"_fd-shower-profile-y"+self.view.id);
      $(".remove-fd-shower-profile",self.view.tabs[tab]).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].fdShowerProfileTile);
        self.view.tabs[tab].fdShowerProfileTile=null;
      });
      $(".save-plot",self.view.tabs[tab].fdShowerProfileTile).click(function(){
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].fdShowerProfileTile);
        var plotWidth = plotSize[0];
        var plotHeight = plotSize[1];
        self.view.savePlot("fd_shower_profile",self.view.tabs[tab].fdShowerProfileTile,plotWidth,plotHeight);
      });
      self.view.tabs[tab].fdShowerProfileTile.on("resizestop",function(){
        self.updateFdShowerProfile(tab,false,false);
      });
      $(".fd-shower-profile-option",self.view.tabs[tab].fdShowerProfileTile).click(function(){
        self.updateFdShowerProfile(tab,true,false);
      });
      $(".fd-shower-profile-plot",self.view.tabs[tab]).prop("id",tab+"_fdShowerProfile"+self.view.id);
      self.updateFdShowerProfile(tab,false,true);
    },

    updateFdShowerProfile: function(tab,changeZoom,changeEye) {
      var self = this;
      self.view.tabs[tab].fdShowerProfileTile.data("changeZoom",changeZoom);
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].fdShowerProfileTile);
      var plotWidth = plotSize[0];
      var plotHeight = plotSize[1];
      var eye;
      if (changeEye == true) {
        eye = -1;
      } else {
        if ($("#select-eye", self.view.tabs[tab]) == null) {
          eye = -1;
        } else {
          eye = $("#select-eye", self.view.tabs[tab]).val();
        }
      }
      $(".fd-shower-profile-eye",self.view.tabs[tab].fdShowerProfileTile).attr("data-value",eye);
      if ((plotWidth > 100) && (plotHeight > 100)) {
        $(".panel-overlay", self.view.tabs[tab].fdShowerProfileTile).show();
        self.view.GET("get_plot", JSON.stringify({
          "plot_name": "fd_shower_profile",
          "plot_width": plotWidth,
          "plot_height": plotHeight,
          "eye": eye,
          "use_shower_age": $(".fd-shower-profile-age", self.view.tabs[tab]).prop("checked"),
          "use_n_e": $(".fd-shower-profile-electrons", self.view.tabs[tab]).prop("checked"),
          "zoom_level": null,
          "tab": tab
        }));
      }
    },

    renderFdShowerProfile: function(tab,data){
      var self=this;
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].fdShowerProfileTile);
      var plotWidth=plotSize[0];
      var plotHeight=plotSize[1];
      var changeZoom=self.view.tabs[tab].fdShowerProfileTile.data("changeZoom");
      var zoomLevel=self.view.getZoomLevel(self.view.tabs[tab].fdShowerProfileTile,changeZoom);
      if(data!=null){
        d3.select("#"+tab+"_fdShowerProfile"+self.view.id).selectAll("*").remove();
        var commands=data;
        commands["height"]=plotHeight;
        commands["width"]=plotWidth;
        self.view.tabs[tab].fdShowerProfileTile.graph=mpld3.draw_figure(tab+"_fdShowerProfile"+self.view.id,commands);
        if(zoomLevel!=null) {
          self.view.tabs[tab].fdShowerProfileTile.graph.axes[0].set_axlim([zoomLevel[0], zoomLevel[1]], [zoomLevel[2], zoomLevel[3]],0,false);
        }
      }else{
        $(".fd-shower-profile-plot",self.view.tabs[tab]).html("<h3>No Data to Display</h3>")
      }
      $(".panel-overlay",self.view.tabs[tab].fdShowerProfileTile).hide();
    },

    addFdCamera: function(tab,x,y,width,height){var self=this;if(self.view.tabs[tab].fdCameraTile==null){
      self.view.gridstackNodes[tab].data("gridstack").add_widget(fdCameraTemplate,x,y,width,height);
      self.view.tabs[tab].fdCameraTile=$(".fd-camera-tile",self.view.tabs[tab]);
      self.drawFdCamera(tab)
      }else{
        console.log("fdCamera already exists");
      }
    },

    drawFdCamera: function(tab){
      var self=this;
      $(".fd-camera-charge",self.view.tabs[tab].fdCameraTile).prop("name",tab+"fd-camera-color-code"+self.view.id);
      $(".fd-camera-timing",self.view.tabs[tab].fdCameraTile).prop("name",tab+"fd-camera-color-code"+self.view.id);
      $(".remove-fd-camera",self.view.tabs[tab]).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].fdCameraTile);
        self.view.tabs[tab].fdCameraTile=null;
      });
      $(".save-plot",self.view.tabs[tab].fdCameraTile).click(function(){
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].fdCameraTile);
        var plotWidth=plotSize[0];
        var plotHeight=plotSize[1];
        self.view.savePlot("fd_camera",self.view.tabs[tab].fdCameraTile,plotWidth,plotHeight);
      });
      self.view.tabs[tab].fdCameraTile.on("resizestop",function(){
        self.updateFdCamera(tab,false,false);
      });
      $(".fd-camera-plot",self.view.tabs[tab]).prop("id",tab+"_fdCameraPlot"+self.view.id);
      $(".fd-camera-drawing-option",self.view.tabs[tab].fdCameraTile).click(function(){
        self.updateFdCamera(tab,false,false);
      });
      self.updateFdCamera(tab,false,true);
    },

    updateFdCamera: function(tab,changeZoom,newEye) {
      var self = this;
      self.view.tabs[tab].fdCameraTile.data("changeZoom",changeZoom);
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].fdCameraTile);
      var plotWidth = plotSize[0];
      var plotHeight = plotSize[1];
      var eye;
      if (newEye == true) {
        eye = -1
      } else {
        if ($("#select-eye", self.view.tabs[tab]) == null) {
          eye = -1;
        } else {
          eye = $("#select-eye", self.view.tabs[tab]).val();
        }
      }
      $(".fd-camera-eye",self.view.tabs[tab].fdCameraTile).attr("data-value",eye);
      if ((plotHeight > 100) && (plotWidth > 100)) {
        $(".panel-overlay", self.view.tabs[tab].fdCameraTile).show();
        self.view.GET("get_plot", JSON.stringify({
          "plot_name": "fd_camera",
          "plot_width": plotWidth,
          "plot_height": plotHeight,
          "eye": eye,
          "draw_charge": $(".fd-camera-charge", self.view.tabs[tab]).prop("checked"),
          "draw_all_pixels": $(".fd-camera-all-pixels", self.view.tabs[tab]).prop("checked"),
          "zoom_level": null,
          "tab": tab
        }));
      }
    },

    renderFdCamera: function(tab,data){
      var self=this;
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].fdCameraTile);
      var plotWidth=plotSize[0];
      var plotHeight=plotSize[1];
      var changeZoom=self.view.tabs[tab].fdCameraTile.data("changeZoom");
      var zoomLevel=self.view.getZoomLevel(self.view.tabs[tab].fdCameraTile,changeZoom);
      if(data!=null){
        d3.select("#"+tab+"_fdCameraPlot"+self.view.id).selectAll("*").remove();
        var commands=data;
        commands["height"]=plotHeight;
        commands["width"]=plotWidth;
        !function(mpld3) {
          mpld3.register_plugin("identifyStations", identifyStations);
          identifyStations.prototype = Object.create(mpld3.Plugin.prototype);
          identifyStations.prototype.constructor = identifyStations;
          identifyStations.requiredProps=["id","station_numbers"];
          function identifyStations(fig, props) {
            mpld3.Plugin.call(this, fig, props);
          }
          identifyStations.prototype.draw = function () {
            var obj=mpld3.get_element(this.props.id[0]);
            var station_ids=this.props.id[1];
            obj.elements().on("mousedown",function(d,i){
              self.selectPixel(tab,station_ids[i])
            });
          };
        }(mpld3);
        self.view.tabs[tab].fdCameraTile.graph=mpld3.draw_figure(tab+"_fdCameraPlot"+self.view.id,commands);
        if(zoomLevel!=null){
          self.view.tabs[tab].fdCameraTile.graph.axes[0].set_axlim([zoomLevel[0],zoomLevel[1]],[zoomLevel[2],zoomLevel[3]],0,false);
        }
        $(".panel-overlay",self.view.tabs[tab].fdCameraTile).hide();
      }else{
        $(".fd-camera-plot",self.view.tabs[tab]).html("<h3>No Data to display</h3>");
        $(".panel-overlay",self.view.tabs[tab].fdCameraTile).hide();
      }
    },

    addFdPixelHisto: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].fdPixelHistogramTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(fdPixelTemplate,x,y,width,height);
        self.view.tabs[tab].fdPixelHistogramTile=$(".fd-pixel-histogram-tile",self.view.tabs[tab]);
        self.drawFdPixelHisto(tab);
      }else{
        console.log("fdPixelHistogram already exists");
      }
    },

    drawFdPixelHisto: function(tab){
      var self=this;
      $(".remove-fd-pixel-histogram",self.view.tabs[tab]).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].fdPixelHistogramTile);
        self.view.tabs[tab].fdPixelHistogramTile=null;
      });
      $(".save-plot",self.view.tabs[tab].fdPixelHistogramTile).click(function(){
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].fdPixelHistogramTile);
        var plotWidth=plotSize[0];
        var plotHeight=plotSize[1];
        self.view.savePlot("fd_pixel_histogram",self.view.tabs[tab].fdPixelHistogramTile,plotWidth,plotHeight);
      });
      self.view.tabs[tab].fdPixelHistogramTile.on("resizestop",function(){
        self.updateFdPixelHisto(tab,self.view.tabs[tab].activeFdPixel,false,false);
      });
      $(".fd-pixel-all-timeslots",self.view.tabs[tab].fdPixelHistogramTile).change(function(){self.updateFdPixelHisto(tab,self.view.tabs[tab].activeFdPixel,false)});
      $(".fd-pixel-plot",self.view.tabs[tab]).prop("id",tab+"_fdPixelHistogram"+self.view.id);
      self.updateFdPixelHisto(tab,-1,false,true);
    },

    updateFdPixelHisto: function(tab,pixel,changeZoom,changeEye) {
      var self = this;
      self.view.tabs[tab].fdPixelHistogramTile.data("changeZoom",changeZoom);
      self.view.tabs[tab].activeFdPixel = pixel;
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].fdPixelHistogramTile);
      var plotWidth=plotSize[0];
      var plotHeight=plotSize[1];
      var eye;
      if (changeEye == true) {
        eye = -1
      } else {
        if ($("#select-eye", self.view.tabs[tab]) == null) {
          eye = -1;
        } else {
          eye = $("#select-eye", self.view.tabs[tab]).val();
        }
      }
      if (pixel > 0) {
        $(".fd-pixel-number", self.view.tabs[tab].fdPixelHistogramTile).html(pixel);
      } else {
        self.view.GET("get_fd_first_pixel_id", JSON.stringify({
          "eye": eye
        }), function (err, res) {
          if (res != null) {
            $(".fd-pixel-number", self.view.tabs[tab].fdPixelHistogramTile).html(res.id);
          }
        })
      }
      $(".fd-pixel-histogram-eye",self.view.tabs[tab].fdPixelHistogramTile).attr("data-value",eye);
      $(".fd-pixel-histogram-pixel",self.view.tabs[tab].fdPixelHistogramTile).attr("data-value",pixel);
      var allTimeSlots = $(".fd-pixel-all-timeslots", self.view.tabs[tab].fdPixelHistogramTile).prop("checked");
      if ((plotHeight > 100) && (plotWidth > 100)) {
        $(".panel-overlay", self.view.tabs[tab].fdPixelHistogramTile).show();
        self.view.GET("get_plot", JSON.stringify({
          "plot_name": "fd_pixel_histogram",
          "plot_width": plotWidth,
          "plot_height": plotHeight,
          "eye": eye,
          "pixel_id": pixel,
          "all_time_slots": allTimeSlots,
          "zoom_level": null,
          "tab": tab
        }));
      }
    },

    renderFdPixelHisto: function(tab,data){
      var self=this;
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].fdPixelHistogramTile);
      var plotWidth=plotSize[0];
      var plotHeight=plotSize[1];
      var changeZoom=self.view.tabs[tab].fdPixelHistogramTile.data("chagneZoom");
      var zoomLevel=self.view.getZoomLevel(self.view.tabs[tab].fdPixelHistogramTile,changeZoom);
      if(data!=null){
        d3.select("#"+tab+"_fdPixelHistogram"+self.view.id).selectAll("*").remove();
        var commands=data;
        commands["height"]=plotHeight;
        commands["width"]=plotWidth;
        self.view.tabs[tab].fdPixelHistogramTile.graph=mpld3.draw_figure(tab+"_fdPixelHistogram"+self.view.id,commands);
        if (zoomLevel!=null) {
          self.view.tabs[tab].fdPixelHistogramTile.graph.axes[0].set_axlim([zoomLevel[0], zoomLevel[1]], [zoomLevel[2], zoomLevel[3]], 0, false);
        }
      }else{
        $(".fd-pixel-plot",self.view.tabs[tab]).html("<h3>No Data to display</h3>")
      }
      $(".panel-overlay",self.view.tabs[tab].fdPixelHistogramTile).hide();
    },

    selectPixel: function(tab,id){
      var self=this;
      if(self.view.tabs[tab].fdPixelHistogramTile!=null){
        self.updateFdPixelHisto(tab,id,true,false);
      }
    }

  });
  return fdFunctions;
});