require.config({
  paths: {
    "mpld3": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/mpld3.v0.3"),
    "augeroffline_d3": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/d3.v3.min"),
    "gridstack": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/gridstack/gridstack"),
    "lodash": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/lodash/lodash")
  },
  shim: {
    gridstack: ["jquery", "lodash"],
    mpld3: {
      exports: "mpld3"
    }
  }
});
define(["jclass",
  "async",
  "jquery",
  "augeroffline_d3",
  "text!../html/main.html",
  "text!../html/event-criteria-popover.html",
  "gridstack",
  "css!./../css/augerofflineeventbrowser",
  "css!./../vendor/gridstack/gridstack.css" ], function(jClass, async, $, d3,MainTemplate, eventCriteriaTemplate) {
  var setupFunctions = jClass._extend({
    init: function init(view) {
      var self = this;
      init._super.call(this, arguments);
      self.view = view;
    },
    setupElements: function(node){
      var self=this;
      self.view.isTabLoading=false;
      var vispaLogoPath=vispa.dynamicURL("extensions/augerofflineeventbrowser/static/img/vispa_logo.png");
      var augerLogoPath=vispa.dynamicURL("extensions/augerofflineeventbrowser/static/img/auger_logo.png");
      self.view.node = $(MainTemplate).appendTo(node);
      $(".auger-logo",self.view.node).attr("src",augerLogoPath);
      $(".vispa-logo",self.view.node).attr("src",vispaLogoPath);
      self.view.activeTab="ov";
      self.view.tabs=[];
      self.view.tabs["ov"]=$("#overview",self.view.node);
      self.view.tabs["sd"]=$("#SD",self.view.node);
      self.view.tabs["rd"]=$("#RD",self.view.node);
      self.view.tabs["fd"]=$("#FD",self.view.node);
      self.view.tabs["custom"]=$("#custom",self.view.node);
      self.view.tabs["ov"].updateNeeded=true;
      self.view.tabs["sd"].updateNeeded=true;
      self.view.tabs["rd"].updateNeeded=true;
      self.view.tabs["fd"].updateNeeded=true;
      self.view.tabs["custom"].updateNeeded=false;
      self.view.tabs["custom"].hasLoaded=false;
      self.view.gridstackNodes=[];
      self.view.gridstackNodes["ov"]=$(".grid-stack",self.view.tabs["ov"]);
      self.view.gridstackNodes["sd"]=$(".grid-stack",self.view.tabs["sd"]);
      self.view.gridstackNodes["rd"]=$(".grid-stack",self.view.tabs["rd"]);
      self.view.gridstackNodes["fd"]=$(".grid-stack",self.view.tabs["fd"]);
      self.view.gridstackNodes["custom"]=$(".grid-stack",self.view.tabs["custom"]);
      self.view.tabs["ov"].eventOverviewTile=$(".event-overview-tile",self.view.tabs["ov"]);
      self.view.tabs["ov"].eventSelectionTile=$(".event-selection-tile",self.view.tabs["ov"]);
      self.view.tabs["ov"].eventCategoriesTile=$(".event-categories-tile",self.view.tabs["ov"]);
      self.view.tabs["ov"].fileListTile=$(".file-list-tile",self.view.tabs["ov"]);
      self.view.tabs["sd"].sdEventInfoTile = $(".sd-event-info-tile", self.view.tabs["sd"]);
      self.view.tabs["sd"].sdLDFTile=$(".sd-ldf-tile",self.view.tabs["sd"]);
      self.view.tabs["sd"].sdMapTile=$(".sd-map-tile",self.view.tabs["sd"]);
      self.view.tabs["sd"].sdStationListTile=$(".sd-station-list-tile",self.view.tabs["sd"]);
      self.view.tabs["sd"].sdTimeResidualsTile=$(".sd-time-residuals-tile",self.view.tabs["sd"]);
      self.view.tabs["fd"].fdTimeFitTile=$(".fd-time-fit-tile",self.view.tabs["fd"]);
      self.view.tabs["fd"].fdEventInfoTile=$(".fd-event-info-tile",self.view.tabs["fd"]);
      self.view.tabs["fd"].fdShowerProfileTile=$(".fd-shower-profile-tile",self.view.tabs["fd"]);
      self.view.tabs["fd"].fdCameraTile=$(".fd-camera-tile",self.view.tabs["fd"]);
      self.view.tabs["fd"].fdPixelHistogramTile=$(".fd-pixel-histogram-tile",self.view.tabs["fd"]);
      self.view.eventCriteria=[0,50,0,5,0,50,90000,1000000000,0,90];
      self.view.activeTab="ov";
      self.view.eventSelector=$(".event-selection-dropdown",self.view.node);
      self.view.plotData=[];
      self.view.infoData=[];
      self.view.requestedPlotData=0;
      self.view.requestedInfoData=0;
      for (var index in self.view.tabs) {
        self.view.tabs[index].activeStation = 0;
        self.view.tabs[index].activeSdStation = 0;
      }
      self.view.gridstackOptions={
        vertical_margin: 5,
        horizontal_margin: 5,
        cell_height: 50,
        width: 36,
        draggable: {
          handle: ".panel-heading"
        }
      };
      self.view.panelNode=$(".grid-stack",self.view.node);
      self.view.panelNode.gridstack(self.view.gridstackOptions);
      $(".add-custom-tab",self.view.node).click(function(){
        self.view.customTabFunctions.addTabDialog();
      });
      $(".previous-event", self.view.node).click(function() {
        $(".event-selection-overlay",self.view.node).show();
        self.view.POST("read_previous_event", JSON.stringify({"criteria": self.view.eventCriteria}));
      });
      $(".next-event", self.view.node).click(function() {
        $(".event-selection-overlay",self.view.node).show();
        self.view.nextEvent(self,false);
      });
      $(".save-tab",self.view.node).click(function(){
        var target;
        if($(".save-tab-target-selection",self.view.node).html()=="custom"){
          target="custom";
        }else
        {
          target="custom_"+$(".save-tab-target-selection",self.view.node).html();
        }
        if((target==self.view.activeTab)||(self.view.prefs.get("saveTabWarning") == false)) {
          self.view.customTabFunctions.saveTab(self.activeTab, target);
        }else {
          self.view.confirm("Save current layout in tab "+$(".save-tab-target-selection",self.view.node).html()+"?",function(res){
            if(res){
              self.view.customTabFunctions.saveTab(self.view.activeTab,target);
            }
          });
        }
      });
    },
    setupSockets: function(){
      var self=this;
      self.view.socket.on("openADST",function(data){
        var data=JSON.parse(data);
        if(data!=null) {
          if ((!data.success)&&(data.reason!=4)) {
            if (data.reason == 0) {
              self.view.changeOfflinePath(filePath);
            } else {
              self.view.alert(data.info);
            }
          } else {
            if(!data.success){
              self.view.alert(data.info);
            }
            self.view.setLoading(false);
            self.view.POST("make_event_dictionaries");
            if ((self.view.state.get("tabName") != null) && (self.view.state.get("tabContent") != null)) {
              self.view.customTabFunctions.makeTab(self.view.state.get("tabName"), self.view.state.get("tabContent"));
            }
            self.draw_ov();
            self.draw_radio();
            self.draw_sd();
            self.draw_fd();
            self.view.state.set("path", self.view.filePath);
            self.view.state.set("eventNumber", data.event_number);
          }
        }
      });
      self.view.socket.on("madeEventDictionaries",function(){
        self.view.tabs["ov"].updateNeeded = true;
        self.view.tabs["sd"].updateNeeded = true;
        self.view.tabs["rd"].updateNeeded = true;
        self.view.tabs["fd"].updateNeeded = true;
        self.view.tabs["custom"].updateNeeded = true;
        self.view.updateTab(self.view.activeTab,true,false);
        self.view.eventSelector.empty();
        self.view.GET("get_event_ids",JSON.stringify({
          "criteria": self.view.eventCriteria
        }));
      });
      self.view.socket.on("getEventIds",function(path){
        $(".event-selection-overlay",self.view.node).hide();
        if(path!=null) {
          self.view.GET("/ajax/fs/getfile", {
            "path": path
          }, function (err, res) {
            if (res.success) {
              var ret = JSON.parse(res.content);
              self.view.eventSelector.empty();
              $(ret).each(function () {
                var entry = "";
                if (this.auger_id != 0) {
                  entry += "Auger: " + this.auger_id.toString() + "  ";
                }
                if (this.sd_id != 0) {
                  entry += "SD: " + this.sd_id.toString() + "  ";
                }
                if ((this.fd_id != 0) && (this.fd_run != 0)) {
                  entry += "FD: " + this.fd_run.toString() + "/" + this.fd_id.toString() + " ";
                }
                if ((this.rd_id != 0) && (this.rd_run != 0)) {
                  entry += "RD:" + this.rd_run.toString() + "/" + this.rd_id.toString() + " ";
                }
                var newItem = $("<option id='event-selector-option' data-bind='dropdown-event' class='event-selection-dropdown-item'></option>").val(this.i).html(entry);
                newItem.value = this.i;
                self.view.eventSelector.append(newItem);
              });
              self.view.eventSelector.selectpicker("refresh");
              self.view.eventSelector.on("change", function () {
                $(".event-selection-overlay",self.view.node).show();
                var id =self.view.eventSelector.val();
                self.view.POST("go_to_event", JSON.stringify({"event_number": id}))
              });
              for (var index in self.view.tabs) {
                if (self.view.tabs[index].eventOverviewTile != null) {
                  self.view.ovFunctions.updateEventOverview(index);
                }
                if (self.view.tabs[index].eventCategoriesTile != null) {
                  self.view.ovFunctions.updateEventCategories(index);
                }
                if (self.view.tabs[index].fileListTile != null) {
                  self.view.ovFunctions.updateFileList(index);
                }
              }
              if (ret.length == 0) {
                self.view.eventSelector.append($("<option id='event-selector-option' data-bind='dropdown-event' class='event-selection-dropdown-item' data-hidden='true'>No Events</option>").val(-1));
                self.view.eventSelector.selectpicker("val", -1);
              }
            }
          });
          self.view.GET("/ajax/fs/exists", {
            path: path
          }, function (err, exists) {
            if (exists) {
              self.view.POST("ajax/fs/remove", {
                "path": path
              });
            }
          })
        }
      });
      self.view.socket.on("getPlotly",function(path){
        if(path!=null){
          self.view.GET("/ajax/fs/getfile",{
            "path": path
          },function(err,res){
            if(res.success){
              var ret=JSON.parse(res.content);
              var plotName=ret[0];
              var tab=ret[1];
             var plot=ret[2];
              self.view.renderPlot(plotName,tab,plot);
              self.view.GET("/ajax/fs/exists",{
                path: path
              },function(err,exists){
                self.view.POST("ajax/fs/remove",{
                  "path":path
                })
              });
            }
          });
        }
      });
      self.view.socket.on("getPlot",function(path){
        if(path!=null){
          self.view.GET("/ajax/fs/getfile",{
            "path":path
          },function(err,res) {
            if((res.success)&&( res.content!=null)) {
              var ret=JSON.parse(res.content);
              var plotName=ret[0];
              var tab=ret[1];
              var plot=ret[2];
              if(self.view.synchronous){
                self.view.plotData.push(ret);
                self.view.handleData();
              }else {
                self.view.renderPlot(plotName, tab, plot);
              }
              self.view.GET("/ajax/fs/exists", {
                path: path
              }, function (err, exists) {
                if (exists) {
                  self.view.POST("ajax/fs/remove", {
                    "path": path
                  });
                }
              });
            }
          });
        }
      });
      self.view.socket.on("getSdEventInfo",function(path){
        if(path!=null){
          self.view.GET("/ajax/fs/getfile",{
            "path": path
          },function(err,res){
            if(res.success){
              var ret=JSON.parse(res.content);
              if(self.view.synchronous){
                self.view.infoData.push(["sdEventInfo",ret[0],ret[1]]);
                self.view.handleData();
              }else {
                self.view.sdFunctions.renderSdEventInfo(ret[1], ret[0]);
              }
              self.view.GET("/ajax/fs/exists",{
                path: path
              },function(err,exists){
                if(exists){
                  self.view.POST("ajax/fs/remove",{
                    "path": path
                  });
                }
              });
            }
          });
        }
      });
      self.view.socket.on("getSdStations",function(path){
        if(path!=null){
          self.view.GET("/ajax/fs/getfile",{
            "path": path
          },function(err,res){
            if(res.success){
              var ret=JSON.parse(res.content);
              if(self.view.synchronous){
                self.view.infoData.push(["sdStations",ret[0],ret[1]]);
                self.handleData();
              }else {
                self.view.sdFunctions.renderSdStationList(ret[1], ret[0]);
              }
              self.view.GET("/ajax/fs/exists",{
                path: path
              },function(err,exists){
                if(exists){
                  self.view.POST("ajax/fs/remove",{
                    "path": path
                  });
                }
              });
            }
          });
        }
      });
      self.view.socket.on("getRdStations",function(path){
        if(path!=null){
          self.view.GET("/ajax/fs/getfile",{
            "path": path
          },function(err,res){
            if(res.success){
              var ret=JSON.parse(res.content);
              if(self.synchronous){
                self.view.infoData.push(["rdStations",ret[0],ret[1]]);
                self.view.handleData();
              }else {
                self.view.rdFunctions.renderRdStationList(ret[1], ret[0]);
              }
              self.view.GET("/ajax/fs/exists",{
                path: path
              },function(err,exists){
                if(exists){
                  self.view.POST("ajax/fs/remove",{
                    "path": path
                  });
                }
              });
            }
          });
        }
      });
      self.view.socket.on("getRdEventInfo",function(path){
        if(path!=null){
          self.view.GET("/ajax/fs/getfile",{
            "path": path
          },function(err,res) {
            if (res.success) {
              if (res.content != null) {
                var ret = JSON.parse(res.content);
                if(self.view.synchronous){
                  self.view.infoData.push(["rdEventInfo",ret[0],ret[1]]);
                  self.view.handleData();
                }else {
                  self.view.rdFunctions.renderRdEventInfo(ret[1], ret[0]);
                }
                self.view.GET("/ajax/fs/exists", {
                  path: path
                }, function (err, exists) {
                  if (exists) {
                    self.view.POST("ajax/fs/remove", {
                      "path": path
                    });
                  }
                });
              }
            }
          });
        }
      });
      self.view.socket.on("getRdStationInfo",function(path){
        if(path!=null){
          self.view.GET("/ajax/fs/getfile",{
            "path": path
          },function(err,res){
            if(res.success){
              var ret=JSON.parse(res.content);
              if(self.view.synchronous){
                self.view.infoData.push(["rdStationInfo",ret[0],ret[1]]);
                self.view.handleData();
              }else {
                self.view.rdFunctions.renderRdStationInfo(ret[1], ret[0]);
              }
              self.view.GET("/ajax/fs/exists",{
                path: path
              },function(err,exists){
                if(exists){
                  self.view.POST("ajax/fs/remove",{
                    "path": path
                  });
                }
              });
            }
          });
        }
      });
      self.view.socket.on("getRdStationDataQuantities",function(path){
        if(path!=null) {
          self.view.GET("/ajax/fs/getfile", {
            "path": path
          }, function (err, res) {
            if (res.success) {
              var ret = JSON.parse(res.content);
              self.view.rdFunctions.renderRdStationQuantities(ret[1],ret[0]);
              self.view.GET("/ajax/fs/exists",{
                path: path
              },function(err,exists){
                if(exists){
                  self.view.POST("ajax/fs/remove",{
                    "path": path
                  });
                }
              });
            }
          });
        }
      });
      self.view.socket.on("getRdShowerDataQuantities",function(path){
        if(path!=null){
          self.view.GET("/ajax/fs/getfile",{
            "path": path
          },function(err,res){
            if(res.success){
              var ret=JSON.parse(res.content);
              self.view.rdFunctions.renderRdShowerQuantities(ret[1],ret[0]);
              self.view.GET("/ajax/fs/exists",{
                path: path
              },function(err,exists){
                if(exists){
                  self.view.POST("ajax/fs/remove",{
                    "path": path
                  });
                }
              });
            }
          });
        }
      });
      self.view.socket.on("getFdEventInfo",function(path){
        if(path!=null){
          self.view.GET("/ajax/fs/getfile",{
            "path": path
          },function(err,res){
            if(res.success){
              var ret=JSON.parse(res.content);
              if(self.view.synchronous){
                self.view.infoData.push(["fdEventInfo",ret[0],ret[1]]);
                self.view.handleData();
              }else {
                self.view.fdFunctions.renderFdEventInfo(ret[1], ret[0]);
              }
              self.view.GET("/ajax/fs/exists",{
                path: path
              },function(err,exists){
                if(exists){
                  self.view.POST("ajax/fs/remove",{
                    "path": path
                  });
                }
              });
            }
          });
        }
      });
      self.view.socket.on("getFileList",function(path){
        if(path!=null){
          self.view.GET("/ajax/fs/getfile",{
            "path": path
          },function(err,res){
            if(res.success){
              var ret=JSON.parse(res.content);
              self.view.ovFunctions.renderFileList(ret[1],ret[0]);
              self.view.GET("/ajax/fs/exists",{
                path: path
              },function(err,exists){
                if(exists){
                  self.view.POST("ajax/fs/remove",{
                    "path": path
                  });
                }
              });
            }
          });
        }
      });
      self.view.socket.on("getEventCategories",function(path){
        if(path!=null){
          self.view.GET("/ajax/fs/getfile",{
            "path": path
          },function(err,res){
            if(res.success){
              var ret=JSON.parse(res.content);
              self.view.ovFunctions.renderEventCategories(ret[1],JSON.parse(ret[0]));
              self.view.GET("/ajax/fs/exists",{
                path: path
              },function(err,exists){
                if(exists){
                  self.view.POST("ajax/fs/remove",{
                    "path": path
                  });
                }
              });
            }
          });
        }
      });
      self.view.socket.on("getOfflineConfiguration",function(path){
        if(path!=null){
          self.view.GET("/ajax/fs/getfile",{
            "path": path
          },function(err,res){
            if(res.success){
              var ret=JSON.parse(res.content);
              self.view.ovFunctions.renderOfflineConfiguration(ret[1],JSON.parse(ret[0]));
              self.view.GET("/ajax/fs/exists",{
                path: path
              },function(err,exists){
                if(exists){
                  self.view.POST("ajax/fs/remove",{
                    "path": path
                  });
                }
              });
            }
          });
        }
      });
      self.view.socket.on("eventChanged",function(reply){
        $(".event-selection-overlay",self.view.node).hide();
        if(reply!=null){
          self.view.eventChange(JSON.parse(reply).event_id);
        }
      });
    },
    draw_radio: function() {
      var self = this;
      self.view.tabs["rd"].rdLDFTile=$(".rd-ldf-tile",self.view.tabs["rd"]);
      self.view.tabs["rd"].rdStationTraceTile=$(".rd-station-trace-tile",self.view.tabs["rd"]);
      self.view.tabs["rd"].rdStationInfoTile=$(".rd-station-info-tile",self.view.tabs["rd"]);
      self.view.tabs["rd"].rdEventInfoTile=$(".rd-event-info-tile",self.view.tabs["rd"]);
      self.view.tabs["rd"].rdStationListTile=$(".rd-station-list-tile",self.view.tabs["rd"]);
      self.view.tabs["rd"].aeraMapTile=$(".aera-map-tile",self.view.tabs["rd"]);
      self.view.tabs["rd"].activeRdLDF="RD_2DLDF";
      self.view.rdFunctions.drawRdStationList("rd");
      self.view.rdFunctions.drawRdStationInfo("rd");
      self.view.rdFunctions.drawRdEventInfo("rd");
      self.view.rdFunctions.drawRdStationTrace("rd");
      self.view.rdFunctions.drawRdLDF("rd");
      self.view.rdFunctions.drawAeraMap("rd");
      self.view.setLoading(false);
    },
    draw_sd: function(){
      var self=this;
      self.view.sdFunctions.drawSdEventInfo("sd");
      self.view.sdFunctions.drawSdLDF("sd");
      self.view.sdFunctions.drawSdMap("sd");
      self.view.sdFunctions.drawSdStationList("sd");
      self.view.sdFunctions.drawSdTimeResiduals("sd");

    },

    draw_fd: function(){
      var self=this;
      self.view.fdFunctions.drawFdTimeFit("fd");
      self.view.fdFunctions.drawFdEventInfo("fd");
      self.view.fdFunctions.drawFdShowerProfile("fd");
      self.view.fdFunctions.drawFdCamera("fd");
      self.view.fdFunctions.drawFdPixelHisto("fd");
    },

    draw_ov: function(){
      var self=this;
      self.view.ovFunctions.drawEventOverview("ov");
      self.view.ovFunctions.drawEventSelection("ov");
      self.view.ovFunctions.drawEventCategories("ov");
      self.view.ovFunctions.drawFileList("ov");
    },
    setupSlideShow: function(){
      var self=this;
      var slideshowInterval=self.view.prefs.get("slideshowInterval");
      var slideShowRunning=false;
      self.view.timeout;
      if(slideshowInterval>0){
        var pauseLoop=$(".pause-event-loop",self.view.node);
        var startLoop=$(".start-event-loop",self.view.node);
        pauseLoop.click(function(){
          if(slideShowRunning) {
            self.synchronous=false;
            clearInterval(self.view.timeOut);
            slideShowRunning = false;
            startLoop.show();
            pauseLoop.hide();
          }
        });
        startLoop.click(function(){
          slideshowInterval=self.view.prefs.get("slideshowInterval");
          if(!slideShowRunning) {
            self.synchronous=true;
            self.view.timeOut = setInterval(function () {
              if(self.view.isTabLoading==false) {
                self.view.nextEvent(self, true);
                self.view.isTabLoading=true;
              }else{
              }
            }, slideshowInterval * 1000);
            pauseLoop.show();
            startLoop.hide();
            slideShowRunning = true;
          }
        });
      }
    },
    setupFullScreen: function(){
      var self=this;
      var enterFullscreen=$(".enter-fullscreen",self.view.node);
      var exitFullscreen=$(".exit-fullscreen",self.view.node);
      var fullscreenOn=false;
      if((!self.view.node[0].webkitRequestFullScreen)&&(!self.view.node[0].mozRequestFullScreen)&&(!self.view.node[0].requestFullscreen)){
        exitFullscreen.hide();
        enterFullscreen.hide();
      }
      enterFullscreen.click(function(){
        if(self.view.node[0].requestFullscreen){
          self.view.node[0].requestFullscreen();
        }if(self.view.node[0].webkitRequestFullScreen) {
          self.view.node[0].webkitRequestFullScreen();
        }else if(self.view.node[0].mozRequestFullScreen){
          self.view.node[0].mozRequestFullScreen();
        }
      });
      exitFullscreen.click(function(){
        if(self.view.node[0].requestFullscreen){
          document.exitFullscreen();
        }if(self.view.node[0].webkitRequestFullscreen) {
          document.webkitExitFullscreen();
        }else if(self.view.node[0].mozRequestFullScreen){
          document.mozCancelFullScreen();
        }
        $(".plot-settings",self.view.node).show();
        $(".fullscreen-top-bar",self.view.node).hide();
      });
      $(document).on("webkitfullscreenchange mozfullscreenchange fullscreenchange",function(){
        if(fullscreenOn){
          fullscreenOn=false;
          exitFullscreen.hide();
          enterFullscreen.show();
          $(".eventbrowser-navbar",self.view.node).show();
          $(".plot-settings",self.view.node).show();
          $(".fullscreen-top-bar",self.view.node).hide();
          if(self.view.isTabLoading==false) {
            self.view.updateTab(self.view.activeTab, false, true);
            self.view.isTabLoading=true;
          }
        }else{
          fullscreenOn=true;
          exitFullscreen.show();
          enterFullscreen.hide();
          $(".eventbrowser-navbar",self.view.node).hide()
          $(".plot-settings",self.view.node).hide();
          $(".fullscreen-top-bar",self.view.node).show();
          if(self.view.isTabLoading==false) {
            self.view.updateTab(self.view.activeTab, false, true);
            self.view.isTabLoading=true;
          }
        }
      });
    },
    setupEventCriteria: function(){
      var self=this;
      var cont = $(eventCriteriaTemplate);
      var popover = $(".event-criteria", self.view.node);
      $(".event-criteria-rd-slider", cont).slider({
        min: 0,
        max: 50,
        step: 1,
        range: true,
        value: [0, 50],
        scale: "logarithmic",
        tooltip: "hide"
      }).on("slide", function (evt) {
        var val;
        if (evt.value[1] == 50) {
          val = "all";
        } else {
          val = String(evt.value[1])
        }
        $(".rd-slider-val", cont)[0].innerHTML = String(evt.value[0]) + "-" + val;
        self.view.eventCriteria[4] = evt.value[0];
        self.view.eventCriteria[5] = evt.value[1];
      });
      $(".rd-slider-val", cont)[0].innerHTML = "0-all";
      $(".event-criteria-sd-slider", cont).slider({
        min: 0,
        max: 50,
        step: 1,
        range: true,
        value: [0, 50],
        scale: "logarithmic",
        tooltip: "hide"
      }).on("slide", function (evt) {
        var val;
        if (evt.value[1] == 50) {
          val = "all";
        } else {
          val = String(evt.value[1]);
        }
        $(".sd-slider-val", cont)[0].innerHTML = String(evt.value[0]) + "-" + val;
        self.view.eventCriteria[0] = evt.value[0];
        self.view.eventCriteria[1] = evt.value[1];
      });
      $(".sd-slider-val", cont)[0].innerHTML = "0-all";
      $(".event-criteria-fd-slider", cont).slider({
        min: 0,
        max: 5,
        step: 1,
        range: true,
        value: [0, 5],
        tooltip: "hide"
      }).on("slide", function (evt) {
        $(".fd-slider-val", cont)[0].innerHTML = String(evt.value[0]) + "-" + String(evt.value[1]);
        self.view.eventCriteria[2] = evt.value[0];
        self.view.eventCriteria[3] = evt.value[1];
      });
      $(".fd-slider-val", cont)[0].innerHTML = "0-5";
      $(".event-criteria-energy-slider", cont).slider({
        min: 90000,
        max: 1000000000,
        scale: "logarithmic",
        range: true,
        value: [90000, 1000000000],
        step: 100000,
        tooltip: "hide"
      }).on("slide", function (evt) {
        var val1;
        var val2;
        if (evt.value[0] == 90000) {
          val1 = "0"
        } else {
          val1 = String(evt.value[0])[0] + "e" + String(Math.floor(Math.log10(evt.value[0])) + 12);
        }
        if (evt.value[1] == 1000000000) {
          val2 = "&infin;";
        } else {
          val2 = String(evt.value[1])[0] + "e" + String(Math.floor(Math.log10(evt.value[1])) + 12);
        }
        $(".energy-slider-val", cont)[0].innerHTML = val1 + "-" + val2;
        self.view.eventCriteria[6] = Math.floor(evt.value[0] / Math.pow(10, Math.floor(Math.log10(evt.value[0])))) * Math.pow(10, Math.floor(Math.log10(evt.value[0])));
        self.view.eventCriteria[7] = Math.floor(evt.value[1] / Math.pow(10, Math.floor(Math.log10(evt.value[1])))) * Math.pow(10, Math.floor(Math.log10(evt.value[1])));
      });
      $(".energy-slider-val", cont)[0].innerHTML = "0-&infin;";
      $(".event-criteria-zenith-slider", cont).slider({
        min: 0,
        max: 90,
        range: true,
        step: 1,
        value: [0, 90],
        tooltip: "hide"
      }).on("slide", function (evt) {
        $(".zenith-slider-val", cont)[0].innerHTML = String(evt.value[0]) + "-" + String(evt.value[1]);
        self.view.eventCriteria[8] = evt.value[0];
        self.view.eventCriteria[9] = evt.value[1];
      });
      $(".zenith-slider-val", cont)[0].innerHTML = "0-90";
      $(".slider-horizontal", cont).each(function () {
        this.style.width = "100px";
      });
      $(".apply-event-criteria", cont).click(function () {
        $(".event-criteria", self.node).popover("hide");
        self.view.applyEventCriteria();
      });
      $(".event-criteria", self.view.node).popover({
        content: cont,
        html: true,
        "max-width": "350px"
      });
    },
    setupTabs: function(){
      var self=this;
      $(".ov-tab",self.view.node).on("shown.bs.tab",function(){
        self.view.activeTab="ov";
        if(self.view.tabs["ov"].updateNeeded==true){
          self.view.updateTab("ov",true,false);
        }
      });
      $(".rd-tab",self.view.node).on("shown.bs.tab",function(){
        self.view.activeTab="rd";
        if(self.view.tabs["rd"].updateNeeded==true) {
          self.view.updateTab("rd",true,false);
        }
      });
      $(".sd-tab",self.view.node).on("shown.bs.tab",function(){
        self.view.activeTab="sd";
        if(self.view.tabs["sd"].updateNeeded==true) {
          self.view.updateTab("sd",true,false);
        }
      });
      $(".fd-tab",self.view.node).on("shown.bs.tab",function(){
        self.view.activeTab="fd";
        if(self.tabs["fd"].updateNeeded==true) {
          self.view.updateTab("fd",true,false);
        }
      });
      $(".custom-tab",self.view.node).on("shown.bs.tab",function(){
        self.view.activeTab="custom";
        $(".save-tab-target-selection",self.view.node).html("custom");
        if(self.view.tabs["custom"].hasLoaded==false){
          self.view.customTabFunctions.loadTab("custom",true,false);
        }
      });
      $(".tab-handle",self.view.node).each(function(){
        var href=$(this).prop("href");
        $(this).prop("href",href+self.view.id);
      });
      $(".tab-pane",self.view.node).each(function(){
        var id=$(this).prop("id");
        $(this).prop("id",id+self.view.id);
      });
      var tabNameList=self.view.prefs.get("tabNames");
      for (var i = 0; i<tabNameList.length; i++){
        self.view.customTabFunctions.addTab(tabNameList[i]);
      }
    }
  });
  return setupFunctions;
});