require.config({
  paths: {
    "mpld3": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/mpld3.v0.2"),
    "augeroffline_d3": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/d3.v3.min"),
    "gridstack": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/gridstack/gridstack"),
    "lodash": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/lodash/lodash"),
  },
  shim: {
    gridstack: ["jquery", "lodash"],
    mpld3: {
      exports: "mpld3"
    }
  }
});

define([
  "vispa/views/main",
  "async",
  "jquery",
  "./fdFunctions",
  "./sdFunctions",
  "./rdFunctions",
  "./ovFunctions",
  "./simFunctions",
  "./preferences",
  "./customTabFunctions",
  "./setupFunctions",
  "augeroffline_d3",
  "gridstack",
  "css!./../css/augerofflineeventbrowser",
  "css!./../vendor/gridstack/gridstack.css"
], function(MainView, async, $, fdFunctions,sdFunctions, rdFunctions, ovFunctions,simFunctions,prefs, customTabFunctions, setupFunctions, d3) {

  if (!("d3" in window)) {
    window.d3 = d3;
  }
  var AugerOfflineEventbrowserView = MainView._extend({
    init: function init(args) {
      var self = this;
      init._super.apply(this, arguments);

      self.sdFunctions=new sdFunctions(this);
      self.rdFunctions=new rdFunctions(this);
      self.ovFunctions=new ovFunctions(this);
      self.fdFunctions=new fdFunctions(this);
      self.simFunctions=new simFunctions(this);
      self.customTabFunctions=new customTabFunctions(this);
      self.setupFunctions=new setupFunctions(this);
      this.state.setup({
        path: null,
        tabName: null,
        tabContent: [],
        eventNumber: 0
      },args);
     },

    getFragment: function(){
      return JSON.stringify({
        "path":this.state.get("path"),
        "tabName": this.state.get("tabName"),
        "tabContent": this.state.get("tabContent"),
        "eventNumber": this.state.get("eventNumber")
      });
    },

    applyFragment: function(fragment){
      var frag=JSON.parse(fragment);
      this.state.set("path", frag.path);
      this.state.set("tabName", frag.tabName);
      this.state.set("tabContent",frag.tabContent);
      this.state.set("eventNumber",frag.eventNumber);
    },

    updateTab: function(tab,changeZoom,synchronous){
      var self=this;
      self.requestedPlotData=0;
      self.requestedInfoData=0;
      if (self.tabs[tab].rdStationListTile != null) {
        self.rdFunctions.updateRdStationList(tab);
        self.requestedInfoData+=1;
      }
      if (self.tabs[tab].rdStationInfoTile != null) {
        self.rdFunctions.updateRdStationInfo(tab, 0);
        self.requestedInfoData+=1;
      }
      if (self.tabs[tab].rdEventInfoTile != null) {
        self.rdFunctions.updateRdEventInfo(tab);
        self.requestedInfoData+=1;
      }
      if (self.tabs[tab].rdTimeResidualsTile != null) {
        self.rdFunctions.updateRdTimeResiduals(tab, changeZoom);
        self.requestedPlotData+=1;
      }
      if (self.tabs[tab].rdLDFTile != null) {
        self.rdFunctions.updateRdLDF(tab, self.tabs[tab].activeRdLDF, changeZoom);
        self.requestedPlotData+=1;
      }
      if (self.tabs[tab].rdStationTraceTile != null) {
        self.rdFunctions.updateRdStationTrace(tab, 0, changeZoom);
        self.requestedPlotData+=1
      }
      if(self.tabs[tab].rdChannelTraceTile!=null){
        self.rdFunctions.updateRdChannelTrace(tab,0,changeZoom);
        self.requestedPlotData+=1;
      }
      if (self.tabs[tab].rdStationSpectrumTile != null) {
        self.rdFunctions.updateRdStationSpectrum(tab, 0, changeZoom);
        self.requestedPlotData+=1;
      }
      if (self.tabs[tab].aeraMapTile != null) {
        self.rdFunctions.updateAeraMap(tab, changeZoom);
        self.requestedPlotData+=1;
      }
      if (self.tabs[tab].sdEventInfoTile != null) {
        self.sdFunctions.drawSdEventInfo(tab);
        self.requestedInfoData+=1;
      }
      if (self.tabs[tab].sdStationListTile != null) {
        self.sdFunctions.updateSdStationList(tab);
        self.requestedInfoData+=1;
      }
      if (self.tabs[tab].sdLDFTile != null) {
        self.sdFunctions.updateSdLDF(tab, changeZoom);
        self.requestedPlotData+=1;
      }
      if (self.tabs[tab].sdTimeResidualsTile != null) {
        self.sdFunctions.updateSdTimeResiduals(tab, changeZoom);
        self.requestedPlotData+=1;
      }
      if (self.tabs[tab].sdMapTile != null) {
        self.sdFunctions.updateSdMap(tab, changeZoom);
        self.requestedPlotData+=1;
      }
      if (self.tabs[tab].sdVemTraceTile != null) {
        self.sdFunctions.updateSdVemTrace(tab, -1, changeZoom);
        self.requestedPlotData+=1;
      }
      if (self.tabs[tab].sdElectrodeTracesTile != null) {
        self.sdFunctions.updateSdElectrodeTraces(tab, -1, changeZoom);
        self.requestedPlotData+=1;
      }
      if (self.tabs[tab].fdEventInfoTile != null) {
        self.fdFunctions.updateFdEyeSelector(tab);
        self.fdFunctions.updateFdEventInfo(tab, -1);
        self.requestedInfoData+=1;
      }
      if (self.tabs[tab].fdTimeFitTile != null) {
        self.fdFunctions.updateFdTimeFit(tab, changeZoom, true);
        self.requestedPlotData+=1;
      }
      if (self.tabs[tab].fdShowerProfileTile != null) {
        self.fdFunctions.updateFdShowerProfile(tab, changeZoom, true);
        self.requestedPlotData+=1;
      }
      if (self.tabs[tab].fdCameraTile != null) {
        self.fdFunctions.updateFdCamera(tab, changeZoom, true);
        self.requestedPlotData+=1;
      }
      if (self.tabs[tab].fdPixelHistogramTile != null) {
        self.fdFunctions.updateFdPixelHisto(tab, -1, changeZoom, true);
        self.requestedPlotData+=1;
      }
      if (self.tabs[tab].sdSimParticleTypesTile!=null){
        self.simFunctions.updateSdSimParticleTypes(tab,true);
        self.requestedPlotData+=1;
      }
      if (self.tabs[tab].sdSimParticleMomentumTile!=null){
        self.simFunctions.updateSdSimParticleMomentum(tab,true);
        self.requestedPlotData+=1;
      }
      if(self.tabs[tab].eventDisplayTile!=null){
        self.ovFunctions.updateEventDisplay(tab);
      }
      self.tabs[tab].updateNeeded=false;
    },


    nextEvent: function(self,synchronous){
      var self=this;
      self.synchronous=synchronous;
      self.POST("read_next_event", JSON.stringify({"criteria": self.eventCriteria}));
    },

    eventChange: function(eventNumber) {
      var self = this;
      for (var index in self.tabs) {
        self.tabs[index].updateNeeded = true;
      }
      self.updateTab(self.activeTab, true, self.synchronous);
      self.eventSelector.selectpicker("val", eventNumber);
      self.state.set("eventNumber", eventNumber);
    },

    getPlotSize: function(tab,tile){
      var self=this;
      var tileWidth=tile.data("_gridstack_node").width*self.gridstackNodes[tab].data("gridstack").cell_width();
      var tileHeight=tile.data("_gridstack_node").height*self.gridstackNodes[tab].data("gridstack").cell_height();
      var headingHeight=$(".panel-heading",tile)[0].scrollHeight;
      var settingsHeight=$(".plot-settings",tile)[0].scrollHeight;
      var reply=[tileWidth-30,tileHeight-headingHeight-settingsHeight-30];
      return reply;
  },

    getPlotRequest: function(tab,plotName,tile){
      var self=this;
      var checkBoxes = $(".plot-bool", tile);
      var plotSize=self.getPlotSize(tab,tile);
      var plotParameters = {
        "plot_name": plotName,
        "plot_width": plotSize[0],
        "plot_height": plotSize[1]
      };
      var parName;
      var parValue;
      checkBoxes.each(function () {
        parName = this.getAttribute("data-parameter");
        parValue = $(this).prop("checked");
        plotParameters[parName] = parValue;
      });
      $(".plot-parameter", tile).each(function () {
        parName = this.getAttribute("data-parameter");
        parValue = this.getAttribute("data-value");
        plotParameters[parName] = parValue;
      });
      $(".plot-slider", tile).each(function () {
        parName = this.getAttribute("data-parameter");
        parValue = $(this).slider("getValue");
        plotParameters[parName] = parValue;
      });
      $(".plot-select", tile).each(function () {
        parName = this.getAttribute("data-parameter");
        parValue = $(this).val();
        plotParameters[parName] = parValue;
      });
      return plotParameters;
    },

    handleData: function(){
      var self=this;
      if((self.plotData.length>=self.requestedPlotData)&&(self.infoData.length>=self.requestedInfoData)){
        for(var i=0;i<self.plotData.length;i++){
          self.renderPlot(self.plotData[i][0],self.plotData[i][1],self.plotData[i][2]);
        }
        for(var j=0;j<self.infoData.length;j++){
          var data=self.infoData[j];
          switch(data[0]){
            case "sdEventInfo":
              self.sdFunctions.renderSdEventInfo(data[2],data[1]);
              break;
            case "sdStations":
              self.sdFunctions.renderSdStationList(data[2],data[1]);
              break;
            case "rdStations":
              self.rdFunctions.renderRdStationList(data[2],data[1]);
              break;
            case "rdEventInfo":
              self.rdFunctions.renderRdEventInfo(data[2],data[1]);
              break;
            case "rdStationInfo":
              self.rdFunctions.renderRdStationInfo(data[2],data[1]);
              break;
            case "fdEventInfo":
              self.fdFunctions.renderFdEventInfo(data[2],data[1]);
              break;
          }
        }
        self.requestedInfoData=0;
        self.requestedPlotData=0;
        self.infoData=[];
        self.plotData=[];
        self.isTabLoading=false;
      }
    },

    renderPlot: function(plotName,tab,data){
      var self=this;
      switch(plotName){
        case "SD_ldf":
          self.sdFunctions.renderSdLDF(tab,data);
          break;
        case "SD_map":
          self.sdFunctions.renderSdMap(tab,data);
          break;
        case "sd_time_residuals":
          self.sdFunctions.renderSdTimeResiduals(tab,data);
          break;
        case "sd_vem_trace":
          self.sdFunctions.renderSdVemTrace(tab,data);
          break;
        case "sd_electrode_trace":
          self.sdFunctions.renderSdElectrodeTraces(tab,data);
          break;
        case "fd_time_fit":
          self.fdFunctions.renderFdTimeFit(tab,data);
          break;
        case "fd_shower_profile":
          self.fdFunctions.renderFdShowerProfile(tab,data);
          break;
        case "fd_camera":
          self.fdFunctions.renderFdCamera(tab,data);
          break;
        case "fd_pixel_histogram":
          self.fdFunctions.renderFdPixelHisto(tab,data);
          break;
        case "RD_station_trace":
          self.rdFunctions.renderRdStationTrace(tab,data);
          break;
        case "RD_channel_trace":
          self.rdFunctions.renderRdChannelTrace(tab,data);
          break;
        case "RD_station_spectrum":
          self.rdFunctions.renderRdStationSpectrum(tab,data);
          break;
        case "rd_ldf":
          self.rdFunctions.renderRdLDF(tab,data);
          break;
        case "AERA_map":
          self.rdFunctions.renderAeraMap(tab,data);
          break;
        case "RD_time_residuals":
          self.rdFunctions.renderRdTimeResiduals(tab,data);
          break;
        case "sd_sim_particle_types":
          self.simFunctions.renderSdSimParticleTypes(tab,data);
          break;
        case "sd_sim_particle_momentum":
          self.simFunctions.renderSdSimParticleMomentum(tab,data);
          break;
        case "sd_sim_particle_time_momentum_distribution":
          self.simFunctions.renderSdSimParticleTimeMomentumDistribution(tab,data);
          break;
        case "event_overview_histogram":
          self.ovFunctions.renderEventOverview(tab,data);
          break;
        case "incoming_directions":
          self.ovFunctions.renderIncomingDirections(tab,data);
          break;
        case "event_display":
          self.ovFunctions.renderEventDisplay(tab,data);
      }
    },

    savePlot: function(plotName,tile,plotWidth,plotHeight){
      var self=this;
      var args = {
        callback: function (path) {
          if (path != null) {
            self.GET("/ajax/fs/exists", {
              path: path
            }, function (err, selectedType) {
              if (selectedType == "f") {
                var msg = "<html> File already exists. <br />" +
                  "Do you want to overwrite it ? </html>";
                self.confirm(msg, function (confirmed) {
                  if (!confirmed) {
                    return;
                  }else{
                    self.save(plotName,tile,plotWidth,plotHeight,path);
                  }
                })
              }else{
                self.save(plotName,tile,plotWidth,plotHeight,path);
              }
            })
          }
        }
      };
        self.spawnInstance("file","FileSelector", args);
    },
    save: function(plotName,tile,plotWidth,plotHeight,path){
      var self=this;
      var checkBoxes = $(".plot-bool", tile);
      var plotParameters = {
        "plot_name": plotName,
        "plot_width": plotWidth,
        "plot_height": plotHeight,
        "zoom_level": self.getZoomLevel(tile, false),
        "saveas": path
      };
      var parName;
      var parValue;
      checkBoxes.each(function () {
        parName = this.getAttribute("data-parameter");
        parValue = $(this).prop("checked");
        plotParameters[parName] = parValue;
      });
      $(".plot-parameter", tile).each(function () {
        parName = this.getAttribute("data-parameter");
        parValue = this.getAttribute("data-value");
        plotParameters[parName] = parValue;
      });
      $(".plot-slider", tile).each(function () {
        parName = this.getAttribute("data-parameter");
        parValue = $(this).slider("getValue");
        plotParameters[parName] = parValue;
      });
      $(".plot-select", tile).each(function () {
        parName = this.getAttribute("data-parameter");
        parValue = $(this).val();
        plotParameters[parName] = parValue;
      });
      self.GET("save_plot", JSON.stringify(plotParameters), function (err, ret) {
        if (ret.success == false) {
          self.alert(ret.error_message);
        }
      });
    },

    openFile: function(){
      var self=this;
      var args = {
        "path": '$HOME',
        callback: function (path) {
          self.POST("open_ADST", JSON.stringify({
            "filepath": path,
            "offline_path": self.offlinePath
          }), function (err, data) {
            if (!data.sucess) {
              alert(data.info);
              self.close();
            } else {
              self.POST("make_event_dictionaries", function () {
                self.tabs["ov"].updateNeeded = true;
                self.tabs["sd"].updateNeeded = true;
                self.tabs["rd"].updateNeeded = true;
                self.tabs["fd"].updateNeeded = true;
                self.tabs["custom"].updateNeeded = true;
                self.updateTab(self.activeTab,true,false);
                self.eventSelector.empty();
                self.GET("get_event_ids",JSON.stringify({"criteria": self.eventCriteria}),function(err,ret){
                $(ret).each(function() {
                  var entry = "";
                  if (this.auger_id != 0) {
                    entry += "Auger: " + this.auger_id.toString() + "  ";
                  }
                  if (this.sd_id != 0) {
                    entry += "SD: " + this.sd_id.toString() + "  ";
                  }
                  if ((this.fd_id != 0) && (this.fd_run != 0)) {
                    entry += "FD: " + this.fd_run.toString() + "/" + this.fd_id.toString() + " ";
                  }
                  if ((this.rd_id != 0) && (this.rd_run != 0)) {
                    entry += "RD:" + this.rd_run.toString() + "/" + this.rd_id.toString() + " ";
                  }
                  self.eventSelector.append($("<option></option>").val(this.i).html(entry));
                });
                  self.eventSelector.selectpicker("refresh");
                });
              });
            }
          });
        }
      };
      self.spawnInstance("file", "FileSelector", args);
    },

    addFile: function() {
      var self = this;
      var args = {
        "path": "$HOME",
        "multimode": true,
        callback: function (path) {
          if (path != null) {
            self.POST("add_ADST", JSON.stringify({"filenames": path}), function (err, ret) {
              if (ret.success) {
                self.POST("make_event_dictionaries", function () {
                  self.updateEventSelectpicker();
                });
              }
            });
          }
        }
      };
      self.spawnInstance("file","FileSelector", args);
    },

    updateEventSelectpicker: function(){
      var self=this;
      self.GET("get_event_ids", JSON.stringify({"criteria": self.eventCriteria}),function (err, ret) {
        self.eventSelector.empty();
          $(ret).each(function () {
            var entry = "";
            if (this.auger_id != 0) {
              entry += "Auger: " + this.auger_id.toString() + "  ";
            }
            if (this.sd_id != 0) {
              entry += "SD: " + this.sd_id.toString() + "  ";
            }
            if ((this.fd_id != 0) && (this.fd_run != 0)) {
              entry += "FD: " + this.fd_run.toString() + "/" + this.fd_id.toString() + " ";
            }
            if ((this.rd_id != 0) && (this.rd_run != 0)) {
              entry += "RD:" + this.rd_run.toString() + "/" + this.rd_id.toString() + " ";
            }
            var newItem = $("<option id='event-selector-option' data-bind='dropdown-event' class='event-selection-dropdown-item'></option>").val(this.i).html(entry);
            newItem.value = this.i;
            self.eventSelector.append(newItem);
          });
          self.eventSelector.selectpicker("refresh");
          for (var index in self.tabs) {
            if (self.tabs[index].eventOverviewTile != null) {
              self.ovFunctions.updateEventOverview(index);
            }
            if (self.tabs[index].eventCategoriesTile != null) {
              self.ovFunctions.updateEventCategories(index);
            }
            if (self.tabs[index].fileListTile != null) {
              self.ovFunctions.updateFileList(index);
            }
          }
          if(ret.length==0) {
            self.eventSelector.append($("<option id='event-selector-option' data-bind='dropdown-event' class='event-selection-dropdown-item' data-hidden='true'>No Events</option>").val(-1));
            self.eventSelector.selectpicker("val",-1);
        }
      });
    },

    applyEventCriteria: function(){
      var self=this;
      $(".event-selection-overlay",self.node).show();
      self.GET("get_event_ids", JSON.stringify({"criteria": self.eventCriteria}));
    },

    getZoomLevel: function(tile,changeZoom){
      if(changeZoom==true){
        return null;
      }
      if(tile.graph!=null) {
        var ax = tile.graph.axes[0];
        var left=ax.x.invert(0);
        var right=ax.x.invert(ax.width);
        var top=ax.y.invert(0);
        var bottom=ax.y.invert(ax.height);
        return [left,right,bottom,top];
      }else{
        return null;
      }
    },

    changeOfflinePath: function(filePath){
      var self=this;
      self.prompt("Please specify your OffLine path",function(res){
        var offlinePath=res;
        self.POST("/ajax/fs/setworkspaceini",{
          request: JSON.stringify({
            AugerEventBrowser: {
              offlinepath: offlinePath
            }
          })
        },function(){
          if(filePath!=null){
            self.openADST(filePath,offlinePath);
          }
        });
      })
    },

    openADST: function(path,offlinePath,eventNumber){
      var self=this;
      var filePath;
      if(typeof path =="object"){
        filePath=path
      }else if(typeof path=="string"){
        filePath=[path];
      }
      self.offlinePath = offlinePath;
      self.filePath=filePath;
      self.POST("open_ADST", JSON.stringify({
        "filepaths": filePath,
        "offline_path": offlinePath,
        "event_number": eventNumber
      }));
    },

    render: function(node) {
      var self = this;
      self.setLabel("Auger Eventbrowser");
      self.setLoading(true);
      self.setupFunctions.setupElements(node);
      self.setupFunctions.setupTabs();
      self.setupFunctions.setupSockets();
      self.setupFunctions.setupSlideShow();
      self.setupFunctions.setupFullScreen();
      self.setupFunctions.setupEventCriteria();
      require([ "mpld3" ], function(mpld3) {
        var eventNumber;
        if (self.state.get("eventNumber")) {
          eventNumber = self.state.get("eventNumber");
        }else{
          eventNumber=0;
        }
        if(self.state.get("path")==null) {
          var openFileDialogPath=self.prefs.get("defaultPath");
          var args = {
            "path": openFileDialogPath,
            "multimode": true,
            callback: function (path) {
              if (path != null) {
                self.GET("check_offline_environment_variable", function (err, res) {
                  if (res == true) {
                    self.openADST(path, null,eventNumber);
                  } else {
                    self.GET("/ajax/fs/getworkspaceini", {
                      request: JSON.stringify({
                        AugerEventBrowser: "offlinepath"
                      })
                    }, function (err, ret) {
                      if (ret.content.AugerEventBrowser == null) {
                        self.changeOfflinePath(path);
                      } else {
                        self.openADST(path, ret.content.AugerEventBrowser.offlinepath,eventNumber)
                      }
                    })
                  }
                });
              } else {
                this.$root.instance.close();
              }
            }
          };
          self.spawnInstance("file", "FileSelector", args);
        }else{
          var path=self.state.get("path");
          self.GET("check_offline_environment_variable", function (err, res) {
            if (res == true) {
              self.openADST(path, null,eventNumber);
            } else {
              self.GET("/ajax/fs/getworkspaceini", {
                request: JSON.stringify({
                  AugerEventBrowser: "offlinepath"
                })
              }, function (err, ret) {
                if(ret!=null) {
                  if (ret.content.AugerEventBrowser == null) {
                    self.changeOfflinePath(path);
                  } else {
                    self.openADST(path, ret.content.AugerEventBrowser.offlinepath, eventNumber)
                  }
                }
              })
            }
          });
        }
      });
    },
    onBeforeClose: function(){
      var self=this;
      if (self.timeOut!=null) {
        clearInterval(self.timeOut);
      }
    }
  },{
    iconClass: "glyphicon glyphicon-eye-open",
    label: "AugerEventbrowser",
    name: "AugerOfflineEventbrowserView",
    menuPosition: 60,
    preferences: {
      items: prefs.preferences
    },
    menu: prefs.menu
  });
  return AugerOfflineEventbrowserView;
});
