require.config({
  paths: {
    "mpld3": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/mpld3.v0.3"),
    "augeroffline_d3": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/d3.v3.min"),
    "gridstack": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/gridstack/gridstack"),
    "lodash": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/lodash/lodash")
  },
  shim: {
    gridstack: ["jquery", "lodash"],
    mpld3: {
      exports: "mpld3"
    }
  }
});
define(["jclass",
  "async",
  "jquery",
  "augeroffline_d3",
  "text!../html/tab.html",
  "gridstack",
  "css!./../css/augerofflineeventbrowser",
  "css!./../vendor/gridstack/gridstack.css" ], function(jClass, async, $, d3,tabTemplate) {
  var customTabFunctions = jClass._extend({
    init: function init(view) {
      var self = this;
      init._super.call(this, arguments);
      self.view = view;
    },
    addTabDialog: function(){
      var self=this;
      self.view.prompt("Please enter a name for the new tab",function(res) {
        var tabName = res;
        if (tabName != "" && tabName != null) {
          var regx = /^[A-Za-z0-9]+$/;
          if (!regx.test(tabName)) {
            self.view.alert("Tab names may only contain alphanumeric characters.");
          } else {
            var tabNames = self.view.prefs.get("tabNames");
            if (tabNames.indexOf(tabName) != -1) {
              self.view.alert("Name already exists");
            } else {
              self.addTab(tabName);
              tabNames.push(tabName);
              self.view.prefs.set("tabNames", tabNames);
            }
          }
        }
      });
    },

    addTab: function(tabName_0){
      var tabName="custom_"+tabName_0;
      var self=this;
      var newTabHeader=$("<li role='presentation' class='new-tab'><a role='tab' data-toggle='tab' class='new-tab-href'>"+tabName_0+"</a></li>");
      $(".new-tab-href",newTabHeader).attr("href","#"+tabName+self.view.id);
      $(".new-tab-href",newTabHeader).attr("aria-controls",tabName+self.view.id);
      $(".new-tab-href",newTabHeader).attr("class",tabName+"-tab-href");
      newTabHeader.attr("class",tabName+"-tab");
      $(newTabHeader).insertAfter($(".custom-tab",self.view.node));
      var newTab=$(tabTemplate);
      var newTabPane= $(".new-tab-pane",newTab);
      newTabPane.attr("id",tabName+self.view.id);
      newTabPane.attr("class",tabName+"-tab-pane tab-pane");
      newTabPane.insertAfter($(".custom-tab-pane",self.node));
      $(".tabs-dropdown-list",self.view.node).append("<li><a class='tabs-dropdown-item'>"+String(tabName_0)+"</a></li>");
      $(".tabs-dropdown-item",self.view.node).unbind().click(function(){
        $(".save-tab-target-selection",self.view.node).html(this.innerHTML);
      });
      self.view.tabs[tabName]=newTabPane;
      self.view.gridstackNodes[tabName]=$(".grid-stack",newTabPane);
      self.view.gridstackNodes[tabName].gridstack(self.view.gridstackOptions);
      self.view.tabs[tabName].hasLoaded=false;
      $("."+tabName+"-tab",self.view.node).on("shown.bs.tab",function(){
        self.view.activeTab=tabName;
        $(".save-tab-target-selection",self.view.node).html(tabName_0);
        if(self.view.tabs[tabName].updateNeeded==true){
          self.view.updateTab(tabName,true,false);
        }
        if(self.view.tabs[tabName].hasLoaded==false){
          self.loadTab(tabName);
        }
      });
    },

    removeTab: function(tab){
      var self=this;
      var tabNames=self.view.prefs.get("tabNames");
      var nameFound=false;
      for(var i=0;i<tabNames.length;i++){
        if("custom_"+tabNames[i]==tab){
          nameFound=true;
          var tab_0=tabNames[i];
        }
      }
      if(nameFound==false){
        self.view.alert("Cannot remove tab "+tab);
      }else{
        self.view.confirm("Are you sure you want to remove the tab "+tab, function(ret){
            if(ret){
              var oldItems=self.view.prefs.get("customTabsContent");
              var items=[];
              for(var i=0;i<oldItems.length;i++){
                if(oldItems[i].tabName!=tab){
                  items.push(oldItems[i]);
                }
              }
              var tabNames=self.view.prefs.get("tabNames");
              for(var i=0;i<tabNames.length;i++){
                if("custom_"+tabNames[i]==tab){
                  $($(".tabs-dropdown-item",self.view.node)[i+1]).parent().remove();
                  if("custom_"+$(".save-tab-target-selection", self.view.node).html()==tab) {
                    $(".save-tab-target-selection", self.view.node).html("custom");
                  }
                  tabNames.splice(i,1);
                }
              }
              self.view.prefs.set("tabNames",tabNames);
              self.view.prefs.set("customTabsContent",items);
              $("."+tab+"-tab-pane",self.view.node).remove();
              $("."+tab+"-tab",self.view.node).remove();

            }
        });
      }
    },

    saveTab: function(tab,target){
      var self=this;
      var oldItems=self.view.prefs.get("customTabsContent");
      var items=[];
      for(var i=0;i<oldItems.length;i++){
        if(oldItems[i].tabName!=target){
          items.push(oldItems[i]);
        }
      }
      $(".grid-stack-item",self.view.tabs[tab]).each(function(){
        items.push({
          "x": $(this).attr("data-gs-x"),
          "y": $(this).attr("data-gs-y"),
          "width": $(this).attr("data-gs-width"),
          "height": $(this).attr("data-gs-height"),
          "class": $(this).attr("class").split(" ")[1],
          "tabName": String(target)
        });
      });
      self.view.prefs.set("customTabsContent",items);
      if(tab!=target) {
        self.view.tabs[target].hasLoaded = false;
      }
    },

    loadTab: function(tab){
      var self=this;
      var tiles=self.view.prefs.get("customTabsContent");
      if(tiles!=null) {
        $(".grid-stack-item", self.view.tabs[tab]).each(function () {
          self.view.gridstackNodes[tab].data("gridstack").remove_widget($(this));
        });
        for (var i = 0; i < tiles.length; i++) {
          var tile = tiles[i];
          if(tile.tabName==tab){
          switch (tile.class) {
            case "fd-time-fit-tile":
              self.view.tabs[tab].fdTimeFitTile = null;
              self.view.fdFunctions.addFdTimeFit(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "fd-event-info-tile":
              self.view.tabs[tab].fdEventInfoTile = null;
              self.view.fdFunctions.addFdEventInfo(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "fd-shower-profile-tile":
              self.view.tabs[tab].fdShowerProfileTile = null;
              self.view.fdFunctions.addFdShowerProfile(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "fd-pixel-histogram-tile":
              self.view.tabs[tab].fdPixelHistogramTile = null;
              self.view.fdFunctions.addFdPixelHisto(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "fd-camera-tile":
              self.view.tabs[tab].fdCameraTile = null;
              self.view.fdFunctions.addFdCamera(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "sd-event-info-tile":
              self.view.tabs[tab].sdEventInfoTile = null;
              self.view.sdFunctions.addSdEventInfo(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "sd-ldf-tile":
              self.view.tabs[tab].sdLDFTile = null;
              self.view.sdFunctions.addSdLDF(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "sd-map-tile":
              self.view.tabs[tab].sdMapTile = null;
              self.view.sdFunctions.addSdMap(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "sd-station-list-tile" :
              self.view.tabs[tab].sdStationListTile = null;
              self.view.sdFunctions.addSdStationList(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "sd-time-residuals-tile":
              self.view.tabs[tab].sdTimeResidualsTile = null;
              self.view.sdFunctions.addSdTimeResiduals(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "sd-vem-trace-tile":
              self.view.tabs[tab].sdVemTraceTile = null;
              self.view.sdFunctions.addSdVemTrace(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "sd-electrode-traces-tile":
              self.view.tabs[tab].sdElectrodeTracesTile = null;
              self.view.sdFunctions.addSdElectrodeTraces(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "rd-station-list-tile":
              self.view.tabs[tab].rdStationListTile = null;
              self.view.rdFunctions.addRdStationList(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "rd-event-info-tile":
              self.view.tabs[tab].rdEventInfoTile = null;
              self.view.rdFunctions.addRdEventInfo(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "rd-station-info-tile":
              self.view.tabs[tab].rdStationInfoTile = null;
              self.view.rdFunctions.addRdStationInfo(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "rd-station-trace-tile":
              self.view.tabs[tab].rdStationTraceTile = null;
              self.view.rdFunctions.addRdStationTrace(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "rd-station-channel-trace-tile":
              self.view.tabs[tab].rdChannelTraceTile=null;
              self.view.rdFunctions.addRdChannelTrace(tab,tile.x,tile.y,tile.width,tile.height);
              break;
            case "rd-station-spectrum-tile":
              self.view.tabs[tab].rdStationSpectrumTile = null;
              self.view.rdFunctions.addRdStationSpectrum(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "rd-ldf-tile":
              self.view.tabs[tab].rdLDFTile = null;
              self.view.rdFunctions.addRdLDF(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "aera-map-tile":
              self.view.tabs[tab].aeraMapTile = null;
              self.view.rdFunctions.addAeraMap(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "rd-time-residuals-tile":
              self.view.tabs[tab].rdTimeResidualsTile = null;
              self.view.rdFunctions.addRdTimeResiduals(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "event-overview-tile":
              self.view.tabs[tab].eventOverviewTile = null;
              self.view.ovFunctions.addEventOverview(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "event-selection-tile":
              self.view.tabs[tab].eventSelectionTile = null;
              self.view.ovFunctions.addEventSelection(tab, tile.x, tile.y, tile.width, tile.height);
              break;
            case "event-categories-tile":
              self.view.tabs[tab].eventCategoriesTile=null;
              self.view.ovFunctions.addEventCategories(tab,tile.x,tile.y,tile.width,tile.height);
              break;
            case "file-list-tile":
              self.view.tabs[tab].fileListTile=null;
              self.view.ovFunctions.addFileList(tab,tile.x,tile.y,tile.width,tile.height);
              break;
            case "offline-configuration-tile":
              self.view.tabs[tab].offlineConfigurationTile=null;
              self.view.ovFunctions.addOfflineConfiguration(tab,tile.x,tile.y,tile.width,tile.height);
              break;
            case "sd-sim-particle-types-tile":
              self.view.tabs[tab].sdSimParticleTypesTile=null;
              self.view.simFunctions.addSdSimParticleTypes(tab,tile.x,tile.y,tile.width,tile.height);
              break;
            case "sd-sim-particle-momentum-tile":
              self.view.tabs[tab].sdSimParticleMomentumTile=null;
              self.view.simFunctions.addSdSimParticleMomentum(tab,tile.x,tile.y,tile.width,tile.height);
            }
          }
        }
        self.view.tabs[tab].hasLoaded=true;
      }
    },
    makeTab: function(tabName_0,tabContent){    ///handles opening of file in custom tab
      var self=this;
      var tabName="custom_"+tabName_0;
      var newTabHeader=$("<li role='presentation' class='new-tab'><a role='tab' data-toggle='tab' class='new-tab-href'>"+tabName_0+"</a></li>");
      $(".new-tab-href",newTabHeader).attr("href","#"+tabName+self.view.id);
      $(".new-tab-href",newTabHeader).attr("aria-controls",tabName+self.view.id);
      $(".new-tab-href",newTabHeader).attr("class",tabName+"-tab-href");
      newTabHeader.attr("class",tabName+"-tab");
      $(newTabHeader).insertAfter($(".custom-tab",self.view.node));
      var newTab = $(tabTemplate);
      var newTabPane = $(".new-tab-pane", newTab);
      newTabPane.attr("id", tabName + self.view.id);
      newTabPane.attr("class", tabName + "-tab-pane tab-pane");
      newTabPane.insertAfter($(".custom-tab-pane", self.view.node));
      /*
      $(".tabs-dropdown-list", self.view.node).append("<li><a href='#' class='tabs-dropdown-item'>" + String(tabName_0) + "</a></li>");
      $(".tabs-dropdown-item", self.view.node).unbind().click(function () {
        $(".save-tab-target-selection", self.view.node).html(this.innerHTML);
      });
      */
      self.view.tabs[tabName] = newTabPane;
      self.view.gridstackNodes[tabName] = $(".grid-stack", newTabPane);
      self.view.gridstackNodes[tabName].gridstack(self.view.gridstackOptions);
      self.view.tabs[tabName].hasLoaded = true;
      var tab = tabName;
      var tiles = tabContent;
      $("."+tabName+"-tab-href",self.view.node).tab("show");
      if (tiles != null) {
        $(".grid-stack-item", self.view.tabs[tab]).each(function () {
          self.view.gridstackNodes[tab].data("gridstack").remove_widget($(this));
        });
        for (var i = 0; i < tiles.length; i++) {
          var tile = tiles[i];
          switch (tile[4]) {
            case "fd-time-fit-tile":
              self.view.tabs[tab].fdTimeFitTile = null;
              self.view.fdFunctions.addFdTimeFit(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "fd-event-info-tile":
              self.view.tabs[tab].fdEventInfoTile = null;
              self.view.fdFunctions.addFdEventInfo(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "fd-shower-profile-tile":
              self.view.tabs[tab].fdShowerProfileTile = null;
              self.view.fdFunctions.addFdShowerProfile(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "fd-pixel-histogram-tile":
              self.view.tabs[tab].fdPixelHistogramTile = null;
              self.view.fdFunctions.addFdPixelHisto(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "fd-camera-tile":
              self.view.tabs[tab].fdCameraTile = null;
              self.view.fdFunctions.addFdCamera(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "sd-event-info-tile":
              self.view.tabs[tab].sdEventInfoTile = null;
              self.view.sdFunctions.addSdEventInfo(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "sd-ldf-tile":
              self.view.tabs[tab].sdLDFTile = null;
              self.view.sdFunctions.addSdLDF(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "sd-map-tile":
              self.view.tabs[tab].sdMapTile = null;
              self.view.sdFunctions.addSdMap(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "sd-station-list-tile" :
              self.view.tabs[tab].sdStationListTile = null;
              self.view.sdFunctions.addSdStationList(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "sd-time-residuals-tile":
              self.view.tabs[tab].sdTimeResidualsTile = null;
              self.view.sdFunctions.addSdTimeResiduals(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "sd-vem-trace-tile":
              self.view.tabs[tab].sdVemTraceTile = null;
              self.view.sdFunctions.addSdVemTrace(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "sd-electrode-traces-tile":
              self.view.tabs[tab].sdElectrodeTracesTile = null;
              self.view.sdFunctions.addSdElectrodeTraces(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "rd-station-list-tile":
              self.view.tabs[tab].rdStationListTile = null;
              self.view.rdFunctions.addRdStationList(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "rd-event-info-tile":
              self.view.tabs[tab].rdEventInfoTile = null;
              self.view.rdFunctions.addRdEventInfo(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "rd-station-info-tile":
              self.view.tabs[tab].rdStationInfoTile = null;
              self.view.rdFunctions.addRdStationInfo(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "rd-station-trace-tile":
              self.view.tabs[tab].rdStationTraceTile = null;
              self.view.rdFunctions.addRdStationTrace(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "rd-station-channel-trace-tile":
              self.view.tabs[tab].rdChannelTraceTile=null;
              self.view.rdFunctions.addRdChannelTrace(tab, tile[0],tile[1],tile[2],tile[3]);
            case "rd-station-spectrum-tile":
              self.view.tabs[tab].rdStationSpectrumTile = null;
              self.view.rdFunctions.addRdStationSpectrum(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "rd-ldf-tile":
              self.view.tabs[tab].rdLDFTile = null;
              self.view.rdFunctions.addRdLDF(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "aera-map-tile":
              self.view.tabs[tab].aeraMapTile = null;
              self.view.rdFunctions.addAeraMap(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "rd-time-residuals-tile":
              self.view.tabs[tab].rdTimeResidualsTile = null;
              self.view.rdFunctions.addRdTimeResiduals(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "event-overview-tile":
              self.view.tabs[tab].eventOverviewTile = null;
              self.view.ovFunctions.addEventOverview(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "event-selection-tile":
              self.view.tabs[tab].eventSelectionTile = null;
              self.view.ovFunctions.addEventSelection(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "event-categories-tile":
              self.view.tabs[tab].eventCategoriesTile = null;
              self.view.ovFunctions.addEventCategories(tab, tile[0], tile[1], tile[2], tile[3]);
              break;
            case "offline-configuration-tile":
              self.view.tabs[tab].offlineConfigurationTile=null;
              self.view.ovFunctions.addOfflineConfiguration(tab,tile[0],tile[1],tile[2],tile[3]);
              break;
            case "sd-sim-particle-types-tile":
              self.view.tabs[tab].sdSimParticleTypesTile=null;
              self.view.simFunctions.addSdSimParticleTypes(tab,tile[0],tile[1],tile[2],tile[3]);
            case "sd-sim-particle-momentum-tile":
              self.view.tabs[tab].sdSimParticleMomentumTile=null;
              self.view.simFunctions.addSdSimParticleMomentum(tab,tile[0],tile[1],tile[2],tile[3]);
          }
        }
      }
      self.view.activeTab=tabName;
      $("." + tabName + "-tab", self.view.node).on("shown.bs.tab", function () {
        self.view.activeTab = tabName;
        $(".save-tab-target-selection", self.view.node).html(tabName_0);
        if (self.view.tabs[tabName].updateNeeded == true) {
          self.view.updateTab(tabName, true,false);
        }
        if (self.view.tabs[tabName].hasLoaded == false) {
          self.view.customTabFunctions.loadTab(tabName);
        }
      });
    }
  });
  return customTabFunctions;
});