define(["vispa/extension",
    "./augerofflineeventbrowser"],
    function(Extension, AugerOfflineEventbrowserView) {
  var AugerOfflineEventbrowserExtension = Extension._extend({
  
    init: function init() {
      var self = this;
      init._super.call(this, "augerofflineeventbrowser","AugerOfflineEventBrowser");
      this.mainMenuAdd([
        this.addView(AugerOfflineEventbrowserView)
      ]);
    }
  });
  return AugerOfflineEventbrowserExtension;
});
