require.config({
  paths: {
    "mpld3": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/mpld3.v0.2"),
    "augeroffline_d3": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/d3.v3.min"),
    "gridstack": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/gridstack/gridstack"),
    "lodash": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/lodash/lodash"),
  },
  shim: {
    gridstack: ["jquery", "lodash"],
    mpld3: {
      exports: "mpld3"
    }
  }
});
define([ "jclass",
  "async",
  "jquery",
  "augeroffline_d3",
  "plotly",
  "text!../html/rd_station_list.html",
  "text!../html/rd_event_info.html",
  "text!../html/rd_station_info.html",
  "text!../html/rd_station_trace.html",
  "text!../html/rd_station_channel_trace.html",
  "text!../html/rd_station_spectrum.html",
  "text!../html/rd_twodldf.html",
  "text!../html/aera_map.html",
  "text!../html/rd_time_residuals.html",
  "gridstack",
  "css!./../css/augerofflineeventbrowser",
  "css!./../vendor/gridstack/gridstack.css" ], function(jClass, async, $, d3,plotly,rdStationListTemplate,rdEventInfoTemplate,
                                                        rdStationInfoTemplate,rdStationTraceTemplate,
                                                        rdStationChannelTraceTemplate,rdStationSpectrumTemplate,
                                                        rdLDFTemplate,aeraMapTemplate,rdTimeResidualsTemplate) {
  var rdFunctions = jClass._extend({
    init: function init(view) {
      var self = this;
      init._super.call(this, arguments);
      self.view = view;
    },
    addRdStationList: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].rdStationListTile==null) {
        self.view.gridstackNodes[tab].data("gridstack").add_widget(rdStationListTemplate,x,y,width,height);
        self.view.tabs[tab].rdStationListTile = $(".rd-station-list-tile", self.view.tabs[tab]);
        self.drawRdStationList(tab);
      }else{
        console.log("rdStationList already exists");
      }
    },

    drawRdStationList: function(tab){
      var self=this;
      self.view.tabs[tab].rdStationTable=$(".rd-stations-table-body",self.view.tabs[tab]);
      self.view.tabs[tab].rdStationListTile.sortBy="Id";
      self.view.tabs[tab].rdStationListTile.sortOrder="up";
      self.updateRdStationList(tab);
      $(".remove-rd-station-list",self.view.tabs[tab]).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].rdStationListTile);
        self.view.tabs[tab].rdStationListTile=null;
      });
      $(".rd-stations-only-signal",self.view.tabs[tab].rdStationListTile).click(function(){
        self.updateRdStationList(tab);
      });
      $(".station-id-th",self.view.tabs[tab].rdStationListTile).click(function(){
        $(".rd-station-sort-criteria",self.view.tabs[tab].rdStationListTile).hide();
        if (self.view.tabs[tab].rdStationListTile.sortBy=="Id"){
          if(self.view.tabs[tab].rdStationListTile.sortOrder=="down"){
            self.view.tabs[tab].rdStationListTile.sortOrder="up";
            $(".sort-id-up",self.view.tabs[tab].rdStationListTile).show();
          }else{
            self.view.tabs[tab].rdStationListTile.sortOrder="down";
            $(".sort-id-down",self.view.tabs[tab].rdStationListTile).show();
          }
        }else {
          self.view.tabs[tab].rdStationListTile.sortOrder="up";
          self.view.tabs[tab].rdStationListTile.sortBy = "Id";
          $(".sort-id-up",self.view.tabs[tab].rdStationListTile).show();
        }
        self.updateRdStationList(tab);
      });
      $(".station-signal-th",self.view.tabs[tab].rdStationListTile).click(function(){
        $(".rd-station-sort-criteria",self.view.tabs[tab].rdStationListTile).hide();
        if (self.view.tabs[tab].rdStationListTile.sortBy=="Signal"){
          if(self.view.tabs[tab].rdStationListTile.sortOrder=="down"){
            self.view.tabs[tab].rdStationListTile.sortOrder="up";
            $(".sort-signal-up",self.view.tabs[tab].rdStationListTile).show();
          }else{
            self.view.tabs[tab].rdStationListTile.sortOrder="down";
            $(".sort-signal-down",self.view.tabs[tab].rdStationListTile).show();
          }
        }else {
          self.view.tabs[tab].rdStationListTile.sortOrder="down";
          self.view.tabs[tab].rdStationListTile.sortBy = "Signal";
          $(".sort-signal-down",self.view.tabs[tab].rdStationListTile).show();
        }
        self.updateRdStationList(tab);
      });
      $(".station-snr-th",self.view.tabs[tab].rdStationListTile).click(function(){
        $(".rd-station-sort-criteria",self.view.tabs[tab].rdStationListTile).hide();
        if (self.view.tabs[tab].rdStationListTile.sortBy=="SignalToNoise"){
          if(self.view.tabs[tab].rdStationListTile.sortOrder=="down"){
            self.view.tabs[tab].rdStationListTile.sortOrder="up";
            $(".sort-snr-up",self.view.tabs[tab].rdStationListTile).show();
          }else{
            self.view.tabs[tab].rdStationListTile.sortOrder="down";
            $(".sort-snr-down",self.view.tabs[tab].rdStationListTile).show();
          }
        }else {
          self.view.tabs[tab].rdStationListTile.sortOrder="down";
          self.view.tabs[tab].rdStationListTile.sortBy = "SignalToNoise";
          $(".sort-snr-down",self.view.tabs[tab].rdStationListTile).show();
        }
        self.updateRdStationList(tab);
      });
      self.view.tabs[tab].rdStationTable.on("click",".rd-stations-table-row",function(){
        $(self.view.tabs[tab].activeStationListItem).attr("class", self.view.tabs[tab].previousStationClass);
        self.view.tabs[tab].activeStationListItem=$(this);
        self.view.tabs[tab].previousStationClass=$(self.view.tabs[tab].activeStationListItem).attr("class");
        $(self.view.tabs[tab].activeStationListItem).removeClass("default").removeClass("success").addClass("info");
        if(self.view.tabs[tab].activeStation!=this.rowId) {
          if (self.view.tabs[tab].rdStationInfoTile != null) {
            self.drawRdStationInfo(tab, this.rowId);
          }
          if (self.view.tabs[tab].rdStationTraceTile != null) {
            self.updateRdStationTrace(tab, this.rowId, true);
          }
          if (self.view.tabs[tab].rdStationSpectrumTile != null) {
            self.updateRdStationSpectrum(tab, this.rowId, false);
          }
          if(self.view.tabs[tab].rdChannelTraceTile!=null){
            self.updateRdChannelTrace(tab,this.rowId,true);
          }
          self.view.tabs[tab].activeStation = this.rowId;
        }
        });
      if(self.view.tabs[tab].aeraMapTile==null){
        $(".locate-rd-station",self.view.tabs[tab].rdStationListTile).hide();
      }
      self.view.tabs[tab].rdStationTable.on("click",".locate-rd-station",function(event){
        event.stopPropagation();
        var id=$(this).parents(".rd-stations-table-row")[0].rowId;
        self.locateRdStation(tab,id)
      });
    },

    updateRdStationList: function(tab) {
      var self = this;
      $(".panel-overlay", self.view.tabs[tab].rdStationListTile).show();
      var onlySignal=$(".rd-stations-only-signal",self.view.tabs[tab].rdStationListTile).prop("checked");
      self.view.GET("get_RD_stations",JSON.stringify({
        "only_signal": onlySignal,
        "tab": tab
      }));
    },

    renderRdStationList: function(tab,data) {
      var self = this;
      if (data != null) {
        if(self.view.tabs[tab].rdStationListTile.sortOrder=="up") {
          data.sort(function (a, b) {
            return a[self.view.tabs[tab].rdStationListTile.sortBy] - b[self.view.tabs[tab].rdStationListTile.sortBy];
          });
        }else{
          data.sort(function (a, b) {
            return b[self.view.tabs[tab].rdStationListTile.sortBy] - a[self.view.tabs[tab].rdStationListTile.sortBy];
          });
        }
        var directives = {
          "id": {
            text: function () {
              return this.Id;
            }
          },
          "signal": {
            text: function () {
              return this.Signal + "+/-" + this.SignalError;
            }
          },
          "signal-to-noise": {
            text: function () {
              return this.SignalToNoise;
            }
          },
          "rd-stations-table-row": {
            "class": function () {
              if (this.Rejected==true){
                return "rd-stations-table-row danger"
              }else {
                if (this.HasPulse == true) {
                    return "rd-stations-table-row success";
                } else {
                  return "rd-stations-table-row default";
                }
              }
            },
            "rowId": function () {
              return this.Id;
            }
          }
        };
        self.view.tabs[tab].rdStationTable.render(data, directives);
        self.view.tabs[tab].activeStationListItem = $(".rd-stations-table-row", self.view.tabs[tab])[0];
        self.view.tabs[tab].previousStationClass = $(self.view.tabs[tab].activeStationListItem).attr("class");
        $(self.view.tabs[tab].activeStationListItem).removeClass("success").removeClass("default").addClass("info");
      }
      $(".panel-overlay", self.view.tabs[tab].rdStationListTile).hide();
    },

    renderRdShowerQuantities: function(tab,data){
      var self=this;
      if(self.view.tabs[tab].rdEventInfoTile!=null){
        if(data.length>0){
          var quantitiesSelector=$("#select-event-quantity",self.view.tabs[tab].rdEventInfoTile);
          quantitiesSelector.on("change", function () {
            self.updateRdEventInfo(tab);
          });
        quantitiesSelector.selectpicker();
        quantitiesSelector.empty();
        for(i=0;i<data.length;i++){
          var newItem=$("<option class='eye-option'></option>").html(data[i]).val(data[i]);
          newItem.value=data[i];
          quantitiesSelector.append(newItem);
        }
        quantitiesSelector.val(null);
        quantitiesSelector.selectpicker("refresh");
        }
      }else{
        console.log("rdEventInfo does not exist");
      }
    },

    addRdEventInfo: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].rdEventInfoTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(rdEventInfoTemplate,x,y,width,height);
        self.view.tabs[tab].rdEventInfoTile=$(".rd-event-info-tile",self.view.tabs[tab]);
        self.drawRdEventInfo(tab);
      }else{
        console.log("rdEventInfo already exists");
      }
    },

    drawRdEventInfo: function(tab) {
      var self = this;
      $(".remove-rd-event-information", self.view.tabs[tab]).click(function () {
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].rdEventInfoTile);
        self.view.tabs[tab].rdEventInfoTile = null;
      });
      self.view.GET("get_RD_shower_data_quantities",JSON.stringify({
        "tab": tab
      }));
      self.updateRdEventInfo(tab);
    },

    updateRdEventInfo: function(tab) {
      var self = this;
      $(".panel-overlay", self.view.tabs[tab].rdEventInfoTile).show();
      var quantity=$("#select-event-quantity",self.view.tabs[tab].rdEventInfoTile).val();
      self.view.GET("get_RD_event_info",JSON.stringify({
        "tab": tab,
        "quantity": quantity
      }));
    },

    renderRdEventInfo: function(tab,data) {
      var self = this;
      if (data != null) {
        var commands = {
          "rd-run": {
            text: function () {
              return this.run;
            }
          },
          "rd-event": {
            text: function () {
              return this.event;
            }
          },
          "utc-time": {
            text: function () {
              return this.date + "  " + this.time;
            }
          },
          "gps-time": {
            text: function () {
              return this.GPSseconds + "s, " + this.GPSnanoseconds + "ns";
            }
          },
          "stations": {
            text: function () {
              return this.stations;
            }
          },
          "zenith": {
            text: function () {
              if(this.zenith!=null) {
                if(this.zenithError!=null) {
                  return this.zenith + "+/-" + this.zenithError;
                }else{
                  return this.zenith;
                }
              }else{
                return "unknown";
              }
            }
          },
          "azimuth": {
            text: function () {
              if(this.azimuth!=null) {
                if(this.azimuthError!=null) {
                  return this.azimuth + "+/-" + this.azimuthError;
                }else{
                  return this.azimuth;
                }
              }else{
                return "unknown";
              }
            }
          },
          "radius": {
            text: function () {
              if(this.radius!=null) {
                return this.radius + "m";
              }else{
                return "unknown";
              }
            }
          },
          "recstage": {
            text: function () {
              return this.recStage;
            }
          },
          "energy": {
            text: function () {
              if (this.energy != null) {
                if (this.energyError != null) {
                  return String(this.energy)[0]+"."+String(this.energy)[1]+String(this.energy)[2] + "e" + String(Math.floor(Math.log10(this.energy)) + 12)+"+/-"+
                  String(this.energyError)[0]+"."+String(this.energyError)[1]+ "e" + String(Math.floor(Math.log10(this.energyError)) + 12);
                } else {
                  return String(this.energy)[0]+"."+String(this.energy)[1]+String(this.energy)[2] + "e" + String(Math.floor(Math.log10(this.energy)) + 12);
                }
              } else {
                return "unknown";
              }
            }
          },
          "quantity": {
            text: function(){
              if(this.quantityError==null){
                return this.quantityValue;
              }else{
                return this.quanitityValue+" +/- "+this.quantityError;
              }
            }
          }
        };
        $(".rd-event-info-table-body", self.view.tabs[tab]).render(data, commands);
        $("#select-event-quantity",self.view.tabs[tab].rdEventInfoTile).selectpicker("val",data.quantity);
      }
      $(".panel-overlay", self.view.tabs[tab].rdEventInfoTile).hide();
    },

    renderRdStationQuantities: function(tab,data){
      var self=this;
      if(self.view.tabs[tab].rdStationInfoTile!=null){
        if(data.length>0){
          var quantitiesSelector=$("#select-station-quantity",self.view.tabs[tab].rdStationInfoTile);
          quantitiesSelector.on("change", function () {
            self.updateRdStationInfo(tab,self.view.tabs[tab].activeStation);
          });
        quantitiesSelector.selectpicker();
        quantitiesSelector.empty();
        for(i=0;i<data.length;i++){
          var newItem=$("<option class='eye-option'></option>").html(data[i]).val(data[i]);
          newItem.value=data[i];
          quantitiesSelector.append(newItem);
        }
        quantitiesSelector.val(null);
        quantitiesSelector.selectpicker("refresh");
        }
      }else{
        console.log("rdStationInfo does not exist");
      }
    },

    addRdStationInfo: function(tab,x,y,width,height){
      var self=this;
      if (self.view.tabs[tab].rdStationInfoTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(rdStationInfoTemplate,x,y,width,height);
        self.view.tabs[tab].rdStationInfoTile=$(".rd-station-info-tile",self.view.tabs[tab]);
        self.drawRdStationInfo(tab,self.view.tabs[tab].activeStation);
      }else{
        console.log("rdStationInfo already exists");
      }
    },

    drawRdStationInfo: function(tab,id){
      var self=this;
      $(".remove-rd-station-info",self.view.tabs[tab]).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].rdStationInfoTile);
        self.view.tabs[tab].rdStationInfoTile=null;
      });
      self.view.GET("get_rd_station_data_quantities",JSON.stringify({
        "tab": tab
      }));
      self.updateRdStationInfo(tab,id);
    },

    updateRdStationInfo: function(tab,id) {
      var self = this;
      $(".panel-overlay", self.view.tabs[tab].rdStationInfoTile).show();
      var quantity=$("#select-station-quantity",self.view.tabs[tab].rdStationInfoTile).val();
      self.view.GET("get_RD_station_info", JSON.stringify({
        "station_id": id,
        "tab": tab,
        "quantity": quantity
      }));
    },

    renderRdStationInfo: function(tab,data) {
      var self = this;
      if (data != null) {
        var directives = {
          "id": {
            text: function () {
              return this.Id;
            }
          },
          "beta": {
            text: function () {
              if (this.Beta!=null) {
                if (this.BetaError!=null) {
                  return this.Beta + "+/-" + this.BetaError;
                }else{
                  return this.Beta;
                }
              }else{
                return "unknown";
              }
            }
          },
          "time-residual": {
            text: function () {
              if(this.TimeResidual!=null) {
                if(this.TimeResidualError!=null) {
                  return this.TimeResidual + "+/-" + this.TimeResidualError + "ns";
                }else{
                  return this.TimeResidual + " ns";
                }
              }else{
                return "unknown";
              }
            }
          },
          "rejectionStatus": {
            text: function () {
              if (this.RejectionStatus.length==0) {
                return "not rejected";
              } else {
                var reply="";
                for(var i=0;i<this.RejectionStatus.length;i++) {
                  reply = reply + this.RejectionStatus[i];
                }
                return reply;
              }
            }
          },
          "saturationStatus": {
            text: function () {
              if (this.IsSaturated == true) {
                return "saturated";
              } else {
                return "not saturated";
              }
            }
          },
          "signalPeak": {
            text: function () {
              return this.SignalPeak + " ns";
            }
          },
          "noiseRms":{
            text: function(){
              if(this.NoiseRms!=null){
                if(this.NoiseRmsError!=null){
                  return this.NoiseRms+"+/-"+this.NoiseRmsError+" micro V/m"
                }else{
                  return this.NoiseRms+" micro V/m"
                }
              }else{
                return "unknown";
              }
            }
          },
          "signalWindow": {
            text: function () {
              if (this.SignalWindowStart != null) {
                var windowStart = this.SignalWindowStart + " ns";
              } else {
                var windowStart = "unknown";
              }
              if (this.SignalWindowStop != null) {
                var windowStop = this.SignalWindowStop + " ns";
              } else {
                var windowStop = "unknown";
              }
              return "[" + windowStart + "..." + windowStop + "]";
            }
          },
          "quantity": {
            text: function(){
              if(this.QuantityError==null){
                return this.QuantityValue;
              }else{
                return this.QuanitityValue+" +/- "+this.QuantityError;
              }
            }
          }
        };
        var rdStationInfoTable = $(".rd-stations-info-table-body", self.view.tabs[tab]);
        rdStationInfoTable.render(data, directives);
        if(data.Quantity!=null){
          $("#select-station-quantity",self.view.tabs[tab].rdStationInfoTile).selectpicker("val",data.Quantity);
        }
      }
      $(".panel-overlay", self.view.tabs[tab].rdStationInfoTile).hide();
    },
    addRdStationTrace: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].rdStationTraceTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(rdStationTraceTemplate,x,y,width,height);
        self.view.tabs[tab].rdStationTraceTile=$(".rd-station-trace-tile",self.view.tabs[tab]);
        self.drawRdStationTrace(tab);
      }else{
        console.log("rdStationTrace already exists");
      }
    },

    drawRdStationTrace: function(tab){
      var self=this;
      $(".remove-rd-station-trace",self.view.tabs[tab]).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].rdStationTraceTile);
        self.view.tabs[tab].rdStationTraceTile=null;
      });
      $(".save-plot",self.view.tabs[tab].rdStationTraceTile).click(function(){
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].rdStationTraceTile);
        var plotWidth=plotSize[0];
        var plotHeight=plotSize[1];
      self.view.savePlot("RD_station_trace",self.view.tabs[tab].rdStationTraceTile,plotWidth,plotHeight);
      });
      $(".rd-station-trace",self.view.tabs[tab]).attr("id",tab+"_rdStationTrace"+self.view.id);
      self.view.tabs[tab].rdTraceTimeRange=$(".rd-trace-time-range",self.view.tabs[tab].rdStationTraceTile);
      self.view.tabs[tab].rdTraceTimeRange.slider({
        min: -3000,
        max: 3000,
        step: 100,
        range: true,
        value: [-500,500]
      });
      self.view.tabs[tab].rdTraceTimeRange.on("slideStop",function(){
        self.updateRdStationTrace(tab,-1,true);
      });
      self.view.tabs[tab].rdStationTraceTile.on("resizestop",function(){
        self.updateRdStationTrace(tab,-1,false);
      });
      if(self.view.tabs[tab].activeStation==null) {
        self.updateRdStationTrace(tab, 0, true);
      }else{
        self.updateRdStationTrace(tab,self.view.tabs[tab].activeStation,true);
      }
      $(".rd-station-trace-settings",self.view.tabs[tab].rdStationTraceTile).bind("show",function(){
        $(".show-rd-station-trace-plot-options",self.view.tabs[tab].rdStationTraceTile).hide();
      });
    },

    updateRdStationTrace: function(tab,id,changeZoom) {
      var self = this;
      self.view.tabs[tab].rdStationTraceTile.data("changeZoom",changeZoom);
      if (id != -1) {
        self.view.tabs[tab].activeStation = id;
      }
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].rdStationTraceTile);
      var plotWidth = plotSize[0];
      var plotHeight =plotSize[1];
      var timeRange = self.view.tabs[tab].rdTraceTimeRange.slider("getValue");
      var plotParameters = {
        "plot_name": "RD_station_trace",
        "station_id": self.view.tabs[tab].activeStation,
        "time_range":timeRange,
        "tab": tab
      };
      if ((plotWidth > 100) && (plotHeight > 100)) {
        $(".panel-overlay", self.view.tabs[tab].rdStationTraceTile).show();
        $(".rd-station-trace-station", self.view.tabs[tab].rdStationTraceTile).attr("data-value", self.view.tabs[tab].activeStation);
        self.view.GET("get_plotly", JSON.stringify(plotParameters));
      }
    },


    renderRdStationTrace: function(tab,ret){
      var self=this;
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].rdStationTraceTile);
      var plotWidth=plotSize[0];
      var plotHeight=plotSize[1];
      var changeZoom=self.view.tabs[tab].rdStationTraceTile.data("changeZoom");
      var zoomLevel=self.view.getZoomLevel(self.view.tabs[tab].rdStationTraceTile, changeZoom);
      if(ret!=null) {
        var data=[];
        for(var i=0;i<ret.signal_traces.length;i++){
          if(i<=2){
            var visible="true";
          }else{
            var visible="legendonly";
          }
          data.push({
            x: ret.time_trace,
            y: ret.signal_traces[i],
            name: ret.labels[i],
            visible: visible
          });
        }
        var shapes=[];
        console.log(ret.noise_window);
        console.log(ret.signal_window);
        if((ret.noise_window[0]!=null)&&(ret.noise_window[1]!=null)){
          shapes.push({
            type: "rect",
            x0:ret.noise_window[0],
            x1: ret.noise_window[1],
            yref: "paper",
            y0: 0,
            y1: 1,
            fillcolor: "red",
            opacity:.1
          });
        }
        if((ret.signal_window[0]!=null)&&(ret.signal_window[1]!=null)){
          shapes.push({
            type: "rect",
            x0:ret.signal_window[0],
            x1: ret.signal_window[1],
            yref: "paper",
            y0: 0,
            y1: 1,
            fillcolor: "green",
            opacity:.1
          });
        }
        var layout= {
          hovermode: "closest",
          width: plotWidth,
          height: plotHeight,
          legend: {
            x:1.,
            y:.99,
            bgcolor: "transparent",
            bordercolor: "black",
            borderwidth: 1
          },
          xaxis:{
            title: "time [ns]",
            range: [ret.time_trace[0],ret.time_trace[ret.time_trace-length-1]]
          },
          yaxis:{
            title: "signal [micro V]"
          },
          margin:{
            l:40,
            r:0,
            t:20,
            b:40,
            pad:0
          },
          shapes: shapes
        };
        d3.select("#" + tab + "_rdStationTrace"+self.view.id).selectAll("*").remove();
        plotly.newPlot(tab+"_rdStationTrace"+self.view.id,data,layout,{modeBarButtonsToRemove: ["sendDataToCloud","hoverCompareCartesian"], showlink: false});
      }else{
        $(".rd-station-trace",self.view.tabs[tab]).html("<h3>No Data to display</h3>")
      }
      $(".panel-overlay",self.view.tabs[tab].rdStationTraceTile).hide();
    },

    addRdChannelTrace: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].rdChannelTraceTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(rdStationChannelTraceTemplate,x,y,width,height);
        self.view.tabs[tab].rdChannelTraceTile=$(".rd-station-channel-trace-tile",self.view.tabs[tab]);
        self.drawRdChannelTrace(tab);
      }else{
        console.log("rdChannelTrace already exists");
      }
    },

    drawRdChannelTrace: function(tab){
      var self=this;
      $(".remove-rd-station-channel-trace", self.view.tabs[tab]).click(function () {
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].rdChannelTraceTile);
        self.view.tabs[tab].rdChannelTraceTile = null;
      });
      self.view.GET("get_rd_channel_length",JSON.stringify({
        "station_id": self.view.tabs[tab].activeStation
      }),function(err,res) {
        if (res>0) {
          $(".save-plot", self.view.tabs[tab].rdChannelTraceTile).click(function () {
            var plotSize = self.view.getPlotSize(tab, self.view.tabs[tab].rdChannelTraceTile);
            var plotWidth = plotSize[0];
            var plotHeight = plotSize[1];
            self.view.savePlot("RD_channel_trace", self.view.tabs[tab].rdChannelTraceTile, plotWidth, plotHeight);
          });
          $(".rd-station-channel-trace", self.view.tabs[tab]).attr("id", tab + "_rdChannelTrace" + self.view.id);
          $(".rd-pol", self.view.tabs[tab].rdChannelTraceTile).click(function () {
            ///argument -1 means that the same station as before is drawn
            self.updateRdChannelTrace(tab, -1, true);
          });
          $(".update-rd-channel-trace", self.view.tabs[tab].rdChannelTraceTile).click(function () {
            self.updateRdChannelTrace(tab, -1, true);
          });
          $(".rd-channel-trace-range", self.view.tabs[tab].rdChannelTraceTile).slider({
            min: 0,
            max: Math.ceil(res / 500) * 500,
            step: 500,
            range: true,
            value: [0, Math.ceil(res / 500) * 500]
          });
          self.view.tabs[tab].rdChannelTraceTile.on("resizestop", function () {
            self.updateRdChannelTrace(tab, -1, false);
          });
          if (self.view.tabs[tab].activeStation == null) {
            self.updateRdChannelTrace(tab, 0, true);
          } else {
            self.updateRdChannelTrace(tab, self.view.tabs[tab].activeStation, true);
          }
        }else{
          $(".rd-station-channel-trace",self.view.tabs[tab].rdChannelTraceTile).html("<h3>No Data to display</h3>");
          $(".rd-station-channel-trace", self.view.tabs[tab]).attr("id", tab + "_rdChannelTrace" + self.view.id);
          $(".rd-channel-trace-range", self.view.tabs[tab].rdChannelTraceTile).slider({
            min: 0,
            max: 1000,
            step: 100,
            range: true,
            value: [0,1000]
          });
        }
      });
    },

    updateRdChannelTrace: function(tab,id,changeZoom) {
      var self = this;
      self.view.tabs[tab].rdChannelTraceTile.data("changeZoom",changeZoom);
      if (id != -1) {
        self.view.tabs[tab].activeStation = id;
      }
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].rdChannelTraceTile);
      var plotWidth = plotSize[0];
      var plotHeight = plotSize[1];
      var xRange=$(".rd-channel-trace-range", self.view.tabs[tab].rdChannelTraceTile).slider("getValue");
      var plotParameters = {
        "plot_name": "RD_channel_trace",
        "plot_width": plotWidth,
        "plot_height": plotHeight,
        "zoom_level": null,
        "station_id": self.view.tabs[tab].activeStation,
        "x_range": [xRange[0],xRange[1]],
        "tab": tab
      };
      var parName;
      var parValue;
      var checkBoxes = $(".plot-bool", self.view.tabs[tab].rdChannelTraceTile);
      checkBoxes.each(function () {
        parName = this.getAttribute("data-parameter");
        parValue = $(this).prop("checked");
        plotParameters[parName] = parValue;
      });
      if ((plotWidth > 100) && (plotHeight > 100)) {
        $(".panel-overlay", self.view.tabs[tab].rdChannelTraceTile).show();
        $(".rd-station-channel-trace-station", self.view.tabs[tab].rdChannelTraceTile).attr("data-value", self.view.tabs[tab].activeStation);
        self.view.GET("get_plot", JSON.stringify(plotParameters));
      }
    },


    renderRdChannelTrace: function(tab,data){
      var self=this;
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].rdChannelTraceTile);
      var plotWidth=plotSize[0];
      var plotHeight=plotSize[1];
      var changeZoom=self.view.tabs[tab].rdChannelTraceTile.data("changeZoom");
      var zoomLevel=self.view.getZoomLevel(self.view.tabs[tab].rdChannelTraceTile, changeZoom);
      if(data!=null) {
        var commands = data;
        commands["width"] = plotWidth;
        commands["height"] = plotHeight;
        d3.select("#" + tab + "_rdChannelTrace"+self.view.id).selectAll("*").remove();
        self.view.tabs[tab].rdChannelTraceTile.graph=mpld3.draw_figure(tab + "_rdChannelTrace"+self.view.id, commands);
        if(zoomLevel!=null) {
          self.view.tabs[tab].rdChannelTraceTile.graph.axes[0].set_axlim([zoomLevel[0], zoomLevel[1]], [zoomLevel[2], zoomLevel[3]], 0, false);
        }
      }else{
        $(".rd-station-channel-trace",self.view.tabs[tab]).html("<h3>No Data to display</h3>")
      }
      $(".panel-overlay",self.view.tabs[tab].rdChannelTraceTile).hide();
    },

    addRdStationSpectrum: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].rdStationSpectrumTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(rdStationSpectrumTemplate,x,y,width,height);
        self.view.tabs[tab].rdStationSpectrumTile=$(".rd-station-spectrum-tile",self.view.tabs[tab]);
        self.drawRdStationSpectrum(tab);
      }else{
        console.log("rdStationSpectrum already exists");
      }
    },

    drawRdStationSpectrum: function(tab){
    var self=this;
      $(".remove-rd-station-spectrum",self.view.tabs[tab]).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].rdStationSpectrumTile);
        self.view.tabs[tab].rdStationSpectrumTile=null;
      });
      $(".save-plot",self.view.tabs[tab].rdStationSpectrumTile).click(function(){
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].rdStationSpectrumTile);
        var plotHeight=plotSize[0];
        var plotWidth=plotSize[1];
        self.view.savePlot("RD_station_spectrum",self.view.tabs[tab].rdStationSpectrumTile,plotWidth,plotHeight);
      });
      $(".rd-station-spectrum",self.view.tabs[tab]).attr("id",tab+"_rdStationSpectrum"+self.view.id);
      $(".rd-spectrum-pol",self.view.tabs[tab]).click(function(){
        self.updateRdStationSpectrum(tab,-1,false);
      });
      self.view.tabs[tab].rdStationSpectrumTile.on("resizestop",function(){
        self.updateRdStationSpectrum(tab,-1,false);
      });
      if(self.view.tabs[tab].activeStation==null) {
        self.updateRdStationSpectrum(tab, 0, true);
      }else{
        self.updateRdStationSpectrum(tab,self.view.tabs[tab].activeStation,true);
      }
  },

    updateRdStationSpectrum: function(tab,id,changeZoom) {
      var self = this;
      self.view.tabs[tab].rdStationSpectrumTile.data("changeZoom",changeZoom);
      if (id != -1) {
        self.view.tabs[tab].activeStation = id;
      }
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].rdStationSpectrumTile);
      var plotWidth = plotSize[0];
      var plotHeight = plotSize[1];
      var plotParameters = {
        "plot_name": "RD_station_spectrum",
        "plot_width": plotWidth,
        "plot_height": plotHeight,
        "zoom_level": null,
        "station_id": self.view.tabs[tab].activeStation,
        "tab": tab
      };
      var parName;
      var parValue;
      var checkBoxes = $(".plot-bool", self.view.tabs[tab].rdStationSpectrumTile);
      checkBoxes.each(function () {
        parName = this.getAttribute("data-parameter");
        parValue = $(this).prop("checked");
        plotParameters[parName] = parValue;
      });
      if ((plotHeight > 100) && (plotWidth > 100)) {
        $(".panel-overlay", self.view.tabs[tab].rdStationSpectrumTile).show();
        self.view.GET("get_plot", JSON.stringify(plotParameters));
      }
    },

    renderRdStationSpectrum: function(tab,data){
      var self=this;
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].rdStationSpectrumTile);
      var plotWidth=plotSize[0];
      var plotHeight=plotSize[1];
      var changeZoom=self.view.tabs[tab].rdStationSpectrumTile.data("changeZoom");
      $(".rd-station-spectrum-station",self.view.tabs[tab].rdStationSpectrumTile).attr("data-value",self.view.tabs[tab].activeStation);
      var zoomLevel=self.view.getZoomLevel(self.view.tabs[tab].rdStationSpectrumTile, changeZoom);
      if (data != null) {
        d3.select("#"+tab+"_rdStationSpectrum"+self.view.id).selectAll("*").remove();
        var commands = data;
        commands["height"] = plotHeight;
        commands["width"] = plotWidth;
        self.view.tabs[tab].rdStationSpectrumTile.graph=mpld3.draw_figure(tab+"_rdStationSpectrum"+self.view.id, commands);
        if(zoomLevel!=null) {
          self.view.tabs[tab].rdStationSpectrumTile.graph.axes[0].set_axlim([zoomLevel[0], zoomLevel[1]], [zoomLevel[2], zoomLevel[3]], 0, false);
        }
      }else{
        $(".rd-station-spectrum",self.view.tabs[tab]).html("<h3>No Data to display</h3>")
      }
      $(".panel-overlay",self.view.tabs[tab].rdStationSpectrumTile).hide();
    },

    addRdLDF: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].rdLDFTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(rdLDFTemplate,x,y,width,height);
        self.view.tabs[tab].rdLDFTile=$(".rd-ldf-tile",self.view.tabs[tab]);
        self.view.tabs[tab].activeRdLDF="RD_2DLDF";
        self.drawRdLDF(tab);
      }else{
        console.log("rdLDF already exists");
      }
    },

    drawRdLDF: function(tab){
    var self=this;
      $(".rd-ldf-setting",self.view.tabs[tab].rdLDFTile).prop("name",tab+"-rd-ldf-dimension"+self.view.id);
      $(".remove-rd-ldf",self.view.tabs[tab]).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].rdLDFTile);
        self.view.tabs[tab].rdLDFTile=null;
      });
      $(".save-plot",self.view.tabs[tab].rdLDFTile).click(function(){
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].rdLDFTile);
        var plotHeight=plotSize[1];
        var plotWidth=plotSize[0];
        self.view.savePlot("rd_ldf",self.view.tabs[tab].rdLDFTile,plotWidth,plotHeight);
      });
      $(".rd-ldf-plot",self.view.tabs[tab]).attr("id",tab+"_rdldfplot"+self.view.id);
      self.view.tabs[tab].rdLDFTile.on("resizestop",function(){
        self.updateRdLDF(tab,self.view.tabs[tab].activeRdLDF,false);
      });
      $(".rd-ldf-setting",self.view.tabs[tab].rdLDFTile).click(function(){
        self.updateRdLDF(tab,true);
        if($(".rd-ldf-oned",self.view.tabs[tab].rdLDFTile).prop("checked")){
          $(".rd-ldf-polarization",self.view.tabs[tab].rdLDFTile).hide();
          $(".rd-ldf-polarization-label",self.view.tabs[tab].rdLDFTile).hide();
        }else{
          $(".rd-ldf-polarization",self.view.tabs[tab].rdLDFTile).show();
          $(".rd-ldf-polarization-label",self.view.tabs[tab].rdLDFTile).show();
        }
      });
      self.updateRdLDF(tab,self.view.tabs[tab].activeRdLDF);
  },

    updateRdLDF: function(tab,changeZoom) {
      var self = this;
      self.view.tabs[tab].rdLDFTile.data("changeZoom",changeZoom);
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].rdLDFTile);
      var plotHeight = plotSize[1];
      var plotWidth = plotSize[0];
      if ((plotWidth > 100) && (plotHeight > 100)) {
        $(".panel-overlay", self.view.tabs[tab].rdLDFTile).show();
        self.view.GET("get_plot", JSON.stringify({
          'plot_name': "rd_ldf",
          'plot_height': plotHeight,
          'plot_width': plotWidth,
          'two_d': $(".rd-ldf-twod", self.view.tabs[tab].rdLDFTile).prop("checked"),
          'one_d': $(".rd-ldf-oned", self.view.tabs[tab].rdLDFTile).prop("checked"),
          'polarization': $(".rd-ldf-polarization",self.view.tabs[tab].rdLDFTile).prop("checked"),
          "zoom_level": null,
          "tab": tab
        }));
      }
    },

    renderRdLDF: function(tab,data){
      var self=this;
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].rdLDFTile);
      var plotWidth=plotSize[0];
      var plotHeight=plotSize[1];
      var changeZoom=self.view.tabs[tab].rdLDFTile.data("changeZoom");
      var zoomLevel = self.view.getZoomLevel(self.view.tabs[tab].rdLDFTile, changeZoom);
      if (data != null) {
        d3.select("#" + tab + "_rdldfplot"+self.view.id).selectAll("*").remove();
        var commands = data;
        commands["height"] = plotHeight;
        commands["width"] = plotWidth;
        !function (mpld3) {
          mpld3.register_plugin("identifyStations", identifyStations);
          identifyStations.prototype = Object.create(mpld3.Plugin.prototype);
          identifyStations.prototype.constructor = identifyStations;
          identifyStations.requiredProps = ["id", "station_numbers"];
          function identifyStations(fig, props) {
            mpld3.Plugin.call(this, fig, props);
          }
          identifyStations.prototype.draw = function () {
            var obj = mpld3.get_element(this.props.id[0]);
            var station_ids = this.props.id[1];
            obj.elements().on("mousedown", function (d, i) {
              self.selectRdStation(tab, station_ids[i])
            });
          };
        }(mpld3);
        self.view.tabs[tab].rdLDFTile.graph = mpld3.draw_figure(tab + "_rdldfplot"+self.view.id, commands);
        if (zoomLevel != null) {
          self.view.tabs[tab].rdLDFTile.graph.axes[0].set_axlim([zoomLevel[0], zoomLevel[1]], [zoomLevel[2], zoomLevel[3]], 0, false);
        }else{
          var ax=self.view.tabs[tab].rdLDFTile.graph.axes[0];
          var left=ax.x.invert(0);
          var right=ax.x.invert(ax.width);
          var top=ax.y.invert(0);
          var bottom=ax.y.invert(ax.height);
          var center=[(left+right)/2.0,(bottom+top)/2.0];
          ax.set_axlim([center[0]-(center[0]-left)/5.0,center[0]+(right-center[0])/5.0 ], [center[1]-(center[1]-bottom)/5.0,center[1]+(top-center[1])/5.0], 0, false);
         }
      } else {
        $(".rd-ldf-plot", self.view.tabs[tab]).html("<h3>No data to display</h3>");
      }
      $(".panel-overlay",self.view.tabs[tab].rdLDFTile).hide();
    },

    addAeraMap: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].aeraMapTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(aeraMapTemplate,x,y,width,height );
        self.view.tabs[tab].aeraMapTile=$(".aera-map-tile",self.view.tabs[tab]);
        if(self.view.tabs[tab].rdStationListTile!=null){
          $(".locate-rd-station",self.view.tabs[tab].rdStationListTile).show();
        }
        self.drawAeraMap(tab);
      }else{
        console.log("aeramap already exists");
      }
    },

    drawAeraMap: function(tab){
      var self=this;
      $(".remove-aera-map",self.view.tabs[tab]).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].aeraMapTile);
        self.view.tabs[tab].aeraMapTile=null;
        if(self.view.tabs[tab].rdStationListTile!=null){
          $(".locate-rd-station",self.view.tabs[tab].rdStationListTile).hide();
        }
      });
      $(".save-plot",self.view.tabs[tab].aeraMapTile).click(function(){
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].aeraMapTile);
        var plotHeight=plotSize[0];
        var plotWidth=plotSize[1];
        self.view.savePlot("AERA_map",self.view.tabs[tab].aeraMapTile,plotWidth,plotHeight);
      });
      $(".aera-map-plot",self.view.tabs[tab]).attr("id",tab+"_aeraMapPlot"+self.view.id);
      self.view.tabs[tab].aeraMapTile.on("resizestop",function(){
        self.updateAeraMap(tab,false);
      });
      var drawSignal=$(".aera-map-draw-signal",self.view.tabs[tab]);
      var drawTiming=$(".aera-map-draw-timing",self.view.tabs[tab]);
      drawSignal.click(function(){
        if(drawTiming.prop("checked")==true){
          drawTiming.prop("checked",false);
        }
        self.updateAeraMap(tab,false);
      });
      drawTiming.click(function(){
        if(drawSignal.prop("checked")==true){
          drawSignal.prop("checked",false);
        }
        self.updateAeraMap(tab,false);
      });
      $(".aera-map-draw-sd",self.view.tabs[tab]).click(function(){
        self.updateAeraMap(tab,false);
      });
      $(".aera-map-draw-shower-axis",self.view.tabs[tab]).click(function(){
        self.updateAeraMap(tab,false);
      });
      $(".aera-map-draw-ldf",self.view.tabs[tab]).click(function(){
        self.updateAeraMap(tab,false);
      });
      $(".aera-map-draw-vxB-vxvxB-axis",self.view.tabs[tab]).click(function(){
        self.updateAeraMap(tab,false);
      });
      self.updateAeraMap(tab,false);
    },

    updateAeraMap: function(tab,changeZoom) {
      var self = this;
      self.view.tabs[tab].aeraMapTile.data("changeZoom",changeZoom);
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].aeraMapTile);
      var plotWidth = plotSize[0];
      var plotHeight =plotSize[1];
      var plotSignal = $(".aera-map-draw-signal", self.view.tabs[tab]).prop("checked");
      var plotTiming = $(".aera-map-draw-timing", self.view.tabs[tab]).prop("checked");
      var drawSD = $(".aera-map-draw-sd", self.view.tabs[tab]).prop("checked");
      var drawShowerAxis = $(".aera-map-draw-shower-axis", self.view.tabs[tab]).prop("checked");
      var drawLDF=$(".aera-map-draw-ldf",self.view.tabs[tab]).prop("checked");
      var drawvxBvxvxBAxis=$(".aera-map-draw-vxB-vxvxB-axis",self.view.tabs[tab]).prop("checked");
      if ((plotHeight > 100) && (plotWidth > 100)) {
        $(".panel-overlay", self.view.tabs[tab].aeraMapTile).show();
        self.view.GET("get_plot", JSON.stringify({
          "plot_name": "AERA_map",
          "plot_height": plotHeight,
          "plot_width": plotWidth,
          "plot_signal": plotSignal,
          "plot_timing": plotTiming,
          "draw_sd": drawSD,
          "draw_shower_axis": drawShowerAxis,
          "draw_ldf": drawLDF,
          "vxb_vxvxb_axis": drawvxBvxvxBAxis,
          "zoom_level": null,
          "tab": tab
        }));
      }
    },

    renderAeraMap: function(tab,data){
      var self=this;
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].aeraMapTile);
      var plotWidth=plotSize[0];
      var plotHeight=plotSize[1];
      var changeZoom=self.view.tabs[tab].aeraMapTile.data("changeZoom");
      var zoomLevel=self.view.getZoomLevel(self.view.tabs[tab].aeraMapTile,changeZoom);
      if(data!=null){
        d3.select("#"+tab+"_aeraMapPlot"+self.view.id).selectAll("*").remove();
        var commands=data;
        commands["width"]=plotWidth;
        commands["height"]=plotHeight;
        !function(mpld3) {
          mpld3.register_plugin("identifyStations", identifyStations);
          identifyStations.prototype = Object.create(mpld3.Plugin.prototype);
          identifyStations.prototype.constructor = identifyStations;
          identifyStations.requiredProps=["id","station_numbers"];
          function identifyStations(fig, props) {
            mpld3.Plugin.call(this, fig, props);
          }
          identifyStations.prototype.draw = function () {
            var obj=mpld3.get_element(this.props.id[0]);
            var station_ids=this.props.id[1];
            obj.elements().on("mousedown",function(d,i){
              self.selectRdStation(tab,station_ids[i])
            });
          };
        }(mpld3);
        self.view.tabs[tab].aeraMapTile.graph=mpld3.draw_figure(tab+"_aeraMapPlot"+self.view.id,commands);
        if(zoomLevel!=null){
          self.view.tabs[tab].aeraMapTile.graph.axes[0].set_axlim([zoomLevel[0],zoomLevel[1]],[zoomLevel[2],zoomLevel[3]],0,false);
        }
      }else{
        $(".aera-map-plot",self.view.tabs[tab]).html("<h3>No data to display</h3>")
      }
      $(".panel-overlay",self.view.tabs[tab].aeraMapTile).hide();
    },

    locateRdStation: function(tab,id){
      var self=this;
      var aeraMapTile=self.view.tabs[tab].aeraMapTile;
      if(aeraMapTile!=null){
        self.view.GET("get_rd_station_position",JSON.stringify({
          "station_id": id
        }),function(err,ret){
          if(ret!=null){
            var zoomLevel=self.view.getZoomLevel(aeraMapTile,false);
            var xLength=.5*(zoomLevel[1]-zoomLevel[0]);
            var yLength=.5*(zoomLevel[3]-zoomLevel[2]);
            self.view.tabs[tab].aeraMapTile.graph.axes[0].set_axlim([ret[0] - xLength,ret[0] +xLength],[ret[1]-yLength,ret[1]+yLength],1000,true);
          }
        });
      }
    },

    addRdTimeResiduals: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].rdTimeResidualsTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(rdTimeResidualsTemplate,x,y,width,height);
        self.view.tabs[tab].rdTimeResidualsTile=$(".rd-time-residuals-tile",self.view.tabs[tab]);
        $(".rd-time-residuals-plot",self.view.tabs[tab]).attr("id",tab+"_rdtimeresidualsplot"+self.view.id);
        self.updateRdTimeResiduals(tab,false);
        $(".remove-rd-time-residuals",self.view.tabs[tab]).click(function(){
          self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].rdTimeResidualsTile);
          self.view.tabs[tab].rdTimeResidualsTile=null;
        });
        $(".save-plot",self.view.tabs[tab].rdTimeResidualsTile).click(function(){
          var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].rdTimeResidualsTile);
          var plotHeight=plotSize[0];
          var plotWidth=plotSize[1];
          self.view.savePlot("RD_time_residuals",self.view.tabs[tab].rdTimeResidualsTile,plotWidth,plotHeight);
        });
        self.view.tabs[tab].rdTimeResidualsTile.on("resizestop",function(){
          self.updateRdTimeResiduals(tab,false);
        });$(".rd-time-residuals-axis",self.view.tabs[tab].rdTimeResidualsTile).click(function(){
          $(".rd-time-residuals-axis", self.view.tabs[tab].rdTimeResidualsTile).prop("checked", false);
          $(this).prop("checked", true);
          self.updateRdTimeResiduals(tab,false);
        });
      }else{
        console.log("rdTimeResiduals already exists");
      }
    },


    updateRdTimeResiduals: function(tab,changeZoom) {
      var self = this;
      self.view.tabs[tab].rdTimeResidualsTile.data("changeZoom",changeZoom);
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].rdTimeResidualsTile);
      var plotWidth = plotSize[0];
      var plotHeight = plotSize[1];
      var rdAxis=$(".rd-time-residuals-rd-axis",self.view.tabs[tab].rdTimeResidualsTile).prop("checked");
      var sdAxis=$(".rd-time-residuals-sd-axis",self.view.tabs[tab].rdTimeResidualsTile).prop("checked");
      var mcAxis=$(".rd-time-residuals-mc-axis",self.view.tabs[tab].rdTimeResidualsTile).prop("checked");
      if ((plotHeight > 100) && (plotWidth > 100)) {
        $(".panel-overlay", self.view.tabs[tab].rdTimeResidualsTile).show();
        var axis = $(".select-axis", self.view.tabs[tab]).val();
        self.view.GET("get_plot", JSON.stringify({
          "plot_name": "RD_time_residuals",
          "plot_width": plotWidth,
          "plot_height": plotHeight,
          "rd_axis": rdAxis,
          "sd_axis": sdAxis,
          "mc_axis": mcAxis,
          "zoom_level": null,
          "tab": tab
        }));
      }
    },

    renderRdTimeResiduals: function(tab,data){
      var self=this;
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].rdTimeResidualsTile);
      var plotWidht=plotSize[0];
      var plotHeight=plotSize[1];
      var changeZoom=self.view.tabs[tab].rdTimeResidualsTile.data("changeZoom");
      var zoomLevel=self.view.getZoomLevel(self.view.tabs[tab].rdTimeResidualsTile,changeZoom);
      if(data!=null) {
        d3.select("#"+tab+"_rdtimeresidualsplot"+self.view.id).selectAll("*").remove();
        var commands = data;
        commands["width"] = plotWidth;
        commands["height"] = plotHeight;
        !function(mpld3) {
          mpld3.register_plugin("identifyStations", identifyStations);
          identifyStations.prototype = Object.create(mpld3.Plugin.prototype);
          identifyStations.prototype.constructor = identifyStations;
          identifyStations.requiredProps=["id","station_numbers"];
          function identifyStations(fig, props) {
            mpld3.Plugin.call(this, fig, props);
          }
          identifyStations.prototype.draw = function () {
            var obj=mpld3.get_element(this.props.id[0]);
            var station_ids=this.props.id[1];
            obj.elements().on("mousedown",function(d,i){
              self.selectRdStation(tab,station_ids[i])
            });
          };
        }(mpld3);
        self.view.tabs[tab].rdTimeResidualsTile.graph=mpld3.draw_figure(tab+"_rdtimeresidualsplot"+self.view.id, commands);
        if(zoomLevel!=null){
          self.view.tabs[tab].rdTimeResidualsTile.graph.axes[0].set_axlim([zoomLevel[0],zoomLevel[1]],[zoomLevel[2],zoomLevel[3]],0,false);
        }
      }else{
        d3.select("#"+tab+"_rdtimeresidualsplot"+self.view.id).selectAll("*").remove();
        $(".rd-time-residuals-plot",self.view.tabs[tab]).html("<h3>No data to display</h3>");
      }
      $(".panel-overlay",self.view.tabs[tab].rdTimeResidualsTile).hide();
    },

    selectRdStation: function(tab,id){
      var self=this;
      self.view.tabs[tab].activeStation=id;
      if(self.view.tabs[tab].rdStationListTile!=null){
        $(self.view.tabs[tab].activeStationListItem).attr("class", self.view.tabs[tab].previousStationClass);
        $(".rd-stations-table-row",self.view.tabs[tab].rdStationListTile).each(function(){
          if(this.rowId==id){
            self.view.tabs[tab].activeStationListItem=$(this);
          }
        });
        self.view.tabs[tab].previousStationClass=$(self.view.tabs[tab].activeStationListItem).attr("class");
        $(self.view.tabs[tab].activeStationListItem).removeClass("default").removeClass("success").addClass("info");
      }
      if(self.view.tabs[tab].rdStationInfoTile!=null){
        self.updateRdStationInfo(tab,id);
      }
      if(self.view.tabs[tab].rdStationSpectrumTile!=null){
        self.updateRdStationSpectrum(tab,id,false);
      }
      if (self.view.tabs[tab].rdStationTraceTile!=null){
        self.updateRdStationTrace(tab,id,true);
      }
      if(self.view.tabs[tab].rdChannelTraceTile!=null){
        self.updateRdChannelTrace(tab,id,true)
      }
    }


  });
  return rdFunctions;
});