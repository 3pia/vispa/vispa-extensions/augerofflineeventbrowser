require.config({
  paths: {
    "mpld3": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/mpld3.v0.2"),
    "augeroffline_d3": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/d3.v3.min"),
    "plotly": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/plotly/dist/plotly"),
    "gridstack": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/gridstack/gridstack"),
    "lodash": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/lodash/lodash")
    },
  shim: {
    gridstack: ["jquery", "lodash"],
    mpld3: {
      exports: "mpld3"
    }
  }
});
define([ "jclass",
  "async",
  "jquery",
  "augeroffline_d3",
  "plotly",
  "text!../html/event_histogram.html",
  "text!../html/event_selection.html",
  "text!../html/event_categories.html",
  "text!../html/file_list.html",
  "text!../html/offline_configuration.html",
  "text!../html/incoming_directions.html",
  "text!../html/event_display.html",
  "gridstack",
  "css!./../css/augerofflineeventbrowser",
  "css!./../vendor/gridstack/gridstack.css" ], function(jClass, async, $, d3,plotly, eventHistogramTemplate,
                                                        eventSelectionTemplate,eventCategoriesTemplate,fileListTemplate,
                                                        offlineConfigurationTemplate,incomingDirectionsTemplate,
                                                        eventDisplayTemplate) {
  var ovFunctions = jClass._extend({
    init: function init(view) {
      var self = this;
      init._super.call(this, arguments);
      self.view = view;
    },
    addEventOverview: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].eventOverviewTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(eventHistogramTemplate,x,y,width,height);
        self.view.tabs[tab].eventOverviewTile=$(".event-overview-tile",self.view.tabs[tab]);
        self.drawEventOverview(tab);
      }else{
        console.log("eventOverview already exists");
      }
    },

    makeEventOverviewRemovable: function(tab){
      var self=this;
      $(".remove-event-overview",self.view.tabs[tab]).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].eventOverviewTile);
        self.view.tabs[tab].eventOverviewTile=null;
      });
    },

    drawEventOverview: function(tab){
      var self=this;
      self.makeEventOverviewRemovable(tab);
      $(".event-overview",self.view.tabs[tab]).attr("id",tab+"_eventOverview"+self.view.id);
      var visibleConditionsForm=$(".conditions-form",self.view.tabs[tab]);
      $(".select-property",self.view.tabs[tab]).selectpicker();
      $(".select-condition",visibleConditionsForm).selectpicker();
      self.updateEventOverview(tab,"sdEnergy");
      self.view.tabs[tab].eventOverviewTile.on("resizestop",function(){
        self.updateEventOverview(tab,$(".select-property",self.view.tabs[tab]).val());
        self.view.tabs[tab].eventOverviewTile.settingsHeight=$(".event-overview-settings",self.view.tabs[tab])[0].scrollHeight;
      });
      $(".add-condition",self.view.tabs[tab]).click(function(){
        var itm=$(this).parents(".conditions-form").clone(true);
        itm.find(".select-limits").val(null);
        var conditionSelector=itm.find(".selectpicker");
        conditionSelector.data("selectpicker",null);
        itm.find(".bootstrap-select").remove();
        conditionSelector.selectpicker();
        $(".event-overview-settings",self.view.tabs[tab]).append(itm);
        $(this).toggle();
        $(this).parent().children(".remove-condition").toggle();
        self.updateEventOverview(tab);
      });
      $(".remove-condition",self.view.tabs[tab]).click(function(){
        $(this).parents(".conditions-form").remove();
        self.updateEventOverview(tab);
      });
      $(".refresh-event-overview",self.view.tabs[tab]).click(function(){
        self.updateEventOverview(tab);
      });
      $(".select-property",self.view.tabs[tab]).on("change",function(){
        self.updateEventOverview(tab);
      })
    },

    updateEventOverview: function(tab){
      var self=this;
      var conditions=[];
      var limits=[];
      $(".selectpicker.select-condition",self.view.tabs[tab]).each(function(){
        conditions.push($(this).val());
      });
      $(".select-limits",self.view.tabs[tab]).each(function(){
        limits.push($(this).val());
      });
      if(self.view.tabs[tab].eventOverviewTile.settingsHeight==null){
        self.view.tabs[tab].eventOverviewTile.settingsHeight=$(".event-overview-settings",self.view.tabs[tab])[0].scrollHeight;
      }
      var property=$(".select-property",self.view.tabs[tab]).val();
      var tile=self.view.tabs[tab].eventOverviewTile.data("_gridstack_node");
      var plotHeight=tile.height*self.view.gridstackNodes[tab].data("gridstack").cell_height()-$(".event-overview-panel-heading",self.view.tabs[tab])[0].scrollHeight-
        self.view.tabs[tab].eventOverviewTile.settingsHeight-30;
      var plotWidth=tile.width*self.view.gridstackNodes[tab].data("gridstack").cell_width();
      if((plotHeight>100)&&(plotWidth>100)) {
        $(".panel-overlay",self.view.tabs[tab].eventOverviewTile).show();
        self.view.GET("get_plotly", JSON.stringify({
          "plot_name": "event_overview_histogram",
          "requested_property": property,
          "conditions": conditions,
          "limits": limits,
          "tab": tab
        }));
      }
    },

    renderEventOverview: function(tab,ret){
      var self=this;
      if (ret != null) {
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].eventOverviewTile);
        var plotWidth=plotSize[0];
        var plotHeight=plotSize[1];
        d3.select("#" + tab + "_eventOverview" + self.view.id).selectAll("*").remove();
        var data=[{
          x: ret.data,
          type: "histogram",
          xbins:{
            start:ret.bins_min,
            end: ret.bins_max,
            size: ret.bins_width
          }
        }];
        var layout={
          bargap:.01,
          width: plotWidth,
          height: plotHeight,
          xaxis:{
            dtick:ret.bins_width,
            title: ret.xlabel
          },
          yaxis:{
            title: "entries"
          }

        };
        plotly.newPlot(tab + "_eventOverview" + self.view.id,data,layout,{modeBarButtonsToRemove: ["sendDataToCloud"]});
        $(".panel-overlay", self.view.tabs[tab].eventOverviewTile).hide();
      }else {
        $(".event-overview", self.view.tabs[tab].eventOverviewTile).html("<h3>No Data to display</h3>");
        $(".panel-overlay", self.view.tabs[tab].eventOverviewTile).hide();
      }
    },

    addEventSelection: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].eventSelectionTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(eventSelectionTemplate,x,y,width,height);
        self.view.tabs[tab].eventSelectionTile=$(".event-selection-tile",self.view.tabs[tab]);
        self.drawEventSelection(tab);
      }else{
        console.log("eventSelection already exists");
      }
    },

    makeEventSelectionRemovable: function(tab){
      var self=this;
      $(".remove-event-selection",self.view.tabs[tab]).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].eventSelectionTile);
        self.view.tabs[tab].eventSelectionTile=null;
      })
    },

    drawEventSelection: function(tab){
      var self=this;
      var augerSearchButton=$(".search-auger-id",self.view.tabs[tab].eventSelectionTile);
      var rdSearchButton=$(".search-rd-number",self.view.tabs[tab].eventSelectionTile);
      var sdSearchButton=$(".search-sd-number",self.view.tabs[tab].eventSelectionTile);
      var fdSearchButton=$(".search-fd-number",self.view.tabs[tab].eventSelectionTile);
      self.makeEventSelectionRemovable(tab);
      augerSearchButton.click(function(){
        self.view.POST("go_to_auger_id",JSON.stringify({"auger_id": $(".auger-id",self.view.tabs[tab].eventSelectionTile).val()}),function(err,ret){
          if(ret.success== true){
            for (var index in self.view.tabs) {
                self.view.tabs[index].updateNeeded = true;
            }
            self.view.updateTab(self.view.activeTab,true);
            augerSearchButton.removeClass("btn-default btn-danger").addClass("btn-success");
            rdSearchButton.removeClass("btn-success btn-danger").addClass("btn-default");
            sdSearchButton.removeClass("btn-success btn-danger").addClass("btn-default");
            fdSearchButton.removeClass("btn-success btn-danger").addClass("btn-default");
          }else{
            augerSearchButton.removeClass("btn-default btn-success").addClass("btn-danger");
          }
        });
      });
      rdSearchButton.click(function(){
        var rdRun=$(".rd-run-number",self.view.tabs[tab].eventSelectionTile).val();
        var rdEvent=$(".rd-event-id",self.view.tabs[tab].eventSelectionTile).val();
        if((rdRun!="")&&rdEvent!="") {
          self.view.POST("go_to_rd_event_number", {"rd_run_number": rdRun, "rd_event_id": rdEvent}, function (err, ret) {
            if (ret.success == true) {
              for (var index in self.view.tabs) {
                self.view.tabs[index].updateNeeded = true;
              }
              self.view.updateTab(self.view.activeTab,true);
              rdSearchButton.removeClass("btn-default btn-danger").addClass("btn-success");
              augerSearchButton.removeClass("btn-success btn-danger").addClass("btn-default");
              sdSearchButton.removeClass("btn-success btn-danger").addClass("btn-default");
              fdSearchButton.removeClass("btn-success btn-danger").addClass("btn-default");
            } else {
              rdSearchButton.removeClass("btn-default btn-success").addClass("btn-danger");
            }
          });
        }
      });
      fdSearchButton.click(function(){
        var fdRun=$(".fd-run-number",self.view.tabs[tab].eventSelectionTile).val();
        var fdEvent=$(".fd-event-id",self.view.tabs[tab].eventSelectionTile).val();
        if((fdRun!="")&&fdEvent!="") {
          self.view.POST("go_to_fd_event_number", {"fd_run": fdRun, "fd_id": fdEvent}, function (err, ret) {
            if (ret.success == true) {
              for (var index in self.view.tabs) {
                self.view.tabs[index].updateNeeded = true;
              }
              self.view.updateTab(self.view.activeTab,true);
              fdSearchButton.removeClass("btn-default btn-danger").addClass("btn-success");
              rdSearchButton.removeClass("btn-success btn-danger").addClass("btn-default");
              sdSearchButton.removeClass("btn-success btn-danger").addClass("btn-default");
              augerSearchButton.removeClass("btn-success btn-danger").addClass("btn-default");
            } else {
              fdSearchButton.removeClass("btn-default btn-success").addClass("btn-danger");
            }
          });
        }
      });
      sdSearchButton.click(function(){
        var sdEvent=$(".sd-event-id",self.view.tabs[tab].eventSelectionTile).val();
        if(sdEvent!=""){
          self.view.POST("go_to_sd_event_number", {"sd_event_id": sdEvent},function(err,ret){
            if(ret.success==true){
              for (var index in self.view.tabs) {
                self.view.tabs[index].updateNeeded = true;
              }
              self.view.updateTab(self.view.activeTab,true);
              sdSearchButton.removeClass("btn-default btn-danger").addClass("btn-success");
              rdSearchButton.removeClass("btn-danger btn-success").addClass("btn-default");
              fdSearchButton.removeClass("btn-success btn-danger").addClass("btn-default");
              augerSearchButton.removeClass("btn-success btn-danger").addClass("btn-default");
            }else{
              sdSearchButton.removeClass("btn-default btn-success").addClass("btn-danger");
            }
          })
        }
      });
    },
    addEventCategories: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].eventCategoriesTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(eventCategoriesTemplate,x,y,width,height);
        self.view.tabs[tab].eventCategoriesTile=$(".event-categories-tile",self.view.tabs[tab]);
        self.drawEventCategories(tab);
      }else{
        console.log("eventCategoreis already exists");
      }
    },
    drawEventCategories: function(tab){
      var self=this;
      $(".remove-event-categories",self.view.tabs[tab]).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].eventCategoriesTile);
        self.view.tabs[tab].eventCategoriesTile=null;
      });
      self.updateEventCategories(tab);
    },
    updateEventCategories: function(tab){
      var self=this;
      $(".panel-overlay",self.view.tabs[tab].eventCategoriesTile).show();
      self.view.GET("get_event_categories",JSON.stringify({
        "tab": tab
      }));
    },

    renderEventCategories: function(tab,data){
      var self=this;
      if(data!=null){
        $(".event-categories-table",self.view.tabs[tab].eventCategoriesTile).render(data);
      }
      $(".panel-overlay",self.view.tabs[tab].eventCategoriesTile).hide();
    },

    addFileList: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].fileListTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(fileListTemplate,x,y,width,height);
        self.view.tabs[tab].fileListTile=$(".file-list-tile",self.view.tabs[tab]);
        self.drawFileList(tab);
      }else{
        console.log("fileList already exists");
      }
    },
    drawFileList: function(tab){
      var self=this;
      $(".remove-file-list",self.view.tabs[tab].fileListTile).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].fileListTile);
        self.view.tabs[tab].fileListTile=null;
      });
      self.updateFileList(tab);
    },
    updateFileList: function(tab){
      var self=this;
      $(".panel-overlay",self.view.tabs[tab].fileListTile).show();
      self.view.GET("get_file_list",JSON.stringify({
        "tab": tab
      }));
    },

    renderFileList: function(tab,data){
      var self=this;
      if(data!=null){
        $(".file-list-table",self.view.tabs[tab].fileListTile).render(data);
      }
      $(".panel-overlay",self.view.tabs[tab].fileListTile).hide();
    },

    addOfflineConfiguration: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].offlineConfigurationTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(offlineConfigurationTemplate,x,y,width,height);
        self.view.tabs[tab].offlineConfigurationTile=$(".offline-configuration-tile",self.view.tabs[tab]);
        self.drawOfflineConfiguration(tab);
      }else{
        console.log("offlineConfiguration already exists");
      }
    },

    drawOfflineConfiguration: function(tab){
      var self=this;
      $(".remove-offline-configuration",self.view.tabs[tab].offlineConfigurationTile).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].offlineConfigurationTile);
        self.view.tabs[tab].offlineConfigurationTile=null;
      });
      $(".update-offline-config",self.view.tabs[tab].offlineConfigurationTile).click(function() {
        self.updateOfflineConfiguration(tab);
      });
      self.updateOfflineConfiguration(tab);
  },

    updateOfflineConfiguration: function(tab){
      var self=this;
      $(".panel-overlay",self.view.tabs[tab].offlineConfigurationTile).show();
      var moduleName=$(".offline-configuration-module-name",self.view.tabs[tab].offlineConfigurationTile).val();
      self.view.GET("get_offline_configuration",JSON.stringify({
        "module_name": moduleName,
        "tab": tab
      }));
    },

    renderOfflineConfiguration: function(tab,data){
      var self=this;
      var commands={
        "module-configuration":{
          text: function(){
            return this.module_config;
          }
        },
        "offline-version":{
            text: function(){
              return this.offline_version;
            }
        }
      };
      $(".offline-configuration-table",self.view.tabs[tab].offlineConfigurationTile).render(data,commands);
      $(".panel-overlay",self.view.tabs[tab].offlineConfigurationTile).hide();
    },
    addIncomingDirections: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].incomingDirectionsTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(incomingDirectionsTemplate,x,y,width,height);
        self.view.tabs[tab].incomingDirectionsTile=$(".incoming-directions-tile",self.view.tabs[tab]);
        self.drawIncomingDirections(tab);
      }
    },
    drawIncomingDirections: function(tab){
      var self=this;
      $(".incoming-directions-plot",self.view.tabs[tab]).attr("id",tab+"_incomingDirectionsPlot"+self.view.id);
      $(".remove-incoming-directions",self.view.tabs[tab].incomingDirectionsTile).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].incomingDirectionsTile);
        self.view.tabs[tab].incomingDirectionsTile=null;
      });
      var horizonButton=$(".incoming-directions-horizon",self.view.tabs[tab].incomingDirectionsTile);
      var equatorialButton=$(".incoming-directions-equatorial",self.view.tabs[tab].incomingDirectionsTile);
      var galacticButton=$(".incoming-directions-galactic",self.view.tabs[tab].incomingDirectionsTile);
      var sdButton=$(".incoming-directions-sd",self.view.tabs[tab].incomingDirectionsTile);
      var rdButton=$(".incoming-directions-rd",self.view.tabs[tab].incomingDirectionsTile);
      horizonButton.click(function(){
        equatorialButton.prop("checked",false);
        galacticButton.prop("checked",false);
        self.updateIncomingDirections(tab);
      });
      equatorialButton.click(function(){
        horizonButton.prop("checked",false);
        galacticButton.prop("checked",false);
        self.updateIncomingDirections(tab);
      });
      galacticButton.click(function(){
        horizonButton.prop("checked",false);
        equatorialButton.prop("checked",false);
        self.updateIncomingDirections(tab);
      });
      sdButton.click(function(){
        self.updateIncomingDirections(tab);
      });
      rdButton.click(function(){
        self.updateIncomingDirections(tab);
      });
      self.updateIncomingDirections(tab);
    },
    updateIncomingDirections: function(tab){
      var self=this;
      var horizon=$(".incoming-directions-horizon",self.view.tabs[tab].incomingDirectionsTile).prop("checked");
      var equatorial=$(".incoming-directions-equatorial",self.view.tabs[tab].incomingDirectionsTile).prop("checked");
      var galactic=$(".incoming-directions-galactic",self.view.tabs[tab].incomingDirectionsTile).prop("checked");
      var sd=$(".incoming-directions-sd",self.view.tabs[tab].incomingDirectionsTile).prop("checked");
      var rd=$(".incoming-directions-rd",self.view.tabs[tab].incomingDirectionsTile).prop("checked");
      self.view.GET("get_plotly",JSON.stringify({
        "plot_name": "incoming_directions",
        "galactic_coordinates": galactic,
        "horizon_coordinates": horizon,
        "equatorial_coordinates": equatorial,
        "sd": sd,
        "rd": rd,
        "tab": tab
      }),function(err,ret){

      });
    },
    renderIncomingDirections: function(tab,ret){
      var self=this;
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].incomingDirectionsTile);
      var plotWidth=plotSize[0];
      var plotHeight=plotSize[1];
      if (ret != null) {
        d3.select("#" + tab + "_incomingDirectionsPlot"+self.view.id).selectAll("*").remove();
        var data=[];
        if(ret.coordinates=="hor") {
          if (ret.sd_r.length > 0) {
            data.push({
              r: ret.sd_r,
              t: ret.sd_theta,
              mode: "markers",
              type: "scatter",
              name: "SD Events",
            });
          }
          if (ret.rd_r.length > 0) {
            data.push({
              r: ret.rd_r,
              t: ret.rd_theta,
              mode: "markers",
              type: "scatter",
              name: "RD Events"
            })
          }
          if (data.length > 0) {
            var layout = {
              width: plotWidth,
              height: plotHeight,
              angularaxis:{
                range: [360,0]
              },
              radialaxis:{
                range: [0,90]
              }

            };

          }
        }else if(ret.coordinates=="gal"){
          if (ret.sd_r.length > 0) {
            data.push({
              lat: ret.sd_r,
              lon: ret.sd_theta,
              mode: "markers",
              type: "scattergeo",
              name: "SD Events"
            });
          }
          if (ret.rd_r.length > 0) {
            data.push({
              lat: ret.rd_r,
              lon: ret.rd_theta,
              mode: "markers",
              type: "scattergeo",
              name: "RD Events"
            })
          }
          data.push({
            lon:[0,184.56,121.17],
            lat:[0,-5.78,-21.57],
            mode: "markers",
            type: "scattergeo",
            text: ["Sgr A* (gal. center)", "M1 (crab nebula)","M31 (Andromeda gal.)"],
            name: "astronomical objects"
          });
          if (data.length > 1) {
            var layout = {
              width: plotWidth,
              height: plotHeight,
              geo: {
                projection: {
                  type: "hammer"
                },
                showcountries: false,
                showframe: false,
                showcoastlines:false,
                lonaxis: {
                  showgrid: true
                },
                lataxis: {
                  showgrid: true,
                  showticklabels:true
                },
                showland: false
              },
              legend:{
                x:0,
                y:100
              },
              margin:{
                l:0,
                r:0,
                t:0,
                b:0,
                pad:0
              }
            };
          }
        }
        plotly.newPlot(tab + "_incomingDirectionsPlot" + self.view.id, data,layout,{modeBarButtonsToRemove: ["sendDataToCloud"], showlink: false});
      }else{
        $(".incoming-directions-plot",self.view.tabs[tab].incomingDirectionsTile).html("<h3>No Data to display</h3>")
      }
      $(".panel-overlay",self.view.tabs[tab].incomingDirectionsTile).hide();
    },
    addEventDisplay: function(tab,x,y,width,height){
      var self=this;
      if(self.view.tabs[tab].eventDisplayTile==null){
        self.view.gridstackNodes[tab].data("gridstack").add_widget(eventDisplayTemplate,x,y,width,height);
        self.view.tabs[tab].eventDisplayTile=$(".event-display-tile",self.view.tabs[tab]);
        self.drawEventDisplay(tab);
      }
    },
    drawEventDisplay: function(tab){
      var self=this;
      $(".event-display-plot",self.view.tabs[tab]).attr("id",tab+"_eventDisplayPlot"+self.view.id);
      self.view.tabs[tab].eventDisplayTile.on("resizestop",function(){
          self.updateEventDisplay(tab);
        });
      $(".remove-event-display",self.view.tabs[tab].eventDisplayTile).click(function(){
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].eventDisplayTile);
        self.view.tabs[tab].eventDisplayTile=null;
      });
      self.updateEventDisplay(tab);
    },
    updateEventDisplay: function(tab){
      var self=this;
      self.view.GET("get_plotly",JSON.stringify({
        "plot_name": "event_display",
        "tab": tab
      }));
    },
    renderEventDisplay: function(tab,ret){
      var self=this;
      var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].eventDisplayTile);
      var plotWidth=plotSize[0];
      var plotHeight=plotSize[1];
      var data=[];
      data.push({
        mode: "markers",
        x: ret.non_signal_x,
        y: ret.non_signal_y,
        z: ret.non_signal_z,
        text: ret.non_signal_names,
        marker:{
          size: 1,
          color: "black"
        },
        type: "scatter3d",
        name: "Silent Stations"
      });
      data.push({
        mode: "markers",
        x: ret.candidate_x,
        y: ret.candidate_y,
        z: ret.candidate_z,
        text: ret.candidate_names,
        marker: {
          size:ret.candidate_signal,
          color: "green",
          sizeref:.1,
          opacity:.5
        },
        type: "scatter3d",
        name: "Candidate Stations"
      });
      data.push({
        mode: "lines",
        x:ret.shower_axis_x,
        y:ret.shower_axis_y,
        z:ret.shower_axis_z,
        type: "scatter3d",
        name: "Shower Axis",
        line:{
          color: "black"
        }
      });
      data.push({
        mode: "markers+text",
        x:ret.eye_x,
        y:ret.eye_y,
        z:ret.eye_z,
        text: ret.eye_names,
        name: "FD Telescopes",
        type: "scatter3d",
        marker: {
          size: 2,
          color: "blue"
        },
        font:{
          size:5
        }
      });
      for(var j=0;j<ret.fd_lines_x.length;j++){
        data.push({
          mode: "lines",
          type: "scatter3d",
          name: ret.fov_names[j],
          x: ret.fd_lines_x[j],
          y: ret.fd_lines_y[j],
          z: ret.fd_lines_z[j],
          hoverinfo: "none"
        })
      }
      layout={
        width: plotWidth,
        height: plotHeight,
        legend:{
          x:100,
          y:0
        },
        scene: {
          xaxis:{
            range: [ret.x_center-ret.axis_length *.5 -.1,ret.x_center+ret.axis_length *.5 +.1]
          },
          yaxis:{
            range: [ret.y_center-ret.axis_length *.5 -.1, ret.y_center+ret.axis_length *.5 +.1]
          },
          zaxis: {
            range: [-.1, ret.axis_length - .1]
          },
          aspectratio:{
            x:1,
            y:1,
            z:1
          },
          camera:{
            eye:{
              x:.7,
              y:.7,
              z:.7
            }
          }
        },
        margin:{
          l:0,
          r:0,
          t:0,
          b:0,
          pad:0
        }
      };
      plotly.newPlot(tab+"_eventDisplayPlot"+self.view.id,data,layout,{modeBarButtonsToRemove: ["sendDataToCloud"], showlink: false});
    }
  });
  return ovFunctions;
});