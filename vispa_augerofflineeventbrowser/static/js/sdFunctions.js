require.config({
  paths: {
    "mpld3": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/mpld3.v0.2"),
    "augeroffline_d3": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/d3.v3.min"),
    "gridstack": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/gridstack/gridstack"),
    "lodash": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/lodash/lodash"),
    "plotly": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/plotly/dist/plotly")
  },
  shim: {
    gridstack: ["jquery", "lodash"],
    mpld3: {
      exports: "mpld3"
    }
  }
});
define([ "jclass",
  "async",
  "jquery",
  "augeroffline_d3",
  "plotly",
  "text!../html/sd_event_info.html",
  "text!../html/sd_ldf.html",
  "text!../html/sd_map.html",
  "text!../html/sd_station_list.html",
  "text!../html/sd_time_residuals.html",
  "text!../html/sd_vem_trace.html",
  "text!../html/sd_electrode_traces.html",
  "gridstack",
  "css!./../css/augerofflineeventbrowser",
  "css!./../vendor/gridstack/gridstack.css" ], function(jClass, async, $, d3,plotly,sdEventInfoTemplate,sdLDFTemplate,
                                                        sdMapTemplate,sdStationListTemplate,sdTimeResidualsTemplate,
                                                        sdVemTraceTemplate,sdElectrodeTraceTemplate) {
    var sdFunctions=jClass._extend({
        init: function init(view){
            var self=this;
            init._super.call(this,arguments);
          self.view=view;
        },

      addSdEventInfo: function(tab,x,y,width,height){
      var self=this;
      if (self.view.tabs[tab].sdEventInfoTile==null) {
          self.view.gridstackNodes[tab].data("gridstack").add_widget(sdEventInfoTemplate,x,y,width,height);
          self.view.tabs[tab].sdEventInfoTile = $(".sd-event-info-tile", self.view.tabs[tab]);
          self.drawSdEventInfo(tab);
      }else{
        console.log("sdEventInfo already exists");
      }
    },

    drawSdEventInfo: function(tab) {
      var self = this;
      $(".remove-sd-event-info", self.view.tabs[tab]).unbind().click(function () {
        self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].sdEventInfoTile);
        self.view.tabs[tab].sdEventInfoTile = null;
      });
      $(".panel-overlay", self.view.tabs[tab].sdEventInfoTile).show();
      self.view.GET("get_SD_event_info",JSON.stringify({
        "tab": tab
      }));
    },
      renderSdEventInfo: function(tab,data) {
        var self=this;
        var directives = {
          "sd-energy": {
            text: function () {
              return String(this.energy)[0]+"."+String(this.energy)[1]+String(this.energy)[2] + "e" + String(Math.floor(Math.log10(this.energy)) + 12)+"+/-"+
                  String(this.energyError)[0]+"."+String(this.energyError)[1]+ "e" + String(Math.floor(Math.log10(this.energyError)) + 12);
              }
          },
          "sd-azimuth": {
            text: function () {
              return this.azimuth + "+/-" + this.azimuthError;
            }
          },
          "sd-zenith": {
            text: function () {
              return this.zenith + "+/-" + this.zenithError;
            }
          },
          "sd-time-utc": {
            text: function () {
              return this.utcDate;
            }
          },
          "sd-time-gps": {
            text: function () {
              return this.gpsSecond;
            }
          },
          "sd-event-number": {
            text: function () {
              return this.eventNumber;
            }
          },
          "sd-stations": {
            text: function () {
              return this.stations;
            }
          }
        };
        var table = $(".sd-event-info-table-body", self.view.tabs[tab]);
        table.render(data, directives);
        $(".panel-overlay", self.view.tabs[tab].sdEventInfoTile).hide();
      },

      addSdLDF: function(tab,x,y,width,height){
      var self=this;
      if (self.view.tabs[tab].sdLDFTile==null) {
          self.view.gridstackNodes[tab].data("gridstack").add_widget(sdLDFTemplate,x,y,width,height);
          self.view.tabs[tab].sdLDFTile = $(".sd-ldf-tile", self.view.tabs[tab]);
          self.drawSdLDF(tab);
      }else{
        console.log("sdLDF already exists");
      }
      },

      drawSdLDF: function(tab){
        var self=this;
        $(".remove-sd-ldf",self.view.tabs[tab]).click(function(){
          self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].sdLDFTile);
          self.view.tabs[tab].sdLDFTile=null;
        });
        $(".save-plot",self.view.tabs[tab].sdLDFTile).click(function(){
          var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdLDFTile);
          var plotWidth=plotSize[0];
          var plotHeight=plotSize[1];
          self.view.savePlot("SD_ldf",self.view.tabs[tab].sdLDFTile,plotWidth,plotHeight)
        });
        $(".sd-ldf-plot",self.view.tabs[tab]).attr("id",tab+"_sdLDFplot"+self.view.id);
        self.view.tabs[tab].sdLDFTile.on("resizestop",function(){
          self.updateSdLDF(tab,false);
        });
        $(".sd-ldf-drawing-option",self.view.tabs[tab].sdLDFTile).click(function(){
          self.updateSdLDF(tab,true);
        });
        self.updateSdLDF(tab,false);
      },

      updateSdLDF: function(tab,changeZoom) {
        var self = this;
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdLDFTile);
        var plotWidth=plotSize[0];
        var plotHeight=plotSize[1];
        var residuals = $(".sd-ldf-draw-residuals", self.view.tabs[tab].sdLDFTile).prop("checked");
        self.view.tabs[tab].sdLDFTile.data("changeZoom",changeZoom);
        if ((plotHeight > 100) && (plotWidth > 100)) {
          $(".panel-overlay", self.view.tabs[tab].sdLDFTile).show();
          self.view.GET("get_plotly", JSON.stringify({
            "plot_name": "SD_ldf",
            "draw_residuals": residuals,
            "tab": tab
          }));
        }
      },
      renderSdLDF: function(tab,ret){
        var self=this;
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdLDFTile);
        var plotWidth=plotSize[0];
        var plotHeight=plotSize[1];
        var changeZoom=self.view.tabs[tab].sdLDFTile.data("changeZoom");
        if (ret != null) {
          d3.select("#" + tab + "_sdLDFplot"+self.view.id).selectAll("*").remove();
          var data=[];
          if(ret.signals.length>0){
            data.push({
              mode:"markers",
              x:ret.distances,
              y:ret.signals,
              text: ret.ids,
              marker:{
                color:"black"
              },
              error_x:{
                type:"data",
                array: ret.distances_error,
                visible:true,
                color: "black"
              },
              error_y:{
                type:"data",
                array:ret.signals_error,
                visible: true,
                color: "black"
              },
              name: "signal stations",
              hoverinfo: "text"
            });
          }
          if(ret.signals_saturated.length>0){
            data.push({
              mode:"markers",
              x:ret.distances_saturated,
              y:ret.signals_saturated,
              text: ret.ids_saturated,
              marker:{
                color:"blue"
              },
              error_x:{
                type:"data",
                array: ret.distances_saturated_error,
                visible:true
              },
              error_y:{
                type:"data",
                array:ret.signals_saturated_error,
                visible: true
              },
              name: "saturated stations",
              hoverinfo: "text"
            })
          }
          if(ret.signals_rejected.length>0){
            data.push({
              mode:"markers",
              x:ret.distances_rejected,
              y:ret.signals_rejected,
              text: ret.ids_rejected,
              marker:{
                color:"red"
              },
              error_x:{
                type:"data",
                array: ret.distances_rejected_error,
                visible:true
              },
              error_y:{
                type:"data",
                array:ret.signals_rejected_error,
                visible: true
              },
              name: "rejected stations",
              hoverinfo: "text"
            })
          }
          if(ret.ldf_signals.length>0){
            data.push({
              mode:"lines",
              x:ret.ldf_distances,
              y:ret.ldf_signals,
              line:{color:"black"},
              name: "LDF fit",
              hoverinfo: "none"
            })
          }
          if(ret.ldf_fill_signal.length>0){
            data.push({
              fill:"tozeroy",
              fillcolor: "rgba(0,0,0,0.3)",
              opacity:.3,
              x:ret.ldf_fill_distances,
              y:ret.ldf_fill_signal,
              name: "LDF uncertainty",
              type: "scatter",
              line:{color:"transparent"},
              hoverinfo: "none"
            });
          }
          var layout= {
            hovermode: "closest",
            width: plotWidth,
            height: plotHeight,
            legend: {
              x:.8,
              y:.99,
              bgcolor: "transparent",
              bordercolor: "black",
              borderwidth: 1
            },
            xaxis:{
              title: "distance to shower axis [m]"
            },
            yaxis:{
              title: "signal [VEM]"
            },
            margin:{
              l:40,
              r:0,
              t:20,
              b:40,
              pad:0
            }
          };
          plotly.newPlot(tab+"_sdLDFplot"+self.view.id,data,layout,{modeBarButtonsToRemove: ["sendDataToCloud"], showlink: false});
        }
        $(".panel-overlay",self.view.tabs[tab].sdLDFTile).hide();
      },

      addSdMap: function(tab,x,y,width,height){
        var self=this;
        if (self.view.tabs[tab].sdMapTile==null){
            self.view.gridstackNodes[tab].data("gridstack").add_widget(sdMapTemplate,x,y,width,height);
            self.view.tabs[tab].sdMapTile=$(".sd-map-tile",self.view.tabs[tab]);
            self.drawSdMap(tab);
            if(self.view.tabs[tab].sdStationListTile!=null){
              $(".locate-sd-station",self.view.tabs[tab].sdStationListTile).show();
            }
        }else{
          console.log("SD map already exists");
        }
      },

      drawSdMap: function(tab){
        var self=this;
        $(".remove-sd-map",self.view.tabs[tab]).click(function(){
          self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].sdMapTile);
          self.view.tabs[tab].sdMapTile=null;
          if(self.view.tabs[tab].sdStationListTile!=null){
            $(".locate-sd-station",self.view.tabs[tab].sdStationListTile).hide();
          }
        });
        $(".sd-map",self.view.tabs[tab]).attr("id",tab+"_sdMap"+self.view.id);
        self.view.tabs[tab].sdMapTile.on("resizestop",function(){
          self.updateSdMap(tab,false);
        });
        self.updateSdMap(tab,false);
      },

      updateSdMap: function(tab,changeZoom){
        var self=this;
        var tile=$(".sd-map-tile",self.view.tabs[tab]).data("_gridstack_node");
        self.view.tabs[tab].sdMapTile.data("changeZoom",changeZoom);
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdMapTile);
        var plotWidth=plotSize[0];
        var plotHeight=plotSize[1];
        if((plotHeight>100)&&(plotWidth>100)){
          $(".panel-overlay",self.view.tabs[tab].sdMapTile).show();
          var drawShowerAxis=$(".sd-map-draw-shower-axis",self.view.tabs[tab]).prop("checked");
          var onlyNearby=$(".sd-map-draw-only-nearby",self.view.tabs[tab]).prop("checked");
          var drawSignal=$(".sd-map-draw-signal",self.view.tabs[tab]).prop("checked");
          var drawTiming=$(".sd-map-draw-timing",self.view.tabs[tab]).prop("checked");
          var drawLDF=$(".sd-map-draw-ldf",self.view.tabs[tab]).prop("checked");
          self.view.GET("get_plotly",JSON.stringify({
            "plot_name": "SD_map",
            "only_nearby": onlyNearby,
            "tab": tab
          }));
        }
      },

      renderSdMap: function(tab,ret){
        var self=this;
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdMapTile);
        var plotWidth=plotSize[0];
        var plotHeight=plotSize[1];
        if (ret!=null){
          d3.select("#"+tab+"_sdMap"+self.view.id).selectAll("*").remove();
          var data=[];
          var x_length=Math.max.apply(Math,ret.signal_stations[0])-Math.min.apply(Math,ret.signal_stations[0]);
          var y_length=Math.max.apply(Math,ret.signal_stations[1])-Math.min.apply(Math,ret.signal_stations[1]);
          var x_mid=(Math.max.apply(Math,ret.signal_stations[0])+Math.min.apply(Math,ret.signal_stations[0]))*.5;
          var y_mid=(Math.max.apply(Math,ret.signal_stations[1])+Math.min.apply(Math,ret.signal_stations[1]))*.5;

          if(plotWidth>plotHeight){
            x_length=x_length*plotWidth/plotHeight;
          }else{
            y_length=y_length*plotHeight/plotWidth;
          }
          if(ret.signal_stations[0].length>0){
            data.push({
              type: "scatter",
              mode: "markers",
              x:ret.signal_stations[0],
              y:ret.signal_stations[1],
              text:ret.signal_station_ids,
              name: "signal stations",
              marker:{
                color: "green"
              }
            })
          }
          if(ret.non_signal_stations[0].length>0){
            data.push({
              type: "scatter",
              mode: "markers",
              x:ret.non_signal_stations[0],
              y: ret.non_signal_stations[1],
              text: ret.non_signal_station_ids,
              name: "non-signal stations",
              marker: {
                color: "black"
              }
            });
          }
          if(ret.rejected_stations[0].length>0){
            data.push({
              type: "scatter",
              mode: "markers",
              x:ret.rejected_stations[0],
              y:ret.rejected_stations[1],
              text:ret.rejected_station_dis,
              name: "rejected stations",
              marker: {
                color: "red"
              }
            });
          }
          if(ret.signal_info[0].length>0){
            data.push({
              type:"scatter",
              mode: "markers",
              x:ret.signal_info[0],
              y:ret.signal_info[1],
              text: ret.signal_labels,
              marker: {
                size: ret.signal_info[2],
                sizeref:.1,
                color: ret.signal_info[3],
                colorscale:"Electric",
                showscale: true,
                colorbar:{
                  title: "timing offset [ns]",
                  titleside: "right",
                  xpad: 10,
                  ypad:20
                }
              },
              name: "signal"
            });
          }
          if(ret.shower_axis.length>0){
            data.push({
              type: "scatter",
              mode: "lines",
              x:ret.shower_axis[0],
              y:ret.shower_axis[1],
              line:{
                color:"black"
              },
              name: "shower axis",
              hoverinfo: "none"
            })
          }
          if(ret.eye_names.length>0){
            data.push({
              type: "scatter",
              mode: "markers+text",
              x:ret.eye_positions[0],
              y:ret.eye_positions[1],
              marker: {
                color: "grey"
              },
              text:ret.eye_names,
              name: "FD Telescopes"
            });
          }
          var layout={
            hovermode: "closest",
            width: plotWidth,
            height: plotHeight,
            xaxis:{
              range:[x_mid-x_length,x_mid+x_length]
            },
            yaxis:{
              range:[y_mid-y_length,y_mid+y_length]
            },
            margin:{
              l:40,
              r:0,
              t:20,
              b:40,
              pad:5
            },
            legend: {
              x:0,
              y:.99,
              bgcolor: "transparent",
              bordercolor: "black",
              borderwidth: 1
            }
            };
          plotly.newPlot(tab+"_sdMap"+self.view.id,data,layout,{modeBarButtonsToRemove: ["sendDataToCloud"], showlink: false})
        }
        $(".panel-overlay",self.view.tabs[tab].sdMapTile).hide();
      },

      locateSdStation: function(tab,id){
        var self=this;
        var sdMapTile=self.view.tabs[tab].sdMapTile;
        if(sdMapTile!=null){
          self.view.GET("get_sd_station_position",JSON.stringify({
            "station_id": id
          }),function(err,ret){
            if(ret!=null){
              var zoomLevel=self.view.getZoomLevel(sdMapTile,false);
              var xLength=.5*(zoomLevel[1]-zoomLevel[0]);
              var yLength=.5*(zoomLevel[3]-zoomLevel[2]);
              self.view.tabs[tab].sdMapTile.graph.axes[0].set_axlim([ret[0] - xLength,ret[0] +xLength],[ret[1]-yLength,ret[1]+yLength],1000,true);
            }
          })
        }
      },

      addSdStationList: function(tab,x,y,width,height){
        var self=this;
        if(self.view.tabs[tab].sdStationListTile==null){
          self.view.gridstackNodes[tab].data("gridstack").add_widget(sdStationListTemplate,x,y,width,height);
          self.view.tabs[tab].sdStationListTile=$(".sd-station-list-tile",self.view.tabs[tab]);
          self.view.tabs[tab].sdStationTable=$(".sd-station-table",self.view.tabs[tab]);
          self.drawSdStationList(tab);
        }else{
          console.log("sdStationList already exists");
        }
      },

      drawSdStationList: function(tab){
        var self=this;
        self.view.tabs[tab].activeSdStation=-1;
        self.view.tabs[tab].sdStationTable=$(".sd-stations-table-body",self.view.tabs[tab].sdStationListTile);
        $(".remove-sd-station-list",self.view.tabs[tab]).click(function(){
          self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].sdStationListTile);
          self.view.tabs[tab].sdStationListTile=null;
        });
        if(self.view.tabs[tab].sdMapTile==null){
          $(".locate-sd-station",self.view.tabs[tab].sdStationListTile).hide();
        }else{
          $(".locate-sd-station",self.view.tabs[tab].sdStationListTile).show();
        }
        self.view.tabs[tab].sdStationTable.on("click",".locate-sd-station",function(event){
          event.stopPropagation();
          self.locateSdStation(tab,$(this).parents(".sd-stations-table-row")[0].id);
        });
        self.view.tabs[tab].sdStationTable.on("click",".sd-stations-table-row",function() {
          $(self.view.tabs[tab].activeSdStationListItem).attr("class", self.view.tabs[tab].previousSdStationClass);
          self.view.tabs[tab].activeSdStation=this.id;
          self.view.tabs[tab].activeSdStationListItem = $(this);
          self.view.tabs[tab].previousSdStationClass = $(self.view.tabs[tab].activeSdStationListItem).attr("class");
          $(self.view.tabs[tab].activeSdStationListItem).removeClass("default").removeClass("success").removeClass("warning").removeClass("danger").addClass("info");
          if(self.view.tabs[tab].sdVemTraceTile!=null){
              self.updateSdVemTrace(tab,this.id,true);
            }
            if(self.view.tabs[tab].sdElectrodeTracesTile!=null){
              self.updateSdElectrodeTraces(tab,this.id,true);
            }
          if(self.view.tabs[tab].sdSimParticleTypesTile!=null){
            self.view.simFunctions.updateSdSimParticleTypes(tab,true);
          }
          if(self.view.tabs[tab].sdSimParticleMomentumTile!=null){
            self.view.simFunctions.updateSdSimParticleMomentum(tab,true);
          }
          if(self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile!=null){
            self.view.simFunctions.updateSdSimParticleTimeMomentumDistribution(tab,true);
          }
        });
        self.updateSdStationList(tab);
      },

      updateSdStationList: function(tab) {
        var self = this;
        $(".panel-overlay", self.view.tabs[tab].sdStationListTile).show();
        self.view.GET("get_sd_stations",JSON.stringify({
          "tab": tab
        }));
      },

      renderSdStationList: function(tab,res) {
        var self=this;
        var directives = {
          "sd-station-id": {
            text: function () {
              return this.id;
            }
          },
          "sd-station-signal": {
            text: function () {
              return this.signal;
            }
          },
          "sd-station-trigger": {
            text: function () {
              return this.trigger;
            }
          },
          "sd-station-removal-reason": {
            text: function () {
              return this.removalReason;
            }
          },
          "sd-stations-row": {
            id: function () {
              return this.id;
            },
            class: function () {
              if (this.rejected == true) {
                return "sd-stations-table-row danger";
              } else if (this.saturated == true) {
                return "sd-stations-table-row warning"
              } else if (this.candidate == true) {
                return "sd-stations-table-row success";
              }
            }
          }
        };
        $(".sd-stations-table-body", self.view.tabs[tab]).render(res, directives);
        self.view.tabs[tab].activeSdStationListItem = $(".sd-stations-table-row", self.view.tabs[tab])[0];
        self.view.tabs[tab].previousSdStationClass = $(self.view.tabs[tab].activeSdStationListItem).attr("class");
        $(self.view.tabs[tab].activeSdStationListItem).removeClass("success").removeClass("default").removeClass("warning").removeClass("danger").addClass("info");
        $(".panel-overlay", self.view.tabs[tab].sdStationListTile).hide();
      },

      addSdTimeResiduals: function(tab,x,y,width,height){
        var self=this;
        if(self.view.tabs[tab].sdTimeResidualsTile==null){
          self.view.gridstackNodes[tab].data("gridstack").add_widget(sdTimeResidualsTemplate,x,y,width,height);
          self.view.tabs[tab].sdTimeResidualsTile=$(".sd-time-residuals-tile",self.view.tabs[tab]);
          self.drawSdTimeResiduals(tab);
        }else{
          console.log("sdTimeResiduals already exists");
        }
      },

      drawSdTimeResiduals: function(tab){
        var self=this;
        $(".sd-time-residuals-draw-plane",self.view.tabs[tab].sdTimeResidualsTile).prop("name",tab+"_sd-time-residuals"+self.view.id);
        $(".sd-time-residuals-draw-curvature",self.view.tabs[tab].sdTimeResidualsTile).prop("name",tab+"_sd-time-residuals"+self.view.id);
        $(".sd-time-residuals",self.view.tabs[tab]).attr("id",tab+"_sdTimeResiduals"+self.view.id);
        $(".save-plot",self.view.tabs[tab].sdTimeResidualsTile).click(function(){
          var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdTimeResidualsTile);
          var plotWidth=plotSize[0];
          var plotHeight=plotSize[1];
          self.view.savePlot("sd_time_residuals", self.view.tabs[tab].sdTimeResidualsTile,plotWidth,plotHeight);
        });
        self.view.tabs[tab].sdTimeResidualsTile.on("resizestop",function(){
          self.updateSdTimeResiduals(tab,false);
        });
        $(".remove-sd-time-residuals",self.view.tabs[tab]).click(function(){
          self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].sdTimeResidualsTile);
          self.view.tabs[tab].sdTimeResidualsTile=null;
        });
        $(".sd-time-residuals-drawing-option",self.view.tabs[tab]).click(function(){
          self.updateSdTimeResiduals(tab,false);
        });
        self.updateSdTimeResiduals(tab,false);
      },
      updateSdTimeResiduals: function(tab,changeZoom) {
        var self = this;
        self.view.tabs[tab].sdTimeResidualsTile.data("changeZoom",changeZoom);
        var drawPlane;
        if ($(".sd-time-residuals-draw-plane", self.view.tabs[tab]).prop("checked") == false && $(".sd-time-residuals-draw-curvature", self.view.tabs[tab]).prop("checked") == false) {
          return;
        } else drawPlane = $(".sd-time-residuals-draw-plane", self.view.tabs[tab]).prop("checked") == true;
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdTimeResidualsTile);
        var plotWidth = plotSize[0];
        var plotHeight = plotSize[1];
        if (plotWidth > 100 && plotHeight > 100) {
          $(".panel-overlay", self.view.tabs[tab].sdTimeResidualsTile).show();
          self.view.GET("get_plotly", JSON.stringify({
            "plot_name": "sd_time_residuals",
            "plane": drawPlane,
            "tab": tab
          }));
        }
      },

      renderSdTimeResiduals: function(tab,ret){
        var self=this;
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdTimeResidualsTile);
        var plotWidth=plotSize[0];
        var plotHeight=plotSize[1];
        if(ret!=null){
          d3.select("#"+tab+"_sdTimeResiduals"+self.view.id).selectAll("*").remove();
          var data=[];
          if(ret.candidates[0].length>0) {
            data.push({
              type: "scatter",
              mode: "markers",
              x: ret.candidates[0],
              y: ret.candidates[2],
              marker: {
                color: "black"
              },
              error_x: {
                type: "data",
                array: ret.candidates[1],
                visible: true,
                color: "black",
                thickness: 1
              },
              error_y: {
                type: "data",
                array: ret.candidates[3],
                visible: true,
                color: "black",
                thickness: 1
              },
              name: "candidate"
            });
          }
          if(ret.candidates[4].length>0) {
            data.push({
              type: "scatter",
              mode: "markers",
              x: ret.candidates[0],
              y: ret.candidates[4],
              marker: {
                color: "black",
                symbol: "triangle-up",
                size:10
              },
              error_x: {
                type: "data",
                array: ret.candidates[1],
                visible: true,
                color: "black",
                thickness:1
              },
              error_y: {
                type: "data",
                array: ret.candidates[5],
                visible: true,
                color: "black",
                thickness: 1
              },
              visible: "legendonly",
              name: "rise times"
            });
          }
          if(ret.candidates[6].length>0) {
            data.push({
              type: "scatter",
              mode: "markers",
              x: ret.candidates[0],
              y: ret.candidates[6],
              marker: {
                color: "black",
                symbol: "triangle-down",
                size: 10
              },
              error_x: {
                type: "data",
                array: ret.candidates[1],
                visible: true,
                color: "black",
                thickness:1
              },
              error_y: {
                type: "data",
                array: ret.candidates[7],
                visible: true,
                color: "black",
                thickness:1
              },
              visible: "legendonly",
              name: "fall times"
            });
          }
          if(ret.candidates[8].length>0) {
            data.push({
              type: "scatter",
              mode: "markers",
              x: ret.candidates[0],
              y: ret.candidates[8],
              marker: {
                color: "black",
                symbol: "square",
                size: 7
              },
              error_x: {
                type: "data",
                array: ret.candidates[1],
                visible: true,
                color: "black",
                thickness:1
              },
              error_y: {
                type: "data",
                array: ret.candidates[9],
                visible: true,
                color: "black",
                thickness:1
              },
              visible: "legendonly",
              name: "T50"
            });
          }
          if(ret.saturated[0].length>0){
            data.push({
              type: "scatter",
              mode: "markers",
              x:ret.saturated[0],
              y: ret.saturated[2],
              marker:{
                color: "blue"
              },
              error_x: {
                type: "data",
                array: ret.saturated[1],
                visible: true,
                color: "blue",
                thickness: 1
              },
              error_y:{
                type:"data",
                array: ret.saturated[3],
                visible: true,
                color: "blue",
                thickness: 1
              },
              name: "saturated"
            });
          }
          if(ret.saturated[4].length>0){
            data.push({
              type: "scatter",
              mode: "markers",
              x:ret.saturated[0],
              y: ret.saturated[4],
              marker:{
                color: "blue",
                symbol: "triangle-up",
                size: 10
              },
              error_x: {
                type: "data",
                array: ret.rejected[1],
                visible: true,
                color: "blue",
                thickness: 1
              },
              error_y:{
                type:"data",
                array: ret.rejected[5],
                visible: true,
                color: "blue",
                thickness: 1
              },
              visible: "legendonly",
              name: "rise times"
            });
          }
          if(ret.saturated[6].length>0){
            data.push({
              type: "scatter",
              mode: "markers",
              x:ret.saturated[0],
              y: ret.saturated[6],
              marker:{
                color: "blue",
                symbol: "triangle-down",
                size: 10
              },
              error_x: {
                type: "data",
                array: ret.rejected[1],
                visible: true,
                color: "blue",
                thickness: 1
              },
              error_y:{
                type:"data",
                array: ret.rejected[7],
                visible: true,
                color: "blue",
                thickness: 1
              },
              visible: "legendonly",
              name: "fall times"
            });
          }
          if(ret.saturated[8].length>0){
            data.push({
              type: "scatter",
              mode: "markers",
              x:ret.saturated[0],
              y: ret.saturated[8],
              marker:{
                color: "blue",
                symbol: "square",
                size: 7
              },
              error_x: {
                type: "data",
                array: ret.rejected[1],
                visible: true,
                color: "blue",
                thickness: 1
              },
              error_y:{
                type:"data",
                array: ret.rejected[9],
                visible: true,
                color: "blue",
                thickness: 1
              },
              visible: "legendonly",
              name: "T50"
            });
          }
          if(ret.rejected[2].length>0){
            data.push({
              type: "scatter",
              mode: "markers",
              x:ret.rejected[0],
              y: ret.rejected[2],
              marker:{
                color: "red"
              },
              error_x: {
                type: "data",
                array: ret.rejected[1],
                visible: true,
                color: "red",
                thickness:1
              },
              error_y:{
                type:"data",
                array: ret.rejected[3],
                visible: true,
                color: "red",
                thickness: 1
              },
              name: "rejected"
            });
          }
          if(ret.rejected[4].length>0){
            data.push({
              type: "scatter",
              mode: "markers",
              x:ret.rejected[0],
              y: ret.rejected[4],
              marker:{
                color: "red",
                symbol: "triangle-up",
                size: 10
              },
              error_x: {
                type: "data",
                array: ret.rejected[1],
                visible: true,
                color: "red",
                thickness: 1
              },
              error_y:{
                type:"data",
                array: ret.rejected[5],
                visible: true,
                color: "red",
                thickness: 1
              },
              visible: "legendonly",
              name: "rise times"
            });
          }
          if(ret.rejected[6].length>0){
            data.push({
              type: "scatter",
              mode: "markers",
              x:ret.rejected[0],
              y: ret.rejected[6],
              marker:{
                color: "red",
                symbol: "triangle-down",
                size: 10
              },
              error_x: {
                type: "data",
                array: ret.rejected[1],
                visible: true,
                color: "red",
                thickness: 1
              },
              error_y:{
                type:"data",
                array: ret.rejected[7],
                visible: true,
                color: "red",
                thickness: 1
              },
              visible: "legendonly",
              name: "fall times"
            });
          }
          if(ret.rejected[8].length>0){
            data.push({
              type: "scatter",
              mode: "markers",
              x:ret.rejected[0],
              y: ret.rejected[8],
              marker:{
                color: "red",
                symbol: "square",
                size: 7
              },
              error_x: {
                type: "data",
                array: ret.rejected[1],
                visible: true,
                color: "red",
                thickness: 1
              },
              error_y:{
                type:"data",
                array: ret.rejected[9],
                visible: true,
                color: "red",
                thickness: 1
              },
              visible: "legendonly",
              name: "T50"
            });
          }
          data.push({
            type: "scatter",
            mode: "lines",
            x:ret.fit_radius,
            y:ret.fit_value,
            line:{
              color: "black"
            },
            name: "fit"
          });
          data.push({
            fill:"tozeroy",
            fillcolor: "rgba(0,0,0,0.3)",
            opacity:.3,
            x:ret.fit_fill_radius,
            y:ret.fit_fill,
            name: "fit uncertainty",
            type: "scatter",
            line:{color:"transparent"},
            hoverinfo: "none"
          });
          var layout={
            hovermode: "closest",
            width: plotWidth,
            height: plotHeight,
            legend: {
              x:1,
              y:.99,
              bgcolor: "transparent",
              bordercolor: "black",
              borderwidth: 1
            },
            xaxis:{
              title: "distance to shower axis [m]"
            },
            yaxis:{
              title: "time [ns]"
            },
            margin:{
              l:40,
              r:0,
              t:20,
              b:40,
              pad:0
            }
          };
          plotly.newPlot(tab+"_sdTimeResiduals"+self.view.id,data,layout,{modeBarButtonsToRemove: ["sendDataToCloud"], showlink: false});
        }
        $(".panel-overlay",self.view.tabs[tab].sdTimeResidualsTile).hide();
      },

      addSdVemTrace: function(tab,x,y,width,height){
        var self=this;
        if(self.view.tabs[tab].sdVemTraceTile==null){
          self.view.gridstackNodes[tab].data("gridstack").add_widget(sdVemTraceTemplate,x,y,width,height);
          self.view.tabs[tab].sdVemTraceTile=$(".sd-vem-trace-tile",self.view.tabs[tab]);
          self.drawSdVemTrace(tab);
        }else{
          console.log("sdVemTrace already exists");
        }
      },

      drawSdVemTrace: function(tab){
        var self=this;
        $(".sd-vem-trace-pmt-1",self.view.tabs[tab].sdVemTraceTile).prop("name",tab+"_pmt-number"+self.view.id);
        $(".sd-vem-trace-pmt-2",self.view.tabs[tab].sdVemTraceTile).prop("name",tab+"_pmt-number"+self.view.id);
        $(".sd-vem-trace-pmt-3",self.view.tabs[tab].sdVemTraceTile).prop("name",tab+"_pmt-number"+self.view.id);
        $(".sd-vem-trace",self.view.tabs[tab]).attr("id",tab+"_sdVemTrace"+self.view.id);
        self.view.tabs[tab].sdVemTraceTile.on("resizestop",function(){
          self.updateSdVemTrace(tab,self.view.tabs[tab].activeSdStation,false);
        });
        $(".remove-sd-vem-trace",self.view.tabs[tab]).click(function(){
          self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].sdVemTraceTile);
          self.view.tabs[tab].sdVemTraceTile=null;
        });
        $(".save-plot",self.view.tabs[tab].sdVemTraceTile).click(function(){
          var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdVemTraceTile);
          var plotWidth=plotSize[0];
          var plotHeight=plotSize[1];
          self.view.savePlot("sd_vem_trace",self.view.tabs[tab].sdVemTraceTile,plotWidth,plotHeight);
        });
        $(".sd-vem-trace-drawing-option",self.view.tabs[tab]).click(function(){
          self.updateSdVemTrace(tab,self.view.tabs[tab].activeSdStation,false);
        });
        if(self.view.tabs[tab].activeSdStation==null) {
          self.updateSdVemTrace(tab, -1, true);
        }else{
          self.updateSdVemTrace(tab, self.view.tabs[tab].activeSdStation, true);
        }
      },

      updateSdVemTrace: function(tab,station,changeZoom) {
        var self = this;
        self.view.tabs[tab].data("changeZoom",changeZoom);
        self.view.tabs[tab].activeSdStation = station;
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdVemTraceTile);
        var plotWidth = plotSize[0];
        var plotHeight = plotSize[1];
        if (plotWidth > 100 && plotHeight > 100) {
          $(".panel-overlay", self.view.tabs[tab].sdVemTraceTile).show();
          self.view.GET("get_plot", JSON.stringify({
            "plot_name": "sd_vem_trace",
            "plot_width": plotWidth,
            "plot_height": plotHeight,
            "station_id": station,
            "pmt_1": $(".sd-vem-trace-pmt-1", self.view.tabs[tab]).prop("checked"),
            "pmt_2": $(".sd-vem-trace-pmt-2", self.view.tabs[tab]).prop("checked"),
            "pmt_3": $(".sd-vem-trace-pmt-3", self.view.tabs[tab]).prop("checked"),
            "zoom_level": null,
            "tab": tab
          }));
        }
      },

      renderSdVemTrace: function(tab,data){
        var self=this;
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdVemTraceTile);
        var plotWidth=plotSize[0];
        var plotHeight=plotSize[1];
        var changeZoom=self.view.tabs[tab].sdVemTraceTile.data("changeZoom");
        var zoomLevel=self.view.getZoomLevel(self.view.tabs[tab].sdVemTraceTile,changeZoom);
        $(".sd-vem-trace-station",self.view.tabs[tab].sdVemTraceTile).attr("data-value",self.view.tabs[tab].activeSdStation);
        if(data!=null){
          d3.select("#"+tab+"_sdVemTrace"+self.view.id).selectAll("*").remove();
          var commands=data;
          commands["height"]=plotHeight;
          commands["width"]=plotWidth;
          self.view.tabs[tab].sdVemTraceTile.graph=mpld3.draw_figure(tab+"_sdVemTrace"+self.view.id,commands);
          if(zoomLevel!=null){
            self.view.tabs[tab].sdVemTraceTile.graph.axes[0].set_axlim([zoomLevel[0],zoomLevel[1]],[zoomLevel[2],zoomLevel[3]],0,false);
          }
        }else{
          $(".sd-vem-trace",self.view.tabs[tab]).html("<h3>No Data to display</h3>");
        }
        $(".panel-overlay",self.view.tabs[tab].sdVemTraceTile).hide();
      },

      addSdElectrodeTraces: function(tab,x,y,width,height){
        var self=this;
        if(self.view.tabs[tab].sdElectrodeTracesTile==null){
          self.view.gridstackNodes[tab].data("gridstack").add_widget(sdElectrodeTraceTemplate,x,y,width,height);
          self.view.tabs[tab].sdElectrodeTracesTile=$(".sd-electrode-traces-tile",self.view.tabs[tab]);
          self.drawSdElectrodeTraces(tab);
        }else{
          console.log("sdElectrodeTraces already exists");
        }
      },

      drawSdElectrodeTraces: function(tab){
        var self=this;
        $(".sd-electrode-traces-pmt-1",self.view.tabs[tab].sdElectrodeTracesTile).prop("name",tab+"_electrode-pmt-number"+self.view.id);
        $(".sd-electrode-traces-pmt-2",self.view.tabs[tab].sdElectrodeTracesTile).prop("name",tab+"_electrode-pmt-number"+self.view.id);
        $(".sd-electrode-traces-pmt-3",self.view.tabs[tab].sdElectrodeTracesTile).prop("name",tab+"_electrode-pmt-number"+self.view.id);
        $(".sd-low-gain",self.view.tabs[tab].sdElectrodeTracesTile).prop("name",tab+"_electrode");
        $(".sd-high-gain",self.view.tabs[tab].sdElectrodeTracesTile).prop("name",tab+"_electrode");
        $(".sd-electrode-traces",self.view.tabs[tab].sdElectrodeTracesTile).attr("id",tab+"_sdElectrodeTrace"+self.view.id);
        self.view.tabs[tab].sdElectrodeTracesTile.on("resizestop",function(){
          self.updateSdElectrodeTraces(tab,self.view.tabs[tab].activeSdStation,false);
        });
        $(".remove-sd-electrode-traces",self.view.tabs[tab]).click(function(){
          self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].sdElectrodeTracesTile);
          self.view.tabs[tab].sdElectrodeTracesTile=null;
        });
        $(".save-plot",self.view.tabs[tab].sdElectrodeTracesTile).click(function(){
          var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdElectrodeTracesTile);
          var plotWidth=plotSize[0];
          var plotHeight=plotSize[1];
          self.view.savePlot("sd_electrode_trace",self.view.tabs[tab].sdElectrodeTracesTile,plotWidth,plotHeight);
        });
        $(".sd-electrode-traces-drawing-option",self.view.tabs[tab].sdElectrodeTracesTile).click(function(){
          self.updateSdElectrodeTraces(tab,self.view.tabs[tab].activeSdStation,true);
        });
        if(self.view.tabs[tab].activeSdStation==null) {
          self.updateSdElectrodeTraces(tab, -1, true);
        }else{
          self.updateSdElectrodeTraces(tab, self.view.tabs[tab].activeSdStation, true);
        }
      },

      updateSdElectrodeTraces: function(tab, station,changeZoom) {
        var self = this;
        self.view.tabs[tab].sdElectrodeTracesTile.data("changeZoom",changeZoom);
        self.view.tabs[tab].activeSdStation = station;
        var pmt;
        if ($(".sd-electrode-traces-pmt-1", self.view.tabs[tab].sdElectrodeTracesTile).prop("checked") == true) {
          pmt = 1;
        } else if ($(".sd-electrode-traces-pmt-2", self.view.tabs[tab].sdElectrodeTracesTile).prop("checked") == true) {
          pmt = 2;
        } else if ($(".sd-electrode-traces-pmt-3", self.view.tabs[tab].sdElectrodeTracesTile).prop("checked") == true) {
          pmt = 3;
        }
        var useHighGain;
        if ($(".sd-high-gain", self.view.tabs[tab].sdElectrodeTracesTile).prop("checked") == true) {
          useHighGain = true;
        } else if ($(".sd-low-gain", self.view.tabs[tab].sdElectrodeTracesTile).prop("checked") == true) {
          useHighGain = false;
        }
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdElectrodeTracesTile);
        var plotWidth = plotSize[0];
        var plotHeight = plotSize[1];
        if (plotWidth > 100 && plotHeight > 100) {
          $(".panel-overlay", self.view.tabs[tab].sdElectrodeTracesTile).show();
          self.view.GET("get_plot", JSON.stringify({
            "plot_name": "sd_electrode_trace",
            "plot_width": plotWidth,
            "plot_height": plotHeight,
            "station_id": station,
            "pmt_1": $(".sd-electrode-traces-pmt-1", self.view.tabs[tab].sdElectrodeTracesTile).prop("checked"),
            "pmt_2": $(".sd-electrode-traces-pmt-2", self.view.tabs[tab].sdElectrodeTracesTile).prop("checked"),
            "pmt_3": $(".sd-electrode-traces-pmt-3", self.view.tabs[tab].sdElectrodeTracesTile).prop("checked"),
            "high_gain": useHighGain,
            "zoom_level": null,
            "tab":tab
          }));
        }
      },

      renderSdElectrodeTraces: function(tab,data){
        var self=this;
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdElectrodeTracesTile);
        var plotWidth=plotSize[0];
        var plotHeight=plotSize[1];
        var changeZoom=self.view.tabs[tab].sdElectrodeTracesTile.data("changeZoom");
        var zoomLevel=self.view.getZoomLevel(self.view.tabs[tab].sdElectrodeTracesTile,changeZoom);
        $(".sd-electrode-traces-station",self.view.tabs[tab].sdElectrodeTracesTile).attr("data-value",self.view.tabs[tab].activeSdStation);
        if(data!=null){
          d3.select("#"+tab+"_sdElectrodeTrace"+self.view.id).selectAll("*").remove();
          var commands=data;
          commands["height"]=plotHeight;
          commands["width"]=plotWidth;
          self.view.tabs[tab].sdElectrodeTracesTile.graph=mpld3.draw_figure(tab+"_sdElectrodeTrace"+self.view.id,commands);
          if(zoomLevel!=null){
            self.view.tabs[tab].sdElectrodeTracesTile.graph.axes[0].set_axlim([zoomLevel[0],zoomLevel[1]],[zoomLevel[2],zoomLevel[3]],0,false);
          }
        }else{
          $(".sd-electrode-traces",self.view.tabs[tab]).html("<h3>No Data to display</h3>");
        }
        $(".panel-overlay",self.view.tabs[tab].sdElectrodeTracesTile).hide();},

      selectSdStation: function(tab,id){
        var self=this;
        self.view.tabs[tab].activeSdStation=id;
        if(self.view.tabs[tab].sdStationListTile!=null){
          $(self.view.tabs[tab].activeSdStationListItem).attr("class", self.view.tabs[tab].previousSdStationClass);
          $(".sd-stations-table-row",self.view.tabs[tab]).each(function(){
            if(this.id==id){
              self.view.tabs[tab].activeSdStationListItem=this;
            }
          });
          self.view.tabs[tab].previousSdStationClass = $(self.view.tabs[tab].activeSdStationListItem).attr("class");
          $(self.view.tabs[tab].activeSdStationListItem).removeClass("default").removeClass("success").removeClass("warning").removeClass("danger").addClass("info");
        }
        if(self.view.tabs[tab].sdVemTraceTile!=null){
          self.updateSdVemTrace(tab,id,true);
        }
        if(self.view.tabs[tab].sdElectrodeTracesTile!=null){
          self.updateSdElectrodeTraces(tab,id,true);
        }
        if(self.view.tabs[tab].sdSimParticleTypesTile!=null){
          self.view.simFunctions.updateSdSimParticleTypes(tab,true);
        }
        if(self.view.tabs[tab].sdSimParticleMomentumTile!=null){
          self.view.simFunctions.updateSdSimParticleMomentum(tab,true);
        }
      }

    });
    return sdFunctions;
});