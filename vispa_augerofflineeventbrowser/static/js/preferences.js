define(function() {
  var self=this;
  var defaultPreferences = {
    customTabsContent: {
      label: "customTabsContent",
      level: 4,
      type:"list",
      value: [],
      description: "Layout of the 'custom' tab",
    },
    tabNames: {
      label: "tabNames",
      level: 4,
      type: "list",
      value: [],
      description: "Names of the custom tabs"
    },
    "saveTabWarning": {
      label: "saveTabWarning",
      level: 1,
      type: "boolean",
      value: true,
      description: "Warn when trying to overwrite a custom tab"
    },
    "slideshowInterval": {
      label: "slideshowInterval",
      level: 1,
      type: "int",
      value: 20,
      description: "Time interval for the sliede show in seconds. If it is set to 0, the slideshow function is deactivated."
    },
    "defaultPath": {
      label: "defaultPath",
      level:1,
      type: "string",
      value: "$HOME",
      description: "Default path for file selector"
    }
  };
  var ovList=[
    {
      label: "Event Overview",
      callback: function(){
        this.$root.instance.ovFunctions.addEventOverview(this.$root.instance.activeTab,0,0,4,12);
      }
    },
    {
      label: "Event Display",
      callback: function(){
        this.$root.instance.ovFunctions.addEventDisplay(this.$root.instance.activeTab,0,0,6,16);
      }
    },
    {
      label: "Event Selection",
      callback: function(){
        this.$root.instance.ovFunctions.addEventSelection(this.$root.instance.activeTab,0,0,4,4);
      }
    },
    {
      label: "Event Categories",
      callback: function(){
        this.$root.instance.ovFunctions.addEventCategories(this.$root.instance.activeTab,0,0,4,4);
      }
    },
    {
      label: "Incoming Directions",
      callback: function(){
        this.$root.instance.ovFunctions.addIncomingDirections(this.$root.instance.activeTab,0,0,4,12);
      }
    },
    {
      label: "File List",
      callback: function(){
        this.$root.instance.ovFunctions.addFileList(this.$root.instance.activeTab,0,0,4,4);
      }
    },
    {
      label: "Offline Configuration",
      callback: function(){
        this.$root.instance.ovFunctions.addOfflineConfiguration(this.$root.instance.activeTab,0,0,4,8);
      }
    }
  ];
  var sdList=[
    {
      label: "SD Event Info",
      callback: function(){
        this.$root.instance.sdFunctions.addSdEventInfo(this.$root.instance.activeTab,0,0,2,8);
      }
    },
    {
      label: "SD Map",
      callback: function(){
        this.$root.instance.sdFunctions.addSdMap(this.$root.instance.activeTab,0,0,5,12);
      }
    },
    {
      label: "SD Station List",
      callback: function(){
        this.$root.instance.sdFunctions.addSdStationList(this.$root.instance.activeTab,0,0,2,8);
      }
    },
    {
      label: "SD LDF",
      callback: function(){
        this.$root.instance.sdFunctions.addSdLDF(this.$root.instance.activeTab,0,0,4,12);
      }
    },
    {
      label: "SD Time Residuals",
      callback: function(){
        this.$root.instance.sdFunctions.addSdTimeResiduals(this.$root.instance.activeTab,0,0,6,12);
      }
    },
    {
      label: "SD VEM Trace",
      callback: function(){
        this.$root.instance.sdFunctions.addSdVemTrace(this.$root.instance.activeTab,0,0,4,8);
      }
    },
    {
      label: "SD Electrode Traces",
      callback: function(){
        this.$root.instance.sdFunctions.addSdElectrodeTraces(this.$root.instance.activeTab,0,0,4,8);
      }
    }
  ];
  var fdList=[
    {
      label: "FD Event Info",
      callback: function(){
        this.$root.instance.fdFunctions.addFdEventInfo(this.$root.instance.activeTab,0,0,2,12);
      }
    },
    {
      label: "FD Camera",
      callback: function(){
        this.$root.instance.fdFunctions.addFdCamera(this.$root.instance.activeTab,0,0,5,12);
      }
    },
    {
      label: "FD Shower Profile",
      callback: function(){
        this.$root.instance.fdFunctions.addFdShowerProfile(this.$root.instance.activeTab,0,0,6,12);
      }
    },
    {
      label: "FD Time Fit",
      callback: function(){
        this.$root.instance.fdFunctions.addFdTimeFit(this.$root.instance.activeTab,0,0,5,12);
      }
    },
    {
      label: "FD Pixel",
      callback: function(){
        this.$root.instance.fdFunctions.addFdPixelHisto(this.$root.instance.activeTab,0,0,4,10);
      }
    }
  ];
  var rdList=[
    {
      label: "Radio Event Info",
      callback: function(){
        this.$root.instance.rdFunctions.addRdEventInfo(this.$root.instance.activeTab,0,0,2,8);
      }
    },
    {
      label: "AERA Map",
      callback: function(){
        this.$root.instance.rdFunctions.addAeraMap(this.$root.instance.activeTab,0,0,4,12);
      }
    },
    {
      label: "Radio Station List",
      callback: function(){
        this.$root.instance.rdFunctions.addRdStationList(this.$root.instance.activeTab,0,0,4,8);
      }
    },
    {
      label: "Radio Station Info",
      callback: function(){
        this.$root.instance.rdFunctions.addRdStationInfo(this.$root.instance.activeTab,0,0,2,8);
      }
    },
    {
      label: "Radio LDF",
      callback: function(){
        this.$root.instance.rdFunctions.addRdLDF(this.$root.instance.activeTab,0,0,4,12);
      }
    },
    {
      label: "Radio Trace",
      callback: function(){
        this.$root.instance.rdFunctions.addRdStationTrace(this.$root.instance.activeTab,0,0,4,12);
      }
    },
    {
      label: "Radio Spectrum",
      callback: function(){
        this.$root.instance.rdFunctions.addRdStationSpectrum(this.$root.instance.activeTab,0,0,4,12);
      }
    },
    {
      label: "Radio Channel Trace",
      callback: function(){
        this.$root.instance.rdFunctions.addRdChannelTrace(this.$root.instance.activeTab,0,0,4,12);
      }
    },
    {
      label: "Radio Time Residuals",
      callback: function(){
        this.$root.instance.rdFunctions.addRdTimeResiduals(this.$root.instance.activeTab,0,0,4,12);
      }
    }
  ];
  var simList=[
    {
      label: "SD Sim Particle Types",
      callback: function(){
        this.$root.instance.simFunctions.addSdSimParticleTypes(this.$root.instance.activeTab,0,0,4,12);
      }
    },
    {
      label: "SD Sim Particle Momentum",
      callback: function(){
        this.$root.instance.simFunctions.addSdSimParticleMomentum(this.$root.instance.activeTab,0,0,4,12);
      }
    },
    {
      label: "SD Sim Particle Time and Momentum",
      callback: function(){
        this.$root.instance.simFunctions.addSdSimParticleTimeMomentumDistribution(this.$root.instance.activeTab,0,0,4,12);
      }
    }
  ];

  var menu={
    overview: {
      label: 'overview',
      iconClass: 'glyphicon glyphicon-picture',
      items: ovList
    },
    SD:{
      label: "SD",
      iconClass: "glyphicon glyphicon-import",
      items: sdList
      },
    FD: {
      label: "FD",
      iconClass: "glyphicon glyphicon-eye-open",
      items: fdList
    },
    Radio: {
      label: "Radio",
      iconClass: "glyphicon glyphicon-flash",
      items: rdList
    },
    Simulation: {
      label: "Simulation",
      iconClass: "glyphicon glyphicon-equalizer",
      items: simList
    },
    removeTab: {
      label: "remove tab",
      iconClass: "glyphicon glyphicon-remove",
      callback: function () {
        this.$root.instance.customTabFunctions.removeTab(this.$root.instance.activeTab);
      }
    },
    addFile: {
      label: "add file",
      iconClass: "glyphicon glyphicon-open-file",
      callback: function () {
        this.$root.instance.addFile();
      }
    },
    changeOfflinePath: {
      label: "change Offline path",
      "iconClass": "glyphicon glyphicon-road",
      callback: function () {
        this.$root.instance.changeOfflinePath(null, "Please reconnect to the workspace to apply changes.");     //give function null if no new file should be opened
      }
    }
  };
  return {
    preferences: defaultPreferences,
    menu: menu
  }
});