require.config({
  paths: {
    "mpld3": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/mpld3.v0.2"),
    "augeroffline_d3": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/d3.v3.min"),
    "gridstack": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/gridstack/gridstack"),
    "lodash": vispa.dynamicURL("extensions/augerofflineeventbrowser/static/vendor/lodash/lodash"),
  },
  shim: {
    gridstack: ["jquery", "lodash"],
    mpld3: {
      exports: "mpld3"
    }
  }
});
define([
  "jclass",
  "async",
  "jquery",
  "augeroffline_d3",
  "text!../html/sd_sim_particle_momentum.html",
  "text!../html/sd_sim_particle_time_momentum_distribution.html",
  "text!../html/sd_sim_particle_types.html",
  "gridstack",
  "css!./../css/augerofflineeventbrowser",
  "css!./../vendor/gridstack/gridstack.css"
], function(jClass, async, $, d3,sdSimParticleMomentumTemplate,sdSimParticleMomentumDistributionTemplate,sdSimParticleTypesTemplate) {
    var simFunctions=jClass._extend({
      init: function init(view) {
        var self = this;
        init._super.call(this, arguments);
        self.view = view;
      },
      addSdSimParticleTypes: function(tab,x,y,width,height){
        var self=this;
        if (self.view.tabs[tab].sdSimParticleTypesTile==null) {
            self.view.gridstackNodes[tab].data("gridstack").add_widget(sdSimParticleTypesTemplate,x,y,width,height);
            self.view.tabs[tab].sdSimParticleTypesTile = $(".sd-sim-particle-types-tile", self.view.tabs[tab]);
            self.drawSdSimParticleTypes(tab);
        }else{
          console.log("sdSimParticleTypes already exists");
        }
      },
      drawSdSimParticleTypes: function(tab){
        var self=this;
        $(".remove-sd-sim-particle-types",self.view.tabs[tab].sdSimParticleTypesTile).click(function(){
          self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].sdSimParticleTypesTile);
          self.view.tabs[tab].sdSimParticleTypesTile=null;
        });
        $(".save-plot",self.view.tabs[tab].sdSimParticleTypesTile).click(function(){
          var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdSimParticleTypesTile);
          var plotWidth=plotSize[0];
          var plotHeight=plotSize[1];
          self.view.savePlot("sd_sim_particle_types",self.view.tabs[tab].sdSimParticleTypesTile,plotWidth,plotHeight)
        });
        $(".sd-sim-particle-types-plot",self.view.tabs[tab]).attr("id",tab+"_sdSimParticleTypesPlot"+self.view.id);
        var particleMomentumVal=$(".particle-momentum-val",self.view.tabs[tab].sdSimParticleTypesTile);
        var particleMomentumSlider=$(".particle-momentum-slider",self.view.tabs[tab].sdSimParticleTypesTile);
        particleMomentumSlider.slider({
          min: 0.5,
          max: 1000000,
          scale: "logarithmic",
          range: true,
          value: [0.1,10000001],
          step: 1,
          tooltip: "hide"
        }).on("slide",function(evt){
          var val1;
          var val2;
          var str1;
          var str2;
          if (evt.value[0]==1){
            val1=0;
            str1=0;
          }else if(evt.value[0]<1001){
            val1=evt.value[0]-1;
            val1 = Math.floor((evt.value[0]-1) / Math.pow(10, Math.floor(Math.log10(evt.value[0]-1)))) * Math.pow(10, Math.floor(Math.log10(evt.value[0]-1)));
            str1=val1+ "MeV";
          }
          else {
            val1 = evt.value[0]-1;
            val1 = Math.floor(val1 / Math.pow(10, Math.floor(Math.log10(val1)))) * Math.pow(10, Math.floor(Math.log10(val1)));
            str1=val1/1000+ "GeV";
          }
          if(evt.value[1]<1001){
            val2=evt.value[1]-1;
            val2 = Math.floor(evt.value[1] / Math.pow(10, Math.floor(Math.log10(evt.value[1]-1)))) * Math.pow(10, Math.floor(Math.log10(evt.value[1]-1)));
            str2=val2+ "MeV";
            particleMomentumSlider.slider({values:[val1,val2]});
          }
          else if(evt.value[1]<1000000-1){
            val2=evt.value[1]-1;
            val2 = Math.floor(val2 / Math.pow(10, Math.floor(Math.log10(val2)))) * Math.pow(10, Math.floor(Math.log10(val2)));
            str2=val2/1000+ "GeV";
            particleMomentumSlider.slider({value: [val1,val2]});
          }else{
            particleMomentumSlider.slider({value: [val1,evt.value[1]]});
            str2="&infin;";
          }
          particleMomentumVal[0].innerHTML=" "+str1+" - "+str2;
        }).on("slideStop",function(){
          self.updateSdSimParticleTypes(tab,true);
        });
        var particleTimeDiffVal=$(".particle-time-diff-val",self.view.tabs[tab].sdSimParticleTypesTile);
        var particleTimeDiffSlider=$(".particle-time-diff-slider",self.view.tabs[tab].sdSimParticleTypesTile);
        particleTimeDiffSlider.slider({
          min:0,
          max:101,
          range: false,
          value: 100,
          step: 1,
          tooltip: "hide"
        }).on("slide",function(evt){
          if (evt.value<101) {
            particleTimeDiffVal[0].innerHTML = " " + evt.value + " ns";
          }else{
            particleTimeDiffVal[0].innerHTML=" &infin;";
          }
        }).on("slideStop",function(){
          self.updateSdSimParticleTypes(tab,true);
        });
        self.view.tabs[tab].sdSimParticleTypesTile.on("resizestop",function(){
          self.updateSdSimParticleTypes(tab,false);
        });
        self.updateSdSimParticleTypes(tab,true);
      },
      updateSdSimParticleTypes: function(tab,changeZoom){
        var self=this;
        self.view.tabs[tab].sdSimParticleTypesTile.data("changeZoom",changeZoom);
        $(".panel-overlay",self.view.tabs[tab].sdSimParticleTypesTile).show();
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdSimParticleTypesTile);
        var plotWidth = plotSize[0];
        var plotHeight =plotSize[1];
        var momentumRange = $(".particle-momentum-slider",self.view.tabs[tab].sdSimParticleTypesTile).slider("getValue");
        var maxTimeDiff=$(".particle-time-diff-slider",self.view.tabs[tab].sdSimParticleTypesTile).slider("getValue");
        var plotParameters = {
          "plot_name": "sd_sim_particle_types",
          "plot_width": plotWidth,
          "plot_height": plotHeight,
          "zoom_level": null,
          "station_id": self.view.tabs[tab].activeSdStation,
          "momentum_range":momentumRange,
          "max_time_diff": maxTimeDiff,
          "tab": tab
        };
        self.view.GET("get_plot",JSON.stringify(plotParameters));
      },
      renderSdSimParticleTypes: function(tab,data){
        var self=this;
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdSimParticleTypesTile);
        var plotWidth=plotSize[0];
        var plotHeight=plotSize[1];
        var changeZoom=self.view.tabs[tab].sdSimParticleTypesTile.data("changeZoom");
        var zoomLevel=self.view.getZoomLevel(self.view.tabs[tab].sdSimParticleTypesTile,changeZoom);
        if (data != null) {
          $(".sd-sim-particle-types-station",self.view.tabs[tab].sdSimParticleTypesTile).attr("data-value",self.view.tabs[tab].activeSdStation);
          d3.select("#" + tab + "_sdSimParticleTypesPlot"+self.view.id).selectAll("*").remove();
          var commands = data;
          commands["height"] = plotHeight;
          commands["width"] = plotWidth;
          self.view.tabs[tab].sdSimParticleTypesTile.graph=mpld3.draw_figure(tab + "_sdSimParticleTypesPlot"+self.view.id, commands);
          if(zoomLevel!=null){
            self.view.tabs[tab].sdSimParticleTypesTile.graph.axes[0].set_axlim([zoomLevel[0],zoomLevel[1]],[zoomLevel[2],zoomLevel[3]],0,false);
          }
        }else{
          $(".sd-sim-particles-plot",self.view.tabs[tab].sdSimParticleTypesTile).html("<h3>No Data to display</h3>")
        }
        $(".panel-overlay",self.view.tabs[tab].sdSimParticleTypesTile).hide();
      },


      addSdSimParticleMomentum: function(tab,x,y,width,height){
        var self=this;
        if (self.view.tabs[tab].sdSimParticleMomentumTile==null) {
            self.view.gridstackNodes[tab].data("gridstack").add_widget(sdSimParticleMomentumTemplate,x,y,width,height);
            self.view.tabs[tab].sdSimParticleMomentumTile = $(".sd-sim-particle-momentum-tile", self.view.tabs[tab]);
            self.drawSdSimParticleMomentum(tab);
        }else{
          console.log("sdSimParticleMomentum already exists");
        }
      },
      drawSdSimParticleMomentum: function(tab){
        var self=this;
        $(".remove-sd-sim-particle-momentum",self.view.tabs[tab].sdSimParticleMomentumTile).click(function(){
          self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].sdSimParticleMomentumTile);
          self.view.tabs[tab].sdSimParticleMomentumTile=null;
        });
        $(".save-plot",self.view.tabs[tab].sdSimParticleMomentumTile).click(function(){
          var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdSimParticleMomentumTile);
          var plotWidth=plotSize[0];
          var plotHeight=plotSize[1];
          self.view.savePlot("sd_sim_particle_momentum",self.view.tabs[tab].sdSimParticleMomentumTile,plotWidth,plotHeight)
        });
        $(".sd-sim-particle-momentum-plot",self.view.tabs[tab]).attr("id",tab+"_sdSimParticleMomentumPlot"+self.view.id);

        self.view.tabs[tab].sdSimParticleMomentumTile.on("resizestop",function(){
          self.updateSdSimParticleMomentum(tab,false);
        });
        $(".plot-bool",self.view.tabs[tab].sdSimParticleMomentumTile).click(function(){
          self.updateSdSimParticleMomentum(tab,false);
        });
        var particleTimeDiffVal=$(".particle-time-diff-val",self.view.tabs[tab].sdSimParticleMomentumTile);
        var particleTimeDiffSlider=$(".particle-time-diff-slider",self.view.tabs[tab].sdSimParticleMomentumTile);
        particleTimeDiffSlider.slider({
          min:0,
          max:101,
          range: false,
          value: 100,
          step: 1,
          tooltip: "hide"
        }).on("slide",function(evt){
          if (evt.value<101) {
            particleTimeDiffVal[0].innerHTML = " " + evt.value + " ns";
          }else{
            particleTimeDiffVal[0].innerHTML=" &infin;";
          }
        }).on("slideStop",function(){
          self.updateSdSimParticleMomentum(tab,true);
        });
        self.updateSdSimParticleMomentum(tab,true);
      },
      updateSdSimParticleMomentum: function(tab,changeZoom){
        var self=this;
        self.view.tabs[tab].sdSimParticleMomentumTile.data("changeZoom",changeZoom);
        $(".panel-overlay",self.view.tabs[tab].sdSimParticleMomentumTile).show();
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdSimParticleMomentumTile);
        var plotWidth = plotSize[0];
        var plotHeight =plotSize[1];
        var drawPhotons=$(".sd-sim-particle-momentum-photons",self.view.tabs[tab].sdSimParticleMomentumTile).prop("checked");
        var drawElectrons=$(".sd-sim-particle-momentum-electrons",self.view.tabs[tab].sdSimParticleMomentumTile).prop("checked");
        var drawMuons=$(".sd-sim-particle-momentum-muons",self.view.tabs[tab].sdSimParticleMomentumTile).prop("checked");
        var drawOther=$(".sd-sim-particle-momentum-other",self.view.tabs[tab].sdSimParticleMomentumTile).prop("checked");
        var drawContour=$(".sd-sim-particle-momentum-contour",self.view.tabs[tab].sdSimParticleMomentumTile).prop("checked");
        var maxTimeDiff=$(".particle-time-diff-slider",self.view.tabs[tab].sdSimParticleMomentumTile).slider("getValue");
        var plotParameters = {
          "plot_name": "sd_sim_particle_momentum",
          "plot_width": plotWidth,
          "plot_height": plotHeight,
          "zoom_level": null,
          "station_id": self.view.tabs[tab].activeSdStation,
          "draw_photons": drawPhotons,
          "draw_electrons": drawElectrons,
          "draw_muons": drawMuons,
          "draw_other": drawOther,
          "draw_contour": drawContour,
          "max_time_diff": maxTimeDiff,
          "tab": tab
        };
        self.view.GET("get_plot",JSON.stringify(plotParameters));
      },
      renderSdSimParticleMomentum: function(tab,data){
        var self=this;
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdSimParticleMomentumTile);
        var plotWidth=plotSize[0];
        var plotHeight=plotSize[1];
        var changeZoom=self.view.tabs[tab].sdSimParticleMomentumTile.data("changeZoom");
        var zoomLevel=self.view.getZoomLevel(self.view.tabs[tab].sdSimParticleMomentumTile,changeZoom);
        if (data != null) {
          $(".sd-sim-particle-momentum-station",self.view.tabs[tab].sdSimParticleMomentumTile).attr("data-value",self.view.tabs[tab].activeSdStation);
          d3.select("#" + tab + "_sdSimParticleMomentumPlot"+self.view.id).selectAll("*").remove();
          var commands = data;
          commands["height"] = plotHeight;
          commands["width"] = plotWidth;
          self.view.tabs[tab].sdSimParticleMomentumTile.graph=mpld3.draw_figure(tab + "_sdSimParticleMomentumPlot"+self.view.id, commands);
          if(zoomLevel!=null){
            self.view.tabs[tab].sdSimParticleMomentumTile.graph.axes[0].set_axlim([zoomLevel[0],zoomLevel[1]],[zoomLevel[2],zoomLevel[3]],0,false);
          }
        }else{
          $(".sd-sim-particle-momentum-plot",self.view.tabs[tab].sdSimParticleMomentumTile).html("<h3>No Data to display</h3>")
        }
        $(".panel-overlay",self.view.tabs[tab].sdSimParticleMomentumTile).hide();
      },

      addSdSimParticleTimeMomentumDistribution: function(tab,x,y,width,height){
        var self=this;
        if (self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile==null) {
            self.view.gridstackNodes[tab].data("gridstack").add_widget(sdSimParticleMomentumDistributionTemplate,x,y,width,height);
            self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile = $(".sd-sim-particle-time-momentum-distribution-tile", self.view.tabs[tab]);
            self.drawSdSimParticleTimeMomentumDistribution(tab);
        }else{
          console.log("sdSimParticleTimeMomentum already exists");
        }
      },
      drawSdSimParticleTimeMomentumDistribution: function(tab){
        var self=this;
        $(".remove-sd-sim-particle-time-momentum-distribution",self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile).click(function(){
          self.view.gridstackNodes[tab].data("gridstack").remove_widget(self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile);
          self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile=null;
        });
        $(".save-plot",self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile).click(function(){
          var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile);
          var plotWidth=plotSize[0];
          var plotHeight=plotSize[1];
          self.view.savePlot("sd_sim_particle_time_momentum_distribution",self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile,plotWidth,plotHeight)
        });
        $(".sd-sim-particle-time-momentum-distribution-plot",self.view.tabs[tab]).attr("id",tab+"_sdSimParticleTimeMomentumDistributionPlot"+self.view.id);

        self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile.on("resizestop",function(){
          self.updateSdSimParticleTimeMomentumDistribution(tab,false);
        });
        $(".plot-bool",self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile).click(function(){
          self.updateSdSimParticleTimeMomentumDistribution(tab,false);
        });
        var particleTimeDiffVal=$(".particle-time-diff-val",self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile);
        var particleTimeDiffSlider=$(".particle-time-diff-slider",self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile);
        particleTimeDiffSlider.slider({
          min:0,
          max:101,
          range: false,
          value: 100,
          step: 1,
          tooltip: "hide"
        }).on("slide",function(evt){
          if (evt.value<101) {
            particleTimeDiffVal[0].innerHTML = " " + evt.value + " ns";
          }else{
            particleTimeDiffVal[0].innerHTML=" &infin;";
          }
        }).on("slideStop",function(){
          self.updateSdSimParticleTimeMomentumDistribution(tab,true);
        });
        self.updateSdSimParticleTimeMomentumDistribution(tab,true);
      },
      updateSdSimParticleTimeMomentumDistribution: function(tab,changeZoom){
        var self=this;
        self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile.data("changeZoom",changeZoom);
        $(".panel-overlay",self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile).show();
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile);
        var plotWidth = plotSize[0];
        var plotHeight =plotSize[1];
        var drawPhotons=$(".sd-sim-particle-time-momentum-distribution-photons",self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile).prop("checked");
        var drawElectrons=$(".sd-sim-particle-time-momentum-distribution-electrons",self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile).prop("checked");
        var drawMuons=$(".sd-sim-particle-time-momentum-distribution-muons",self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile).prop("checked");
        var drawOther=$(".sd-sim-particle-time-momentum-distribution-other",self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile).prop("checked");
        var maxTimeDiff=$(".particle-time-diff-slider",self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile).slider("getValue");
        var plotParameters = {
          "plot_name": "sd_sim_particle_time_momentum_distribution",
          "plot_width": plotWidth,
          "plot_height": plotHeight,
          "zoom_level": null,
          "station_id": self.view.tabs[tab].activeSdStation,
          "draw_photons": drawPhotons,
          "draw_electrons": drawElectrons,
          "draw_muons": drawMuons,
          "draw_other": drawOther,
          "max_time_diff": maxTimeDiff,
          "tab": tab
        };
        self.view.GET("get_plot",JSON.stringify(plotParameters));
      },
      renderSdSimParticleTimeMomentumDistribution: function(tab,data){
        var self=this;
        var plotSize=self.view.getPlotSize(tab,self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile);
        var plotWidth=plotSize[0];
        var plotHeight=plotSize[1];
        var changeZoom=self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile.data("changeZoom");
        var zoomLevel=self.view.getZoomLevel(self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile,changeZoom);
        if (data != null) {
          $(".sd-sim-particle-time-momentum-distribution-station",self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile).attr("data-value",self.view.tabs[tab].activeSdStation);
          d3.select("#" + tab + "_sdSimParticleTimeMomentumDistributionPlot"+self.view.id).selectAll("*").remove();
          var commands = data;
          commands["height"] = plotHeight;
          commands["width"] = plotWidth;
          self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile.graph=mpld3.draw_figure(tab + "_sdSimParticleTimeMomentumDistributionPlot"+self.view.id, commands);
          if(zoomLevel!=null){
            self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile.graph.axes[0].set_axlim([zoomLevel[0],zoomLevel[1]],[zoomLevel[2],zoomLevel[3]],0,false);
          }
        }else{
          $(".sd-sim-particle-time-momentum-distribution-plot",self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile).html("<h3>No Data to display</h3>")
        }
        $(".panel-overlay",self.view.tabs[tab].sdSimParticleTimeMomentumDistributionTile).hide();
      }
    });
  return simFunctions;
});