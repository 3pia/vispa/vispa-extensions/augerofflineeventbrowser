# -*- coding: utf-8 -*-

# imports
from vispa import AbstractExtension

# import the controller
from controller import AugerOfflineEventbrowserController


# noinspection PyMethodMayBeStatic
class AugerOfflineEventbrowserExtension(AbstractExtension):

    def get_name(self):
        return 'augeroffline-eventbrowser'

    def dependencies(self):
        return []

    def setup(self, extensionList):
        self.controller(AugerOfflineEventbrowserController(self))
        self.remote()
