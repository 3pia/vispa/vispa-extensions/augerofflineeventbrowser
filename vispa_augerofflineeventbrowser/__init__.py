# -*- coding: utf-8 -*-

from vispa.server import AbstractExtension
from controller import AugerOfflineEventbrowserController


class AugerOfflineEventbrowserExtension(AbstractExtension):

    def name(self):
        return "augerofflineeventbrowser"

    def dependencies(self):
        return []

    def setup(self):
        self.add_controller(AugerOfflineEventbrowserController(self))
        self.add_workspace_directoy()
