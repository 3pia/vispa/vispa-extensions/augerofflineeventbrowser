# -*- coding: utf-8 -*-

# imports
import cherrypy
import logging
from vispa.controller import AbstractController

logger = logging.getLogger(__name__)


class AugerOfflineEventbrowserController(AbstractController):
    def _eventbrowser(self, offline_path=None):
        window_id = cherrypy.request.private_params.get("_windowId", None)
        view_id = cherrypy.request.private_params.get("_viewId", None)
        eid = window_id + "-" + view_id
        return self.extension.get_workspace_instance("AugerOfflineEventbrowser", eid, offline_path=offline_path,view_id=view_id,window_id=window_id)

    @cherrypy.expose
    def check_offline_environment_variable(self):
        return self._eventbrowser().check_offline_environment_variable()

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def open_ADST(self, filepaths, offline_path,event_number=0):
        r = self._eventbrowser(offline_path)
        return r.open_ADST(filepaths, offline_path,event_number)

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def add_ADST(self, filenames):
        return self._eventbrowser().add_ADST(filenames)

    @cherrypy.expose
    def make_event_dictionaries(self):
        return self._eventbrowser().make_event_dictionaries()

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    @cherrypy.tools.ajax(encoded=True)
    def get_file_list(self,tab):
        return self._eventbrowser().get_file_list(tab)

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def get_event_ids(self, criteria):
        return self._eventbrowser().get_event_ids(criteria)

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def get_plot(self, *args, **kwargs):
        self._eventbrowser().get_plot(*args, **kwargs)
        return

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    #@cherrypy.tools.ajax(encoded=True)
    def get_plotly(self,*args,**kwargs):
        self._eventbrowser().get_plotly(*args,**kwargs)
        return

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def get_RD_shower_data_quantities(self,tab):
        self._eventbrowser().get_rd_shower_data_quantities(tab)
        return

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    @cherrypy.tools.ajax(encoded=True)
    def get_RD_event_info(self,tab,quantity):
        return self._eventbrowser().get_RD_event_info(tab,quantity)

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    @cherrypy.tools.ajax(encoded=True)
    def update_event(self, criteria):
        return self._eventbrowser().update_event(criteria)

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def read_next_event(self, criteria):
        return self._eventbrowser().read_next_event(criteria)

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def read_previous_event(self, criteria):
        return self._eventbrowser().read_previous_event(criteria)

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def go_to_event(self, event_number):
        return self._eventbrowser().go_to_event(int(event_number))

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    @cherrypy.tools.ajax(encoded=True)
    def save_plot(self, saveas, *args, **kwargs):
        return self._eventbrowser().save_plot(saveas=saveas, *args, **kwargs)

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def get_offline_configuration(self,module_name,tab):
        self._eventbrowser().get_offline_configuration(module_name,tab)
        return

    @cherrypy.expose
    @cherrypy.tools.ajax()
    @cherrypy.tools.json_parameters()
    def get_rd_station_data_quantities(self,tab):
        self._eventbrowser().get_rd_station_data_quantities(tab)
        return

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def get_RD_stations(self, only_signal,tab):
        self._eventbrowser().get_RD_stations(only_signal,tab)
        return

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    @cherrypy.tools.ajax(encoded=True)
    def get_RD_station_info(self, tab,station_id=0,quantity=None):
        self._eventbrowser().get_RD_station_info(int(station_id),tab,quantity)
        return

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def get_rd_station_position(self, station_id):
        return self._eventbrowser().get_rd_station_position(station_id)

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def get_sd_station_position(self, station_id):
        return self._eventbrowser().get_sd_station_position(station_id)

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def get_rd_channel_length(self, station_id):
        return self._eventbrowser().get_rd_channel_length(station_id)

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def get_SD_event_info(self,tab):
        self._eventbrowser().get_SD_event_info(tab)
        return

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    @cherrypy.tools.ajax(encoded=True)
    def get_sd_stations(self,tab):
        return self._eventbrowser().get_sd_stations(tab)

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def go_to_auger_id(self, auger_id):
        return self._eventbrowser().go_to_auger_id(auger_id)

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    def go_to_rd_event_number(self, rd_run_number, rd_event_id):
        return self._eventbrowser().go_to_rd_event_number(rd_run_number, rd_event_id)

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    def go_to_sd_event_number(self, sd_event_id):
        return self._eventbrowser().go_to_sd_event_number(sd_event_id)

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    def go_to_fd_event_number(self, fd_id, fd_run):
        return self._eventbrowser().go_to_fd_event_number(fd_id, fd_run)

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def get_fd_event_info(self, eye,tab):
        return self._eventbrowser().get_fd_event_info(eye,tab)

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    def get_fd_eyes(self):
        return self._eventbrowser().get_fd_eyes()

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def get_fd_pixel_ids(self, eye=-1):
        return self._eventbrowser().get_fd_pixel_ids(eye)

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def get_fd_first_pixel_id(self, eye=-1):
        return self._eventbrowser().get_fd_first_pixel_id(eye)

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    @cherrypy.tools.ajax(encoded=True)
    def get_event_categories(self,tab):
        self._eventbrowser().get_event_categories(tab)
        return

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    @cherrypy.tools.ajax(encoded=True)
    def get_incoming_directions(self,galactic_coordinates=True, horizon_coordinates=False,equatorial_coordinates=False, sd=True,rd=False):
        reply= self._eventbrowser().get_incoming_directions(galactic_coordinates=galactic_coordinates,horizon_coordinates=horizon_coordinates,equatorial_coordinates=equatorial_coordinates,sd=sd,rd=rd)
        return reply