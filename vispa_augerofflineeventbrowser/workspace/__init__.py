import logging
import os
# noinspection PyPep8Naming
import json as JSON
import matplotlib.pyplot as plt
import mpld3
import ROOT
import threading
import vispa
import numpy as np
import math
from matplotlib.lines import Line2D
from matplotlib.path import Path
from matplotlib.collections import PatchCollection
from mpld3 import plugins
import matplotlib.patches as mpatches
import mpld3_plugins
import tempfile
import glob

logger = logging.getLogger(__name__)


def _get_pixel_vertices(pix_id, elevation, azimuth):
    n_row = 22
    i_col = int(pix_id / n_row) + 1
    i_row = pix_id % n_row + 1
    pixel_size = 1.5 * math.pi / 180.
    d_omega = pixel_size
    d_phi = math.sqrt(3) * pixel_size / 2.
    if i_row % 2 == 1:
        beta = -(i_col - 10.) * d_omega
    else:
        beta = -(i_col - 10.5) * d_omega
    alpha = -(11.66667 - i_row) * d_phi
    alpha_m = elevation
    pixel_width = math.sqrt(3) / 2. * math.pi / 180.
    xx = pixel_width * math.sqrt(3) / 2.
    yy = pixel_width * .5
    x = [0., xx, xx, 0., -xx, -xx, 0.]
    y = [-pixel_width, -yy, yy, pixel_width, yy, -yy, -pixel_width]
    verts = []
    for k in range(len(x)):
        x[k] += beta
        y[k] += alpha
        sin_delta = math.sin(y[k] + alpha_m) * math.cos(y[k])
        delta = math.asin(sin_delta)
        sin_phi = math.sin(x[k]) / math.cos(delta)
        phi = math.asin(sin_phi)
        verts.append((phi * 180. / math.pi + azimuth, delta * 180. / math.pi))
    return verts


def _get_gaisser_hillas(x, params):
    delta_x = params[0] - params[1]
    if delta_x == 0:
        return 0
    if (x - params[1]) / delta_x <= 0:
        return 0
    term_1 = math.pow((x - params[1]) / delta_x, delta_x / params[3])
    term_2 = math.exp((params[0] - x) / params[3])
    return params[2] * term_1 * term_2


# noinspection PyTypeChecker
class AugerOfflineEventbrowser:
    def __init__(self, view_id,window_id,offline_path=None):
        self._view_id=view_id
        self._window_id=window_id
        self._topic="extension.%s.socket" % view_id
        self.convert_enums = None
        self.file_names = []
        self.files = ROOT.std.vector("string")()
        self.event = None
        self.geo = None
        self.file = None
        self.file_info = None
        self.event_index_dic = []
        self.sd_event_id_dic = []
        self.sd_azimuth_dic = []
        self.sd_zenith_dic = []
        self.sd_energy_dic = []
        self.sd_stations_dic = []
        self.rd_run_number_dic = []
        self.rd_event_id_dic = []
        self.rd_stations_dic = []
        self.rd_zenith_dic = []
        self.rd_azimuth_dic = []
        self.fd_zenith_dic = []
        self.fd_energy_dic = []
        self.fd_eye_number_dic = []
        self.x_max_dic = []
        self.fd_id_dic = []
        self.fd_run_dic = []
        self.auger_id_dic = []
        self.event_dics_loaded = False
        if offline_path is not None:
            os.environ["AUGEROFFLINEROOT"] = os.path.expandvars(offline_path)
        self.__event_number = 0  # counting starts at zero
        self.__number_of_events = 0
        self._rd_rec_stage_dic = {
            0: "unsuccessful/none",
            50: "BaryCenter",
            100: "PrePlaneFit",
            105: "PlaneFitLinear",
            110: "PlaneFitLinear",
            120: "PlaneFit3d",
            150: "SphericalWaveFit",
            160: "SphericalWafeFitVarC",
            170: "ConicalWaveFit",
            180: "LDFFit1d",
            190: "HyperbolicWaveFit",
            200: "LDFFit2d",
            220: "LDFFit2dWithCore"
        }

    def check_offline_environment_variable(self):
        if "AUGEROFFLINEROOT" in os.environ:
            return True
        else:
            return False

    def open_ADST(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_open_ADST,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_open_ADST(self, filenames, offline_path,event_number=0):
        if offline_path is not None:
            offline_path = os.path.expandvars(offline_path)
            station_rrec_exists = os.path.exists(os.path.join(offline_path, "include/adst/StationRRecDataQuantities.h"))
            shower_rrec_exists = os.path.exists(os.path.join(offline_path, "include/adst/ShowerRRecDataQuantities.h"))
            channel_rrec_exists = os.path.exists(os.path.join(offline_path, "include/adst/ChannelRRecDataQuantities.h"))
            if not (channel_rrec_exists or shower_rrec_exists or station_rrec_exists):
                vispa.remote.send_topic(self._topic+".openADST",window_id=self._window_id,data=JSON.dumps({"success": False, "reason": 1, "info": "Offline module could not be imported. "
                                           "Please check if the Offline path is set correctly"}))
                return
            elif not (channel_rrec_exists and shower_rrec_exists and station_rrec_exists):
                info_text = ""
                if not channel_rrec_exists:
                    info_text += "File ChannelRRecDataQuantities.h not in offline path <br>"
                if not station_rrec_exists:
                    info_text += "File StationRRecDataQuantities.h not in offline path <br>"
                if not shower_rrec_exists:
                    info_text += "File ShowerRRecDataQuantities.h not in offline path <br>"
                vispa.remote.send_topic(self._topic+".openADST",window_id=self._window_id,data=JSON.dumps({"success": False, "reason": 1, "info": info_text}))
                return
            os.environ["AUGEROFFLINEROOT"] = offline_path
        ROOT.gSystem.Load("$AUGEROFFLINEROOT/lib/libRecEventKG.so")
        try:
            # noinspection PyUnresolvedReferences
            from radiotools import convertEnums
        except ImportError as e:
            vispa.remote.send_topic(self._topic+".openADST",window_id=self._window_id,data=JSON.dumps({"success": False, 'reason': 2, "info": "Could not import converEnums.py, %s" % e}))
            return
        self.convert_enums = convertEnums
        if not self.file is None:
            self.file.Close()
            self.geo.Delete()
            del self.file
            del self.geo
            self.file = None
            self.geo = None
        for name in filenames:
            filename = str(os.path.expanduser(os.path.expandvars(name)))
            if not os.path.exists(filename):
                vispa.remote.send_topic(self._topic+".openADST",window_id=self._window_id,data=JSON.dumps({'success': False, 'reason': 3, 'info': 'ADST file %s does not exist.' % filename}))
                return
            self.file_names.append(filename)
            self.files.push_back(filename)
        self.file = ROOT.RecEventFile(self.files)
        if self.file.GetNEvents() == 0:
            vispa.remote.send_topic(self._topic+".openADST",window_id=self._window_id,data=JSON.dumps(
                {'success': False, 'reason': 4, 'info': 'File contains no events or is not an actual ADST file.'}))
            return
        self.geo = ROOT.DetectorGeometry()
        self.event = ROOT.RecEvent()
        self.file.SetBuffers(self.event)
        self.file.ReadDetectorGeometry(self.geo)
        self.__number_of_events = self.file.GetNEvents()
        self.file_info = ROOT.FileInfo()
        self.file.ReadFileInfo(self.file_info)
        self.__event_number=int(event_number)
        if not self.file.ReadEvent(self.__event_number):
            vispa.remote.send_topic(self._topic+".openADST",window_id=self._window_id,data=JSON.dumps({'success': True,"reason":-1,"event_number": event_number}))
            return
        else:
            vispa.remote.send_topic(self._topic+".openADST",window_id=self._window_id,data=JSON.dumps({'success': False, 'reason': 4, 'info': 'Open ADST returned error. The Event Browser might not run properly.'}))
            return

    def add_ADST(self, filenames):
        filename = "None"
        for name in filenames:
            filename = str(os.path.expanduser(os.path.expandvars(name)))
            if not os.path.exists(filename):
                return JSON.dumps({"success": False, "info": "File %s does not exist" % filename})
            self.file_names.append(filename)
            self.files.push_back(filename)
        self.file.SetBuffers(self.event)
        self.__number_of_events = self.file.GetNEvents()
        self.file.ReadFileInfo(self.file_info)
        if self.read_event(self.__event_number):
            return JSON.dumps({"success": True})
        else:
            return JSON.dumps({"success": False, "info": "ADST file %s could not be opened" % filename})

    def read_event(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_read_event,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_read_event(self, event_number):
        if self.file.ReadEvent(self.__event_number) == ROOT.RecEventFile.eSuccess:
            vispa.remote.send_topic(self._topic+".eventChanged",window_id=self._window_id,data=JSON.dumps({"reload": True , "event_id": self.__event_number}))
            return
        else:
            vispa.remote.send_topic(self._topic+".eventChanged",window_id=self._window_id,data=JSON.dumps({"reload": False , "event_id": self.__event_number}))

    def read_next_event(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_read_next_event,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_read_next_event(self, criteria):
        if self.__event_number == self.__number_of_events - 1:
            i_event = 0
        else:
            i_event = self.__event_number + 1
        while i_event != self.__event_number:
            if self.sd_stations_dic[i_event] >= criteria[0] and (
                            self.sd_stations_dic[i_event] <= criteria[1] or criteria[1] == 50):
                if self.fd_eye_number_dic[i_event] >= criteria[2] and (
                                self.fd_eye_number_dic[i_event] <= criteria[3] or criteria[3] == 5):
                    if self.rd_stations_dic[i_event] >= criteria[4] and (
                                    self.rd_stations_dic[i_event] <= criteria[5] or criteria[5] == 50):
                        if self.sd_energy_dic[i_event] >= criteria[6] * 1.e12 and (
                                        self.sd_energy_dic[i_event] <= criteria[7] * 1.e12 or criteria[7] == 1e9):
                            if criteria[8] <= self.sd_zenith_dic[i_event] <= criteria[9]:
                                self.__event_number = i_event
                                self.file.ReadEvent(self.__event_number)
                                vispa.remote.send_topic(self._topic+".eventChanged",window_id=self._window_id,data=JSON.dumps({"reload": True , "event_id": self.__event_number}))
                                return
            if i_event == self.__number_of_events - 1:
                i_event = 0
            else:
                i_event += 1
        vispa.remote.send_topic(self._topic+".eventChanged",window_id=self._window_id,data=JSON.dumps({"reload": True , "event_id": self.__event_number}))
        return

    def update_event(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_update_event,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_update_event(self, criteria):
        if self.sd_stations_dic[self.__event_number] >= criteria[0] and (
                    self.sd_stations_dic[self.__event_number] <= criteria[1] or criteria[1] == 50):
            if self.fd_eye_number_dic[self.__event_number] >= criteria[2] and (
                        self.fd_eye_number_dic[self.__event_number] <= criteria[3] or criteria[3] == 5):
                if self.rd_stations_dic[self.__event_number] >= criteria[4] and (
                            self.rd_stations_dic[self.__event_number] <= criteria[5] or criteria[5] == 50):
                    if self.sd_energy_dic[self.__event_number] >= criteria[6] * 1.e12 and (
                                self.sd_energy_dic[self.__event_number] <= criteria[7] * 1.e12 or
                                    criteria[7] == 1e9):
                        if criteria[8] <= self.sd_zenith_dic[self.__event_number] <= criteria[9]:
                            vispa.remote.send_topic(self._topic+".eventChanged",window_id=self._window_id,data=JSON.dumps({"reload": False , "event_id": self.__event_number}))
                            return
        if self.__event_number == self.__number_of_events - 1:
            i_event = 0
        else:
            i_event = self.__event_number + 1
        while i_event != self.__event_number:
            if self.sd_stations_dic[i_event] >= criteria[0] and (
                            self.sd_stations_dic[i_event] <= criteria[1] or criteria[1] == 50):
                if self.fd_eye_number_dic[i_event] >= criteria[2] and (
                                self.fd_eye_number_dic[i_event] <= criteria[3] or criteria[3] == 5):
                    if self.rd_stations_dic[i_event] >= criteria[4] and (
                                    self.rd_stations_dic[i_event] <= criteria[5] or criteria[5] == 50):
                        if self.sd_energy_dic[i_event] >= criteria[6] * 1.e12 and (
                                        self.sd_energy_dic[i_event] <= criteria[7] * 1.e12 or criteria[7] == 1e9):
                            if criteria[8] <= self.sd_zenith_dic[i_event] <= criteria[9]:
                                if self.__event_number == i_event:
                                    vispa.remote.send_topic(self._topic+".eventChanged",window_id=self._window_id,data=JSON.dumps({"reload": False , "event_id": self.__event_number}))
                                    return
                                else:
                                    self.__event_number = i_event
                                    self.file.ReadEvent(self.__event_number)
                                    vispa.remote.send_topic(self._topic+".eventChanged",window_id=self._window_id,data=JSON.dumps({"reload": True , "event_id": self.__event_number}))
                                    return
            if i_event == self.__number_of_events - 1:
                i_event = 0
            else:
                i_event += 1
        vispa.remote.send_topic(self._topic+".eventChanged",window_id=self._window_id,data=JSON.dumps({"reload": False , "event_id": self.__event_number}))
        return

    def read_previous_event(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_read_previous_event,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_read_previous_event(self, criteria):
        if self.__event_number == 0:
            i_event = self.__number_of_events - 1
        else:
            i_event = self.__event_number - 1
        while i_event != self.__event_number:
            if self.sd_stations_dic[i_event] >= criteria[0] and (
                            self.sd_stations_dic[i_event] <= criteria[1] or criteria[1] == 50):
                if self.fd_eye_number_dic[i_event] >= criteria[2] and (
                                self.fd_eye_number_dic[i_event] <= criteria[3] or criteria[3] == 5):
                    if self.rd_stations_dic[i_event] >= criteria[4] and (
                                    self.rd_stations_dic[i_event] <= criteria[5] or criteria[5] == 50):
                        if self.sd_energy_dic[i_event] >= criteria[6] * 1.e12 and (
                                        self.sd_energy_dic[i_event] <= criteria[7] * 1.e12 or criteria[7] == 1e9):
                            if criteria[8] <= self.sd_zenith_dic[i_event] <= criteria[9]:
                                self.__event_number = i_event
                                self.file.ReadEvent(self.__event_number)
                                vispa.remote.send_topic(self._topic+".eventChanged",window_id=self._window_id,data=JSON.dumps({"reload": True , "event_id": self.__event_number}))
                                return
            if i_event == 0:
                i_event = self.__number_of_events - 1
            else:
                i_event -= 1
        vispa.remote.send_topic(self._topic+".eventChanged",window_id=self._window_id,data=JSON.dumps({"reload": True , "event_id": self.__event_number}))
        return


    def go_to_event(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_go_to_event,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_go_to_event(self, event_number):
        self.__event_number = event_number
        self.file.ReadEvent(event_number)
        self.__event_number = event_number
        vispa.remote.send_topic(self._topic+".eventChanged",window_id=self._window_id,data=JSON.dumps({"reload": True , "event_id": self.__event_number}))
        return

    def make_event_dictionaries(self):
        thread=threading.Thread(target=self._do_make_event_dictionaries)
        thread.deamon=True
        thread.running=True
        thread.run()
        return

    def _do_make_event_dictionaries(self):
        event_info = ROOT.EventInfo()
        for i in range(self.__number_of_events):
            self.file.GetEventInfo(i, event_info)
            # general event properties
            self.event_index_dic.append(i)
            self.auger_id_dic.append(event_info.GetAugerId())
            # sd event properties
            self.sd_event_id_dic.append(event_info.GetSDId())
            self.sd_zenith_dic.append(event_info.GetSDZenith() * 180. / math.pi)
            self.sd_energy_dic.append(float(event_info.GetSDEnergy()))
            self.sd_stations_dic.append(event_info.GetNumberOfStations())
            # fd event properties
            self.x_max_dic.append(event_info.GetXmax(0))
            self.fd_zenith_dic.append(event_info.GetFDZenith(0) * 180. / math.pi)
            self.fd_energy_dic.append(event_info.GetFDEnergy(0))
            self.fd_eye_number_dic.append(event_info.GetNumberOfEyes())
            self.fd_id_dic.append(event_info.GetEventId(0))
            self.fd_run_dic.append(event_info.GetRunId(0))
            # rd event properties
            self.rd_run_number_dic.append(event_info.GetRdRunNumber())
            self.rd_event_id_dic.append(event_info.GetRdEventId())
            self.rd_zenith_dic.append(event_info.GetRdZenith() * 180. / math.pi)
            self.rd_azimuth_dic.append(event_info.GetRdAzimuth() * 180. / math.pi)
            self.rd_stations_dic.append(event_info.GetRdNumberOfStations())
        self.event_dics_loaded = True
        vispa.remote.send_topic(self._topic+".madeEventDictionaries",window_id=self._window_id)
        return

    def get_file_list(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_get_file_list,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_get_file_list(self,tab):
        reply = []
        for item in self.file_names:
            reply.append({
                "filename": item
            })
        fd, path=tempfile.mkstemp()
        os.write(fd,JSON.dumps([reply,tab]))
        os.close(fd)
        vispa.remote.send_topic(self._topic+".getFileList",window_id=self._window_id,data=path)
        return

    def get_event_categories(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_get_event_categories,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_get_event_categories(self,tab):
        if not self.event_dics_loaded:
            self.make_event_dictionaries()
        sd = 0
        fd = 0
        rd = 0
        sdfd = 0
        sdrd = 0
        sdfdrd = 0
        for i in self.event_index_dic:
            if self.sd_stations_dic[i] > 0:
                sd += 1
                if self.rd_stations_dic[i] > 0:
                    sdrd += 1
                if self.fd_eye_number_dic[i] > 0:
                    sdfd += 1
                    if self.rd_stations_dic[i] > 0:
                        sdfdrd += 1
            if self.fd_eye_number_dic[i] > 0:
                fd += 1
            if self.rd_stations_dic[i] > 0:
                rd += 1
        reply=JSON.dumps({
            "sd": sd,
            "fd": fd,
            "rd": rd,
            "sdfd": sdfd,
            "sdrd": sdrd,
            "sdfdrd": sdfdrd
        })
        fd, path=tempfile.mkstemp()
        os.write(fd,JSON.dumps([reply,tab]))
        os.close(fd)
        vispa.remote.send_topic(self._topic+".getEventCategories",window_id=self._window_id,data=path)
        return

    def get_event_overview_histogram(self, requested_property, conditions, limits):
        if not self.event_dics_loaded:
            self.make_event_dictionaries()
        masks = self._get_overview_masks(conditions, limits)
        # Set the binning value to the default value of bins in numpy, just in case something unpredicted happens
        binning = 10
        data = None
        if requested_property == "sdZenith":
            data = self.sd_zenith_dic
            xlabel="Zenith [degrees]"
            bins_min=0
            bins_max=90
            bins_width=5
        elif requested_property == "sdEnergy":
            data = []
            for energy in self.sd_energy_dic:
                if energy>0:
                    data.append(np.log10(energy))
                else:
                    data.append(0)
            bins_min=14
            bins_max=21
            bins_width=.25
            xlabel="log10(SD Energy [eV])"
        elif requested_property == "sdStations":
            data = self.sd_stations_dic
            bins_min=0
            bins_max=100
            bins_width=1
            xlabel="# of SD Stations"
        elif requested_property == "xMax":
            data = self.x_max_dic
            bins_min=200
            bins_max=1000
            bins_width=10
            xlabel="Xmax [g/cm^2]"
        elif requested_property == "fdEnergy":
            data = []
            if len(self.fd_energy_dic)>0:
                for energy in self.fd_energy_dic:
                    if energy>0:
                        data.append(np.log10(energy))
                    else:
                        data.append(0)
            xlabel="log10(FD Energy [eV])$"
            bins_min=14
            bins_max=21
            bins_width=.25
        elif requested_property == "fdZenith":
            data = self.fd_zenith_dic
            bins_min=0
            bins_max=90
            bins_width=5
            xlabel="Zenith [degrees]"
        elif requested_property == "fdEyes":
            data = self.fd_eye_number_dic
            bins_min=0
            bins_max=5
            bins_width=1
            xlabel="# of FD Eyes"
        elif requested_property == "rdStations":
            data = self.rd_stations_dic
            xlabel="# of RD Stations"
            bins_min=0
            bins_max=100
            bins_width=1
        elif requested_property == "rdAzimuth":
            data = self.rd_azimuth_dic
            xlabel="Azimuth [degrees]"
            bins_min=0
            bins_max=360
            bins_width=5
        elif requested_property == "rdZenith":
            data = self.rd_zenith_dic
            xlabel="Zenith [degrees]"
            bins_min=0
            bins_max=90
            bins_width=5

        if data is None or len(data)==0:
            return None
        indices = np.array(self.event_index_dic)
        if len(masks) > 0:
            for mask in masks:
                masked_indices = np.array(self.event_index_dic)[mask]
                indices = np.intersect1d(indices, masked_indices)
        plotting_data = []
        if len(indices)>0:
            for index in indices:
                plotting_data.append(data[index])
        if len(plotting_data) > 0:
            return {
                "data": list(plotting_data),
                "xlabel": xlabel,
                "bins_min": bins_min,
                "bins_max": bins_max,
                "bins_width": bins_width
            }
        else:
            return None

    def _get_overview_masks(self, conditions, limits):
        masks = []
        for i in range(len(conditions)):
            condition = conditions[i]
            limit = limits[i]
            if condition == "sdMinEnergy":
                masks.append(np.array(self.sd_energy_dic) > float(limit))
            if condition == "sdMinZenith":
                masks.append(np.array(self.sd_zenith_dic) >= float(limit))
            if condition == "sdMaxZenith":
                masks.append(np.array(self.sd_zenith_dic) <= float(limit))
            if condition == "sdMinStations":
                masks.append(np.array(self.sd_stations_dic) >= int(limit))
            if condition == "rdMinZenith":
                masks.append(np.array(self.rd_zenith_dic) >= float(limit))
            if condition == "rdMaxZenith":
                masks.append(np.array(self.rd_zenith_dic) <= float(limit))
            if condition == "rdMinStations":
                masks.append(np.array(self.rd_stations_dic) >= int(limit))
            if condition == "minXmax":
                masks.append(np.array(self.x_max_dic) >= float(limit))
            if condition == "maxXmax":
                masks.append(np.array(self.x_max_dic) <= float(limit))
            if condition == "fdMinEnergy":
                masks.append(np.array(self.fd_energy_dic) >= float(limit))
            if condition == "fdMinZenith":
                masks.append(np.array(self.fd_zenith_dic) >= float(limit))
            if condition == "fdMaxZenith":
                masks.append(np.array(self.fd_zenith_dic) <= float(limit))
            if condition == "fdMinEyes":
                masks.append(np.array(self.fd_eye_number_dic) >= int(limit))
        return masks

    def get_event_ids(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_get_event_ids,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_get_event_ids(self, criteria):
        reply = []
        if not self.event_dics_loaded:
            self.make_event_dictionaries()
        for i in range(len(self.event_index_dic)):
            if self.sd_stations_dic[i] >= criteria[0] and (self.sd_stations_dic[i] <= criteria[1] or criteria[1] == 50):
                if self.fd_eye_number_dic[i] >= criteria[2] and (
                                self.fd_eye_number_dic[i] <= criteria[3] or criteria[3] == 5):
                    if self.rd_stations_dic[i] >= criteria[4] and (
                                    self.rd_stations_dic[i] <= criteria[5] or criteria[5] == 50):
                        if self.sd_energy_dic[i] >= criteria[6] * 1.e12 and (
                                        self.sd_energy_dic[i] <= criteria[7] * 1.e12 or criteria[7] == 1e9):
                            if criteria[8] <= self.sd_zenith_dic[i] <= criteria[9]:
                                reply.append({
                                    "i": i,
                                    "auger_id": self.auger_id_dic[i],
                                    "sd_id": self.sd_event_id_dic[i],
                                    "fd_id": self.fd_id_dic[i],
                                    "fd_run": self.fd_run_dic[i],
                                    "rd_id": self.rd_event_id_dic[i],
                                    "rd_run": self.rd_run_number_dic[i]
                                })
        fd, path=tempfile.mkstemp()
        os.write(fd,JSON.dumps(reply))
        os.close(fd)
        vispa.remote.send_topic(self._topic+".getEventIds",window_id=self._window_id,data=path)
        return JSON.dumps(reply)

    def go_to_auger_id(self, auger_id):
        if not self.event_dics_loaded:
            self.make_event_dictionaries()
        for i in range(len(self.event_index_dic)):
            if self.auger_id_dic[i] == int(auger_id):
                self.file.ReadEvent(i)
                self.__event_number = i
                return JSON.dumps({"success": True})
        return JSON.dumps({"success": False})

    def go_to_rd_event_number(self, rd_run_number, rd_event_id):
        if not self.event_dics_loaded:
            self.make_event_dictionaries()
        for i in range(len(self.event_index_dic)):
            if (self.rd_run_number_dic[i] == int(rd_run_number)) and (self.rd_event_id_dic[i] == int(rd_event_id)):
                self.file.ReadEvent(i)
                self.__event_number = i
                return JSON.dumps({"success": True})
        return JSON.dumps({"success": False})

    def go_to_sd_event_number(self, sd_event_id):
        if not self.event_dics_loaded:
            self.make_event_dictionaries()
        for i in range(len(self.event_index_dic)):
            if self.sd_event_id_dic[i] == int(sd_event_id):
                self.__event_number = i
                self.file.ReadEvent(i)
                return JSON.dumps({"success": True})
        return JSON.dumps({"success": False})

    def go_to_fd_event_number(self, fd_id, fd_run):
        if not self.event_dics_loaded:
            self.make_event_dictionaries()
        for i in range(len(self.event_index_dic)):
            if (self.fd_id_dic[i] == int(fd_id)) and (self.fd_run_dic[i] == int(fd_run)):
                self.file.ReadEvent(i)
                self.__event_number = i
                return JSON.dumps({"success": True})
        return JSON.dumps({"success": False})

    def get_plotly(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_get_plotly,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_get_plotly(self,plot_name,tab,*args,**kwargs):
        data=getattr(self,"get_"+plot_name)(*args,**kwargs)
        fd,path=tempfile.mkstemp()
        os.write(fd,JSON.dumps([plot_name,tab,data]))
        os.close(fd)
        vispa.remote.send_topic(self._topic+".getPlotly",window_id=self._window_id,data=path)
        return


    def get_plot(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_get_plot,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.run()
        return

    def _do_get_plot(self, plot_name,tab, interactive=True, save_as="", *args, **kwargs):
        """ generic plot function. Each plotting routine should be called through
            this function """
        fig = getattr(self, "get_" + plot_name)(*args, **kwargs)
        if fig is None:
            fd, path=tempfile.mkstemp()
            os.write(fd,JSON.dumps([plot_name,tab,None]))
            os.close(fd)
            vispa.remote.send_topic(self._topic+".getPlot",window_id=self._window_id,data=path)
            return
        fig.tight_layout()
        if save_as != "":
            if os.path.exists(os.path.dirname(save_as)):
                fig.savefig(save_as)
                return
            else:
                logger.debug("path %s does not exist" % save_as)
                return

        if interactive:
            fd, path=tempfile.mkstemp()
            os.write(fd,JSON.dumps([plot_name,tab,mpld3.fig_to_dict(fig)]))
            os.close(fd)
            vispa.remote.send_topic(self._topic+".getPlot",window_id=self._window_id,data=path)
            return
        else:
            logger.debug("not implemented")

    def save_plot(self, saveas, *args, **kwargs):
        if saveas.lower().endswith((
                ".eps", ".jpg", ".jpeg", ".pdf", ".pgf", ".png", ".ps", ".raw", ".rgba", ".svg", ".svgz",
                ".tif", ".tiff")):
            self.get_plot(save_as=os.path.expandvars(saveas), *args, **kwargs)
            return JSON.dumps({"success": True, "error_message": None})
        else:
            return JSON.dumps(
                {"success": False,
                 "error_message": "Unsupported file format. Supported formats "
                                  "are: .png, .jpg, .jpeg, .pdf, .tif, .eps, "
                                  ".pgf, .ps, .raw, .rgba, .svg, .svgz, .tiff"})

    def get_offline_configuration(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_get_offline_configuration,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_get_offline_configuration(self,module_name,tab):
        offline_version=self.file_info.GetOfflineVersion()
        offline_configuration=self.file_info.GetOfflineConfiguration()
        start_int=offline_configuration.find("<"+module_name+">")
        stop_int=offline_configuration.find("</"+module_name+">")
        if module_name=="":
            module_config=""
        elif start_int!=-1 and stop_int!=-1:
            module_config=offline_configuration[start_int:stop_int]+"</"+module_name+">"
        else:
            module_config="Not found"
        reply=JSON.dumps({
            "offline_version": offline_version,
            "module_config": module_config
        })
        fd, path=tempfile.mkstemp()
        os.write(fd,JSON.dumps([reply,tab]))
        os.close(fd)
        vispa.remote.send_topic(self._topic+".getOfflineConfiguration",window_id=self._window_id,data=path)
        return

    def get_rd_shower_data_quantities(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_get_rd_shower_data_quantities,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_get_rd_shower_data_quantities(self,tab):
        data_quantities=[]
        for element in self.convert_enums.rdshQ.elements:
            data_quantities.append(str(element))
        fd, path=tempfile.mkstemp()
        os.write(fd,JSON.dumps([data_quantities,tab]))
        os.close(fd)
        vispa.remote.send_topic(self._topic+".getRdShowerDataQuantities",window_id=self._window_id,data=path)
        return

    def get_RD_event_info(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_get_RD_event_info,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_get_RD_event_info(self,tab,quantity):
        rdevent = self.event.GetRdEvent()
        rdshower = rdevent.GetRdRecShower()
        event = rdevent.GetRdEventId()
        run = rdevent.GetRdRunNumber()
        gps_second = rdevent.GetGPSSecond()
        gps_nanosecond = rdevent.GetGPSNanoSecond()
        yymmdd = self.event.GetYYMMDD()
        yy = yymmdd / 10000
        mm = (yymmdd - yy * 10000) / 100
        dd = yymmdd - yy * 10000 - mm * 100
        date = str(dd) + "." + str(mm) + ".20" + str(yy)
        hhmmss = self.event.GetHHMMSS()
        hh = hhmmss / 10000
        mm = (hhmmss - hh * 10000) / 100
        ss = (hhmmss - hh * 10000 - mm * 100)
        timestamp = str(hh) + ":" + str(mm) + ":" + str(ss)
        if not np.isnan(round(rdshower.GetAzimuth() * 180. / math.pi, 2)):
            azimuth = round(rdshower.GetAzimuth() * 180. / math.pi, 2)
        else:
            azimuth=None
        if not np.isnan(round(rdshower.GetZenith() * 180. / math.pi, 2)):
            zenith = round(rdshower.GetZenith() * 180. / math.pi, 2)
        else:
            zenith=None
        if not np.isnan(rdshower.GetZenithError()):
            zenith_error = round(rdshower.GetZenithError() * 180. / math.pi, 2)
        else:
            zenith_error = None
        if not np.isnan(rdshower.GetAzimuthError()):
            azimuth_error = round(rdshower.GetAzimuthError() * 180. / math.pi, 2)
        else:
            azimuth_error = None
        stations = len(rdevent.GetRdStationVector())
        if not np.isnan(round(rdshower.GetRadius(), 2)):
            radius = round(rdshower.GetRadius(), 2)
        else:
            radius=None
        if rdshower.HasParameter(self.convert_enums.rdshQ.eRecStage):
            rec_stage = self._rd_rec_stage_dic.get(
                int(rdshower.GetParameter(self.convert_enums.rdshQ.eRecStage) * 100.))
        else:
            rec_stage = None
        if rdshower.HasParameter(50):
            energy = round(rdshower.GetParameter(50))
        if rdshower.HasParameterCovariance(50,50):
            energy_error = round(rdshower.GetParameterError(50))
        else:
            energy = None
            energy_error = None
        if quantity is None:
            quantity_value="None Selected"
            quantity_error=None
        else:
            if rdshower.HasParameter(self.convert_enums.rdshQ.elements[quantity]):
                quantity_value=rdshower.GetParameter(self.convert_enums.rdshQ.elements[quantity])
                if rdshower.HasParameterCovariance(self.convert_enums.rdshQ.elements[quantity],self.convert_enums.rdshQ.elements[quantity]):
                    quantity_error=rdshower.GetParameterError(self.convert_enums.rdshQ.elements[quantity])
                    if rdshower.GetParameterError(self.convert_enums.rdshQ.element[quantity])>0:
                        quantity_value=np.around(quantity_value,int(-np.log10(quantity_error)))
                        quantity_error=np.around(quantity_error,int(-np.log10(quantity_error)+1))
                    else:
                        if quantity_value!=0:
                            quantity_value=np.around(quantity_value,int(max(2,-np.log10(abs(quantity_value)+2))))
                else:
                    quantity_error=None
                    if quantity_value!=0:
                        quantity_value=np.around(quantity_value,int(max(2,-np.log10(abs(quantity_value)+2))))
            else:
                quantity_value="Not Set"
                quantity_error=None
        reply = {
            "event": event,
            "run": run,
            "GPSseconds": gps_second,
            "date": date,
            "time": timestamp,
            "GPSnanoseconds": gps_nanosecond,
            "azimuth": azimuth,
            "zenithError": zenith_error,
            "azimuthError": azimuth_error,
            "zenith": zenith,
            "stations": stations,
            "radius": radius,
            "recStage": rec_stage,
            "energy": energy,
            "energyError": energy_error,
            "quantity": quantity,
            "quantityValue": quantity_value,
            "quantityError": quantity_error
        }

        fd, path=tempfile.mkstemp()
        os.write(fd,JSON.dumps([reply,tab]))
        os.close(fd)
        vispa.remote.send_topic(self._topic+".getRdEventInfo",window_id=self._window_id,data=path)
        return

    def get_RD_stations(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_get_RD_stations,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()

    def _do_get_RD_stations(self, only_signal,tab):
        rdevent = self.event.GetRdEvent()
        if rdevent is None:
            fd, path=tempfile.mkstemp()
            os.write(fd,JSON.dumps([None,tab]))
            os.close(fd)
            vispa.remote.send_topic(self._topic+".getRdStations",window_id=self._window_id,data=path)
            return
        stations = rdevent.GetRdStationVector()
        if len(stations) == 0:
            fd, path=tempfile.mkstemp()
            os.write(fd,JSON.dumps([None,tab]))
            os.close(fd)
            vispa.remote.send_topic(self._topic+".getRdStations",window_id=self._window_id,data=path)
            return
        reply = []
        for station in stations:
            if station.HasPulse() or not only_signal:
                if station.HasParameter(self.convert_enums.rdstQ.eSignal):
                    signal = round(1000000. * station.GetParameter(self.convert_enums.rdstQ.eSignal), 2)
                else:
                    signal = None
                if station.HasParameterError(self.convert_enums.rdstQ.eSignal):
                    signal_error = round(1000000. * station.GetParameterError(self.convert_enums.rdstQ.eSignal), 2)
                else:
                    signal_error = None
                if station.HasParameter(self.convert_enums.rdstQ.eSignalToNoise):
                    signal_to_noise = round(station.GetParameter(self.convert_enums.rdstQ.eSignalToNoise), 2)
                else:
                    signal_to_noise = None
                if station.GetRejectionStatus() == 0:
                    rejected = False
                else:
                    rejected = True
                reply.append({
                    "Id": station.GetId(),
                    "HasPulse": station.HasPulse(),
                    "Signal": signal,
                    "SignalError": signal_error,
                    "SignalToNoise": signal_to_noise,
                    "Rejected": rejected
                })
        fd, path=tempfile.mkstemp()
        os.write(fd,JSON.dumps([reply,tab]))
        os.close(fd)
        vispa.remote.send_topic(self._topic+".getRdStations",window_id=self._window_id,data=path)
        return

    def get_RD_station_info(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_get_RD_station_info,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_get_RD_station_info(self, station_id,tab,quantity):
        rdevent = self.event.GetRdEvent()
        if rdevent is None:
            fd, path=tempfile.mkstemp()
            os.write(fd,JSON.dumps([None,tab]))
            os.close(fd)
            vispa.remote.send_topic(self._topic+".getRdStationInfo",window_id=self._window_id,data=path)
            return
        if len(rdevent.GetRdStationVector()) == 0:
            fd, path=tempfile.mkstemp()
            os.write(fd,JSON.dumps([None,tab]))
            os.close(fd)
            vispa.remote.send_topic(self._topic+".getRdStationInfo",window_id=self._window_id,data=path)
            return
        if station_id != 0:
            station = self.event.GetRdEvent().GetRdStationById(station_id)
        else:
            station = self.event.GetRdEvent().GetRdStationVector()[0]
        if station.HasParameter(self.convert_enums.rdstQ.eAngleToLorentzVector):
            beta = round(station.GetParameter(self.convert_enums.rdstQ.eAngleToLorentzVector) * 180. / math.pi, 2)
        else:
            beta = None
        if station.HasParameterError(self.convert_enums.rdstQ.eAngleToLorentzVector):
            beta_error = round(
                station.GetParameterError(self.convert_enums.rdstQ.eAngleToLorentzVector) * 180. / math.pi, 2)
        else:
            beta_error = None
        if station.HasParameter(self.convert_enums.rdstQ.eTimeResidual):
            time_residual = round(station.GetParameter(self.convert_enums.rdstQ.eTimeResidual), 2)
        else:
            time_residual = None
        if station.HasParameterError(self.convert_enums.rdstQ.eTimeResidual):
            time_residual_error = round(station.GetParameterError(self.convert_enums.rdstQ.eTimeResidual), 2)
        else:
            time_residual_error = None
        if (station.HasParameter(self.convert_enums.rdstQ.eSignalTime) and station.HasParameter(
                self.convert_enums.rdstQ.eTraceStartTime)):
            signal_peak = round(station.GetParameter(self.convert_enums.rdstQ.eSignalTime) - station.GetParameter(
                self.convert_enums.rdstQ.eTraceStartTime), 1)
        else:
            signal_peak = None
        if station.HasParameter(self.convert_enums.rdstQ.eSignalSearchWindowStart):
            signal_window_start = station.GetParameter(self.convert_enums.rdstQ.eSignalSearchWindowStart)
        else:
            signal_window_start = None
        if station.HasParameter(self.convert_enums.rdstQ.eSignalSearchWindowStop):
            signal_window_stop = station.GetParameter(self.convert_enums.rdstQ.eSignalSearchWindowStop)
        else:
            signal_window_stop = None
        if station.HasParameter(self.convert_enums.rdstQ.eNoiseRmsMag):
            noise_rms=np.around(station.GetParameter(self.convert_enums.rdstQ.eNoiseRmsMag)*1.e6,1)
        else:
            noise_rms=None
        if station.HasParameterError(self.convert_enums.rdstQ.eNoiseRmsMag):
            noise_rms_error=np.around(station.GetParameterError(self.convert_enums.rdstQ.eNoiseRmsMag)*1.e6,1)
        else:
            noise_rms_error=None


        rejection_status = station.GetRejectionStatus()
        rejection_reasons = []
        if rejection_status >= 2 ** 46:
            rejection_reasons.append("noisy")
            rejection_status -= 2 ** 46
        if rejection_status >= 2 ** 45:
            rejection_reasons.append("ext. triggered data corrupted")
            rejection_status -= 2 ** 45
        if rejection_status >= 2 ** 44:
            rejection_reasons.append("self triggered data corrupted")
            rejection_status -= 2 ** 44
        if rejection_status >= 2 ** 43:
            rejection_reasons.append("no weather station data")
            rejection_status -= 2 ** 43
        if rejection_status >= 2 ** 42:
            rejection_reasons.append("wrong timing")
            rejection_status -= 2 ** 42
        if rejection_status >= 2 ** 41:
            rejection_reasons.append("digitizer not calibrated")
            rejection_status -= 2 ** 41
        if rejection_status >= 2 ** 40:
            rejection_reasons.append("digitizer broken")
            rejection_status -= 2 ** 40
        if rejection_status >= 2 ** 39:
            rejection_reasons.append("LNA not calibrated")
            rejection_status -= 2 ** 39
        if rejection_status >= 2 ** 38:
            rejection_reasons.append("LNA broken")
            rejection_status -= 2 ** 38
        if rejection_status >= 2 ** 37:
            rejection_reasons.append("antenna broken")
            rejection_status -= 2 ** 37
        if rejection_status >= 2 ** 36:
            rejection_reasons.append("no communication")
            rejection_status -= 2 ** 36
        if rejection_status >= 4096:
            rejection_reasons.append("no noise information")
            rejection_status -= 4096
        if rejection_status >= 2048:
            rejection_reasons.append("pulse shape")
            rejection_status-= 2048
        if rejection_status >= 1024:
            rejection_reasons.append("antenna background level")
            rejection_status -= 1024
        if rejection_status >= 512:
            rejection_reasons.append("footprint deselected")
            rejection_status -= 512
        if rejection_status >= 256:
            rejection_reasons.append("no temperature correction")
            rejection_status -= 256
        if rejection_status >= 128:
            rejection_reasons.append("invalid signal search window")
            rejection_status -= 128
        if rejection_status >= 64:
            rejection_reasons.append("lonely station")
            rejection_status -= 64
        if rejection_status >= 32:
            rejection_reasons.append("polarization deselected")
            rejection_status -= 32
        if rejection_status >= 16:
            rejection_reasons.append("top down deselected")
            rejection_status -= 16
        if rejection_status >= 8:
            rejection_reasons.append("large time residual")
            rejection_status -= 8
        if rejection_status >= 4:
            rejection_reasons.append("bad SNR")
            rejection_status -= 4
        if rejection_status >= 2:
            rejection_reasons.append("beacon correction failed")
            rejection_status -= 2
        if rejection_status == 1:
            rejection_reasons.append("manually rejected")

        if quantity is None:
            quantity_value="None Selected"
            quantity_error=None
        else:
            if station.HasParameter(self.convert_enums.rdstQ.elements[quantity]):
                quantity_value=station.GetParameter(self.convert_enums.rdstQ.elements[quantity])
                if station.HasParameterError(self.convert_enums.rdstQ.elements[quantity]):
                    quantity_error=station.GetParameterError(self.convert_enums.rdstQ.elements[quantity])
                    if station.GetParameterError(self.convert_enums.rdstQ.element[quantity])>0:
                        quantity_value=np.around(quantity_value,int(-np.log10(quantity_error)))
                        quantity_error=np.around(quantity_error,int(-np.log10(quantity_error)+1))
                    else:
                        if quantity_value!=0:
                            quantity_value=np.around(quantity_value,int(max(2,-np.log10(abs(quantity_value))+2)))
                else:
                    quantity_error=None
                    if quantity_value!=0:
                        quantity_value=np.around(quantity_value,int(max(2,-np.log10(abs(quantity_value))+2)))
            else:
                quantity_value="Not Set"
                quantity_error=None

        reply = {
            "Id": station.GetId(),
            "Beta": beta,
            "BetaError": beta_error,
            "TimeResidual": time_residual,
            "TimeResidualError": time_residual_error,
            "RejectionStatus": rejection_reasons,
            "IsSaturated": station.IsSaturated(),
            "SignalPeak": signal_peak,
            "SignalWindowStart": np.around(signal_window_start, 2),
            "SignalWindowStop": np.around(signal_window_stop, 2),
            "NoiseRms": noise_rms,
            "NoiseRmsError": noise_rms_error,
            "QuantityValue": quantity_value,
            "QuantityError": quantity_error,
            "Quantity": quantity
        }
        fd, path=tempfile.mkstemp()
        os.write(fd,JSON.dumps([reply,tab]))
        os.close(fd)
        vispa.remote.send_topic(self._topic+".getRdStationInfo",window_id=self._window_id,data=path)
        return

    def get_rd_station_data_quantities(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_get_rd_station_data_quantities,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_get_rd_station_data_quantities(self,tab):
        data_quantities=[]
        for element in self.convert_enums.rdstQ.elements:
            data_quantities.append(str(element))
        fd, path=tempfile.mkstemp()
        os.write(fd,JSON.dumps([data_quantities,tab]))
        os.close(fd)
        vispa.remote.send_topic(self._topic+".getRdStationDataQuantities",window_id=self._window_id,data=path)
        return


    def get_rd_station_position(self, rd_id):
        station_position = self.geo.GetRdStationPosition(int(rd_id))
        return JSON.dumps([station_position[0] / 1000., station_position[1] / 1000.])

    def get_AERA_map(self, plot_width, plot_height, plot_signal=False, plot_timing=False, draw_sd=True,
                     draw_shower_axis=True,draw_ldf=False,vxb_vxvxb_axis=False, zoom_level=None):
        # read in data
        aeraGPSfileI = []
        aeraGPSfileII = []
        aeraGPSfileIII = []
        aeraGPSfileI_rejected = []
        aeraGPSfileII_rejected = []
        aeraGPSfileIII_rejected = []

        AERA_NL_I = []
        AERA_NL_II = []
        SdGPSfile = []
        aera_signal_stations = []
        aera_timing_stations = []
        rd_event = self.event.GetRdEvent()
        if rd_event is None:
            return None
        if len(rd_event.GetRdStationVector()) == 0:
            return None
        labels_I = []
        ids_I = []
        labels_I_rejected = []
        ids_I_rejected = []
        labels_II = []
        ids_II = []
        labels_II_rejected = []
        ids_II_rejected = []
        labels_III = []
        ids_III = []
        labels_III_rejected = []
        ids_III_rejected = []
        labels_NL_I = []
        ids_NL_I = []
        labels_NL_II = []
        ids_NL_II = []
        aera_phase3_stations = [86, 89, 90, 95, 96, 97, 104, 106, 114, 115, 127, 128, 129, 130, 138, 139, 140, 141, 147,
                                148, 149, 150, 105, 116, 117]
        AERA_NL_stations = [2, 5, 9, 4, 8, 12, 34, 35, 36, 43, 44, 49, 58, 59, 60, 69, 70, 83, 84, 85, 88, 92, 93, 94,
                            100,
                            101, 102, 103, 110, 111, 112, 113, 122, 123, 124, 125, 126, 135, 136, 137, 144, 145, 146,
                            151,
                            152, 157]
        for station in rd_event.GetRdStationVector():
            rd_id = station.GetId()
            station_position = self.geo.GetRdStationPosition(rd_id)
            station_label = "station " + str(rd_id)
            if station.IsRejected():
                station_label += " [Rejected]"
            if station.HasPulse():
                if plot_signal:
                    aera_signal_stations.append([station_position[0] / 1000., station_position[1] / 1000.,
                                                 station.GetParameter(self.convert_enums.rdstQ.eSignal)])
                if plot_timing and station.HasParameter(self.convert_enums.rdstQ.eSignalTime):
                    aera_timing_stations.append([station_position[0] / 1000., station_position[1] / 1000.,
                                                 station.GetParameter(self.convert_enums.rdstQ.eSignalTime)])
            if rd_id in range(125):
                if station.GetRejectionStatus()==0:
                    aeraGPSfileI.append([rd_id, station_position[0] / 1000., station_position[1] / 1000.])
                    labels_I.append(station_label)
                    ids_I.append(rd_id)
                else:
                    aeraGPSfileI_rejected.append([rd_id, station_position[0] / 1000., station_position[1] / 1000.])
                    labels_I_rejected.append(station_label)
                    ids_I_rejected.append(rd_id)
                if rd_id in AERA_NL_stations:
                    AERA_NL_I.append([rd_id, station_position[0] / 1000., station_position[1] / 1000.])
                    labels_NL_I.append(station_label)
                    ids_NL_I.append(rd_id)
            else:
                if rd_id in aera_phase3_stations:
                    if station.GetRejectionStatus()==0:
                        aeraGPSfileIII.append([rd_id, station_position[0] / 1000., station_position[1] / 1000.])
                        labels_III.append(station_label)
                        ids_III.append(rd_id)
                    else:
                        aeraGPSfileIII_rejected.append([rd_id, station_position[0] / 1000., station_position[1] / 1000.])
                        labels_III_rejected.append(station_label)
                        ids_III_rejected.append(rd_id)

                else:
                    if station.GetRejectionStatus()==0:
                        aeraGPSfileII.append([rd_id, station_position[0] / 1000., station_position[1] / 1000.])
                        labels_II.append(station_label)
                        ids_II.append(rd_id)
                    else:
                        aeraGPSfileII_rejected.append([rd_id, station_position[0] / 1000., station_position[1] / 1000.])
                        labels_II_rejected.append(station_label)
                        ids_II_rejected.append(rd_id)
                    if rd_id in AERA_NL_stations:
                        AERA_NL_II.append([rd_id, station_position[0] / 1000., station_position[1] / 1000.])
                        labels_NL_II.append(station_label)
                        ids_NL_II.append(rd_id)

        if draw_sd:
            sd_x_positions = self.geo.GetStationXPositions()
            sd_y_positions = self.geo.GetStationYPositions()
            for i in range(len(sd_x_positions)):
                SdGPSfile.append([sd_x_positions[i] / 1000., sd_y_positions[i] / 1000.])

        AERA_NL_I = np.array(AERA_NL_I)
        AERA_NL_II = np.array(AERA_NL_II)

        # global plot settings
        labelAERAI = 'AERA24 LPDA, ext. trig.'  # 'AERA phase I'
        labelAERAINL = 'AERA24 LPDA, int. trig.'  # 'AERA phase I'
        labelAERAIIext = 'AERA124 butterfly, ext. trig.'  # 'AERA phase II'
        labelAERAIIint = 'AERA124 butterfly, int. trig.'  # 'AERA phase II'
        labelAERAIII = 'AERA153 butterfly, ext. trig.'
        # labelCRS = 'CRS'
        # labelBeacon = 'beacon'  # / CO'
        labelSd = 'SD station'  # 'Sd'
        labelFd = 'FD site'  # 'Sd'
        # labelAMIGA = r'$\muon$-counter'
        colorAERAI = 'r'  # '#21CC37'#'r' #'#33a02c'
        colorAERAII = '#5A10FF'  # '#21CC37' #'#1f78b4'
        colorAERAIII = '#5A10FF'  # '#b2df8a'
        # colorCRS = 'yellow'  # ungefuelltes Kaestchen
        # colorBeacon = 'g'  # ungefuelltes Kaestchen
        colorSd = '0.7'  # '#FF9A32' #hellgrau
        colorFd = 'lime'  # '#FF9A32' #hellgrau
        # colorAMIGA = 'b'

        # plot map

        # plot FD and Heat FOV
        plot_x = float(plot_width) / 80.
        plot_y = float(plot_height) / 80.
        fig = plt.figure(figsize=(plot_x, plot_y), dpi=80)
        ax = fig.add_subplot(111)

        shower_core = rd_event.GetRdRecShower().GetCoreSiteCS()
        if not (shower_core is None or np.isnan(shower_core[0]) or np.isnan(shower_core[1])):
            ax.set_xlim(shower_core[0] / 1000. - 1, shower_core[0] / 1000. + 1)
            ax.set_ylim(shower_core[1] / 1000. - 1, shower_core[1] / 1000. + 1)
            # draw shower axis
            if draw_shower_axis:
                az = rd_event.GetRdRecShower().GetAzimuth()
                shower_axis_x = [shower_core[0] / 1000., shower_core[0] / 1000. + 10 * math.cos(az)]
                shower_axis_y = [shower_core[1] / 1000., shower_core[1] / 1000. + 10 * math.sin(az)]
                ax.plot(shower_axis_x, shower_axis_y, "k-", linewidth=3)


        legend_list = []
        SdGPSfile = np.array(SdGPSfile)
        aeraGPSfileI = np.array(aeraGPSfileI)
        aeraGPSfileI_rejected = np.array(aeraGPSfileI_rejected)
        aeraGPSfileII = np.array(aeraGPSfileII)
        aeraGPSfileIII = np.array(aeraGPSfileIII)
        aeraGPSfileII_rejected = np.array(aeraGPSfileII_rejected)
        aeraGPSfileIII_rejected = np.array(aeraGPSfileIII_rejected)
        aera_signal_stations = np.array(aera_signal_stations)
        aera_timing_stations = np.array(aera_timing_stations)

        if SdGPSfile.size > 0:
            SD, = ax.plot(SdGPSfile.T[0], SdGPSfile.T[1], 'o', color=colorSd, markersize=7, markeredgecolor='k',
                          label=labelSd, zorder=20)
            legend_list.append(SD)
        if aeraGPSfileI.size > 0:
            AERAI = ax.scatter(aeraGPSfileI.T[1], aeraGPSfileI.T[2], marker='$-|-$', color="k", s=100,
                               label=labelAERAI,
                               zorder=19)
            legend_list.append(AERAI)
            mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(AERAI, labels_I))
            mpld3.plugins.connect(fig, mpld3_plugins.identify_stations(AERAI, ids_I))
        if aeraGPSfileI_rejected.size > 0:
            AERAI_rejected = ax.scatter(aeraGPSfileI_rejected.T[1], aeraGPSfileI_rejected.T[2], marker='$-|-$', color="r", s=100,
                               label=labelAERAI,
                               zorder=19)
            legend_list.append(AERAI_rejected)
            mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(AERAI_rejected, labels_I_rejected))
            mpld3.plugins.connect(fig, mpld3_plugins.identify_stations(AERAI_rejected, ids_I_rejected))
        if AERA_NL_I.size > 0:
            AERAINL = ax.scatter(AERA_NL_I.T[1], AERA_NL_I.T[2], marker='^', color='w', s=50, label=labelAERAINL,
                                 zorder=18)
            legend_list.append(AERAINL)
            mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(AERAINL, labels=labels_NL_I))
            mpld3.plugins.connect(fig, mpld3_plugins.identify_stations(AERAINL, ids_NL_I))
        if aeraGPSfileII.size > 0:
            AERAII = ax.scatter(aeraGPSfileII.T[1], aeraGPSfileII.T[2], marker='$><$', c="k", s=100,
                                label=labelAERAIIext,
                                zorder=17)
            legend_list.append(AERAII)
            mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(AERAII, labels=labels_II))
            mpld3.plugins.connect(fig, mpld3_plugins.identify_stations(AERAII, ids_II))
        if aeraGPSfileII_rejected.size > 0:
            AERAII_rejected = ax.scatter(aeraGPSfileII_rejected.T[1], aeraGPSfileII_rejected.T[2], marker='$><$', color="r", s=100,
                                label=labelAERAIIext,
                                zorder=17)
            mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(AERAII_rejected, labels=labels_II_rejected))
            mpld3.plugins.connect(fig, mpld3_plugins.identify_stations(AERAII_rejected, ids_II_rejected))
        if AERA_NL_II.size > 0:
            AERAIINL = ax.scatter(AERA_NL_II.T[1], AERA_NL_II.T[2], marker='v', color='w', s=50, label=labelAERAIIint,
                                  zorder=16)
            legend_list.append(AERAIINL)
            mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(AERAIINL, labels=labels_NL_II))
            mpld3.plugins.connect(fig, mpld3_plugins.identify_stations(AERAIINL, ids_NL_II))
        if aeraGPSfileIII.size > 0:
            AERAIII = ax.scatter(aeraGPSfileIII.T[1], aeraGPSfileIII.T[2], marker='$><$', color="k", s=100,
                                 label=labelAERAIII, zorder=15)
            mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(AERAIII, labels_III))
            mpld3.plugins.connect(fig, mpld3_plugins.identify_stations(AERAIII, ids_III))
        if aeraGPSfileIII_rejected.size > 0:
            AERAIII_rejected = ax.scatter(aeraGPSfileIII_rejected.T[1], aeraGPSfileIII_rejected.T[2], marker='$><$', color="r", s=100,
                                 label=labelAERAIII, zorder=15)
            mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(AERAIII_rejected, labels_III_rejected))
            mpld3.plugins.connect(fig, mpld3_plugins.identify_stations(AERAIII_rejected, ids_III_rejected))
        if aera_signal_stations.size > 0:
            ax.scatter(aera_signal_stations.T[0], aera_signal_stations.T[1], c=aera_signal_stations.T[2],
                       s=200, cmap=plt.cm.gnuplot2_r, marker="o", zorder=1)
        if aera_timing_stations.size > 0:
            ax.scatter(aera_timing_stations.T[0], aera_timing_stations.T[1], c=aera_timing_stations.T[2],
                       s=200, cmap=plt.cm.gnuplot2_r, marker="o", zorder=2)
        # draw FD and HEAT

        ax.plot(-31.906, 14.981, 'o', color=colorFd, markersize=12, markeredgecolor='k', label=labelFd, alpha=0.5,
                markeredgewidth=2)
        wedgeFD = mpatches.Wedge((-31.906, 14.981), 1, 240, 60, ec="none", facecolor="lime", alpha=0.3, label="FD FOV")
        ax.add_patch(wedgeFD)
        for ang in [-120, -90, -60, -30, 0, 30, 60]:
            ax.plot((-31.906, -31.906 + np.cos(np.deg2rad(ang))), (14.981, 14.981 + np.sin(np.deg2rad(ang))), "k")
        ax.plot((-31.906, -31.906 + np.cos(np.deg2rad(15)) * .7), (14.981, 14.981 + np.sin(np.deg2rad(15)) * .7), "k",
                linewidth=0.5)
        ax.plot((-31.906, -31.906 + np.cos(np.deg2rad(-15)) * .7), (14.981, 14.981 + np.sin(np.deg2rad(-15)) * .7), "k",
                linewidth=0.5)
        HEAT1 = mpatches.Wedge((-31.906, 14.981), .7, -15, 15, ec="none", facecolor='b', alpha=0.3, label="HEAT FOV")
        HEAT2 = mpatches.Wedge((-31.906, 14.981), .7, -60, -30, ec="none", facecolor='b', alpha=0.3)
        HEAT3 = mpatches.Wedge((-31.906, 14.981), .7, 30, 60, ec="none", facecolor='b', alpha=0.3)
        ax.add_patch(HEAT1)
        ax.add_patch(HEAT2)
        ax.add_patch(HEAT3)
        ax.set_aspect("equal", adjustable="box")
        x_range=2
        y_range=2
        core_x=shower_core[0]/1000.
        core_y=shower_core[1]/1000.
        X = np.arange(core_x-x_range,core_x+ x_range, .005)
        Y = np.arange(core_y-y_range, core_y+y_range, .005)
        X, Y = np.meshgrid(X, Y)
        rd_shower=rd_event.GetRdRecShower()
        sd_shower=self.event.GetSDEvent().GetSdRecShower()
        fit_result = rd_shower.Get2dLDFFitResult()
        if draw_ldf or vxb_vxvxb_axis:
            from radiotools import CSTransformation
            cs = CSTransformation.CSTransformation(sd_shower.GetZenith(), sd_shower.GetAzimuth(), np.array([0,0,0]))
            e_vxB=cs.transform_from_vxB_vxvxB(np.array([1,0,0]))
            e_vxvxB=cs.transform_from_vxB_vxvxB(np.array([0,1,0]))

        if len(np.array(fit_result.Parameters())) > 0 and draw_ldf:
            p = np.array(fit_result.Parameters())
            z_plot = 1.e-6*p[0] * np.exp(-((((X-core_x)*e_vxB[0]+(Y-core_y)*e_vxB[1])*1000. - (p[1] + p[5])) ** 2 + (((Y-core_y)*e_vxvxB[1]+(X-core_x)*e_vxvxB[0])*1000. - p[2]) ** 2) / p[3] ** 2) -1.e-6* p[6] * p[0] * np.exp(
                -((((X-core_x)*e_vxB[0]+(Y-core_y)*e_vxB[1])*1000. - (p[1] + p[4])) ** 2 + (((Y-core_y)*e_vxvxB[1]+(X-core_x)*e_vxvxB[0])*1000. - p[2]) ** 2) / np.exp(p[7] + p[8] * p[3]) ** 2)
            minimum = z_plot.min()
            maximum=z_plot.max()
            contour = ax.imshow(z_plot, origin='upper', vmin=minimum, vmax=maximum,
                                extent=[core_x-x_range, core_x+x_range, core_y-y_range, core_y+y_range], cmap=plt.cm.gnuplot2_r, aspect="equal",
                                zorder=-2)

        if vxb_vxvxb_axis:
            ax.plot([core_x,core_x+.5*e_vxB[0]],[core_y,core_y+.5*e_vxB[1]],color="green")
            ax.plot([core_x,core_x+.5*e_vxvxB[0]],[core_y,core_y+.5*e_vxvxB[1]],color="green")
            ax.annotate("vxB",xy=(core_x+.5*e_vxB[0],core_y+.5*e_vxB[1]),color="green")
            ax.annotate("vxvxB",xy=(core_x+.5*e_vxvxB[0],core_y+.5*e_vxvxB[1]),color="green")
        # matplotlib markers to choose
        markers = []
        for m in Line2D.markers:
            try:
                if len(m) == 1 and m != ' ':
                    markers.append(m)
            except TypeError:
                pass
        if zoom_level is not None:
            ax.set_xlim([zoom_level[0], zoom_level[1]])
            ax.set_ylim([zoom_level[2], zoom_level[3]])
        ax.set_xlabel("x [km]")
        ax.set_ylabel("y [km]")
        return fig

    def get_rd_ldf(self, plot_width, plot_height, zoom_level=None, two_d=True, one_d=False, polarization=False):
        if two_d:
            return self.get_RD_2DLDF(plot_width, plot_height, zoom_level, polarization)
        if one_d:
            return self.get_RD_1DLDF(plot_width, plot_height, zoom_level)

    # noinspection PyUnresolvedReferences
    def get_RD_2DLDF(self, plot_width, plot_height, zoom_level=None, polarization=False):
        rdevent = self.event.GetRdEvent()
        rdshower = rdevent.GetRdRecShower()
        if rdshower is None:
            return None
        # get fit result
        fit_result = rdshower.Get2dLDFFitResult()
        if len(np.array(fit_result.Parameters())) == 0:
            logger.info("no 2D LDF available in ADST file")
        rdstations = rdevent.GetRdStationVector()
        if len(rdstations) == 0:
            return None
        positions_vxB = []
        positions_vxvxB = []
        energy_densities = []
        energy_density_errors = []
        energy_density_noise = []
        signal_mask = []
        rejected_mask = []
        saturated_mask = []
        station_ids = []
        pol_arrows = []
        from radiotools import CSTransformation

        sd_shower = self.event.GetSDEvent().GetSdRecShower()
        e_field_cs = CSTransformation.CSTransformation(sd_shower.GetZenith(), sd_shower.GetAzimuth())
        cs = CSTransformation.CSTransformation(sd_shower.GetZenith(), sd_shower.GetAzimuth(), sd_shower.GetCoreSiteCS())

        for station in rdstations:
            station_position = self.geo.GetRdStationPosition(station.GetId())
            station_vxB_vxvxB = cs.transform_to_vxB_vxvxB(station_position)
            if station.HasParameter(self.convert_enums.rdstQ.eLDFFitStationPositionVxB):
                position_vxB = station.GetParameter(self.convert_enums.rdstQ.eLDFFitStationPositionVxB)
            else:
                position_vxB = station_vxB_vxvxB[0]
            positions_vxB.append(position_vxB)
            if station.HasParameter(self.convert_enums.rdstQ.eLDFFitStationPositionVxB):
                position_vxvxB = station.GetParameter(self.convert_enums.rdstQ.eLDFFitStationPositionVxVxB)
            else:
                position_vxvxB = station_vxB_vxvxB[1]
            positions_vxvxB.append(position_vxvxB)
            station_id = station.GetId()
            station_ids.append(station_id)
            saturated_mask.append(station.IsSaturated())
            if station.HasParameter(80):
                energy_densities.append(station.GetParameter(80))
            if station.HasParameterError(80):
                energy_density_errors.append(
                    station.GetParameterError(80))
            else:
                energy_densities.append(0)
                energy_density_errors.append(0)
            if station.HasParameter(80):
                energy_density_noise.append(station.GetParameter(80))
            else:
                energy_density_noise.append(0)
            signal_mask.append(station.HasPulse())
            rejected_mask.append(station.GetRejectionStatus() > 0)
            if station.HasPulse():
                if polarization and station.HasParameter(
                        self.convert_enums.rdstQ.eEFieldVectorEW) and station.HasParameter(
                    self.convert_enums.rdstQ.eEFieldVectorNS) and station.HasParameter(
                    self.convert_enums.rdstQ.eEFieldVectorV):
                    e_field = np.array([station.GetParameter(self.convert_enums.rdstQ.eEFieldVectorEW),
                                        station.GetParameter(self.convert_enums.rdstQ.eEFieldVectorNS),
                                        station.GetParameter(self.convert_enums.rdstQ.eEFieldVectorV)])
                    e_field_vxB = e_field_cs.transform_to_vxB_vxvxB(e_field)
                    if e_field_vxB[0] > 0:
                        e_field_vxB = -e_field_vxB
                    arrow_length = 1.e2 / np.sqrt(e_field_vxB[0] ** 2 + e_field_vxB[1] ** 2)
                    pol_arrows.append(
                        [position_vxB, position_vxvxB, e_field_vxB[0] * arrow_length, e_field_vxB[1] * arrow_length])
        positions_vxB = np.array(positions_vxB)
        positions_vxvxB = np.array(positions_vxvxB)
        signal_mask = np.array(signal_mask, dtype=np.bool)
        rejected_mask = np.array(rejected_mask, dtype=np.bool)
        energy_densities = np.array(energy_densities)

        # def func_2dLDF(x, y, p):
        #     return p[0] * np.exp(-((x - (p[1] + p[5])) ** 2 + (y - p[2]) ** 2) / p[3] ** 2) - p[6] * p[0] * np.exp(
        #         -((x - (p[1] + p[4]) ** 2 + (y - p[2]) ** 2) / np.exp(p[7] + p[8] * p[3]) ** 2))

        #         TF2 *LDF2DFit = new TF2("LDF2DFit",
        #           "[0]*TMath::Exp(-((x-([1]+[5]))**2+(y-[2])**2)/[3]**2)-[6]*[0]*TMath::Exp(-((x-([1]+[4]))**2+(y-[2])**2)/TMath::Exp([7]+[8]*[3])**2)",
        #           -1000, 1000,-1000,1000);
        #         for(int j=0;j<LDF2DFit->GetNumberFreeParameters();j++){
        #             LDF2DFit->SetParameter(j,r.Parameter(j));
        #         }
        plot_x = float(plot_width) / 80.
        plot_y = float(plot_height) / 80.
        fig, ax = plt.subplots(1, 1, figsize=(plot_x, plot_y), dpi=80)
        x_range = 5000
        y_range = 5000
        markersize = 150 * 2
        X = np.arange(-x_range, x_range, 5)
        Y = np.arange(-y_range, y_range, 5)
        X, Y = np.meshgrid(X, Y)
        if len(np.array(fit_result.Parameters())) > 0:
            p = np.array(fit_result.Parameters())
            #         z_plot = func_2dLDF(X, Y, params)
            z_plot = p[0] * np.exp(-((X - (p[1] + p[5])) ** 2 + (Y - p[2]) ** 2) / p[3] ** 2) - p[6] * p[0] * np.exp(
                -((X - (p[1] + p[4])) ** 2 + (Y - p[2]) ** 2) / np.exp(p[7] + p[8] * p[3]) ** 2)
            minimum = z_plot.min()
            maximum = max(z_plot.max(), max(energy_densities))
            contour = ax.imshow(z_plot, origin='upper', vmin=minimum, vmax=maximum,
                                extent=[-x_range, x_range, -y_range, y_range], cmap=plt.cm.gnuplot2_r, aspect="equal",
                                zorder=-2)
        else:
            if len(energy_densities) > 0:
                minimum = 0
                maximum = max(energy_densities)
            else:
                return None

        ax.scatter(positions_vxB[signal_mask | rejected_mask],
                   positions_vxvxB[signal_mask | rejected_mask], c=energy_densities[signal_mask | rejected_mask],
                   marker='o', cmap=plt.cm.gnuplot2_r, linewidth=2, s=markersize, vmin=minimum, vmax=maximum,
                   label='data', edgecolors=[".4", ".4"])
        if np.sum(~(signal_mask | rejected_mask)) > 0:
            ax.scatter(positions_vxB[~(signal_mask | rejected_mask)],
                       positions_vxvxB[~(signal_mask | rejected_mask)], color='0.8', edgecolors=".4", linewidth=2,
                       vmin=minimum, vmax=maximum, s=markersize, marker='s', label='silent')
        if np.sum(rejected_mask) > 0:
            ax.scatter(positions_vxB[rejected_mask], positions_vxvxB[rejected_mask], marker='x',
                       linewidth=2, vmin=minimum, vmax=maximum, s=markersize, color='r', label='flagged')
        ax.plot([0, 0], [-y_range, y_range], '--', color='0.7', zorder=-1)
        ax.plot([-x_range, x_range], [0, 0], '--', color='0.7', zorder=-1)
        """
        if len(np.array(fit_result.Parameters())) > 0:
            # noinspection PyUnboundLocalVariable
            cbar = ax.figure.colorbar(contour, ax=ax, orientation="vertical")  # , format='%.2g'
            cbar.set_label("eV/m^2")
        """
        ax.set_xlabel('position in v x B [m]')
        ax.set_ylabel('position in v x (v x B) [m]')
        ax.set_xlim(-x_range, x_range)
        ax.set_ylim(-y_range, y_range)
        ax.grid(b=False)
        station_identifier = plt.scatter(positions_vxB, positions_vxvxB, s=100, alpha=0)
        mpld3.plugins.connect(fig, mpld3_plugins.identify_stations(station_identifier, station_ids))
        mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(station_identifier, station_ids))
        if (rdshower.HasParameter(self.convert_enums.rdshQ.eCoreX) and rdshower.HasParameter(
                self.convert_enums.rdshQ.eCoreY) and rdshower.HasParameter(self.convert_enums.rdshQ.eCoreZ)):
            core_position_vxB_vxvxB = cs.transform_to_vxB_vxvxB(
                np.array([rdshower.GetParameter(self.convert_enums.rdshQ.eCoreX),
                          rdshower.GetParameter(self.convert_enums.rdshQ.eCoreY),
                          rdshower.GetParameter(self.convert_enums.rdshQ.eCoreZ)]))
            ax.plot([core_position_vxB_vxvxB[0]], [core_position_vxB_vxvxB[1]], marker="*", mfc="k", mec="w", mew=.5,
                    ms=20)
        else:
            ax.plot([0], [0], marker="*", mfc="k", mec="w", mew=.5, ms=20)

        if zoom_level is not None:
            plt.xlim([zoom_level[0], zoom_level[1]])
            plt.ylim([zoom_level[2], zoom_level[3]])
        if len(pol_arrows) > 0:
            import matplotlib.patches as patches

            for arrow in pol_arrows:
                plt.gca().add_patch(
                    patches.FancyArrow(arrow[0], arrow[1], arrow[2], arrow[3], fc="w", ec="k", head_width=15, width=3,
                                       zorder=1111))
        return fig

    def get_RD_1DLDF(self, plot_width, plot_height, zoom_level=None):
        plot_x = float(plot_width) / 80.
        plot_y = float(plot_height) / 80.
        fig = plt.figure(figsize=(plot_x, plot_y), dpi=80)
        rd_stations = self.event.GetRdEvent().GetRdStationVector()
        if len(rd_stations) == 0:
            return None
        signals = []
        signal_errors = []
        radius_array = []
        station_id = []
        for station in rd_stations:
            if (station.HasPulse() and station.HasParameter(
                    self.convert_enums.rdstQ.eSignal) and station.HasParameterError(self.convert_enums.rdstQ.eSignal)):
                axis_site = self.event.GetRdEvent().GetRdRecShower().GetAxisSiteCS()
                core_site = self.event.GetRdEvent().GetRdRecShower().GetCoreSiteCS()
                radius = self.geo.GetStationAxisDistance(self.geo.GetRdStationPosition(station.GetId()), axis_site,
                                                         core_site)
                if not math.isnan(radius):
                    signals.append(station.GetParameter(self.convert_enums.rdstQ.eSignal) * 1e6)
                    signal_errors.append(station.GetParameterError(self.convert_enums.rdstQ.eSignal) * 1e6)
                    radius_array.append(radius)
                    station_id.append(station.GetId())
        if len(radius_array) == 0:
            return None
        plt.errorbar(radius_array, signals, yerr=signal_errors, fmt="o", color="k", markersize=4, zorder=1)
        station_identifier = plt.scatter(radius_array, signals, s=50, alpha=0, zorder=2)
        mpld3.plugins.connect(fig, mpld3_plugins.identify_stations(station_identifier, station_id))
        plt.xlabel("distance to shower core [m]")
        plt.ylabel("Signal [microV/m]")

        if zoom_level is not None:
            plt.xlim([zoom_level[0], zoom_level[1]])
            plt.ylim([zoom_level[2], zoom_level[3]])

        return fig

    def get_RD_station_trace(self,station_id,time_range):
        try:
            # noinspection PyUnresolvedReferences
            import convertEnums
        except ImportError as e:
            return JSON.dumps({"success": False, 'reason': 2, "info": "Could not import converEnums.py, %s" % e})
        self.convert_enums = convertEnums
        if self.event.GetRdEvent() is None:
            return None
        if len(self.event.GetRdEvent().GetRdStationVector()) == 0:
            return None
        if int(station_id) == 0:
            station_id = self.event.GetRdEvent().GetRdStationVector()[0].GetId()
        station = self.event.GetRdEvent().GetRdStationById(int(station_id))
        labels = ["East-West", "North-South", "Vertical","", "vxB", "vxvxB", "v", "polarization", "absolute"]
        time_binning = station.GetRdTrace(0).GetSamplingRate()
        if station.HasParameter(self.convert_enums.rdstQ.eSignalTime) and station.HasParameter(
                self.convert_enums.rdstQ.eTraceStartTime):
            signal_time = station.GetParameter(self.convert_enums.rdstQ.eSignalTime) - station.GetParameter(
                self.convert_enums.rdstQ.eTraceStartTime)
        else:
            signal_time=0
        rd_traces=[]
        for trace_number in range(9):
            trace = []
            if trace_number in range(0, 3):
                try:
                    trace = station.GetRdTimeTrace(int(trace_number))
                    time_binning = station.GetRdTrace(int(trace_number)).GetSamplingRate()
                # TODO: Catch a useful exception and return exemplary a warning
                except:
                    pass
            if trace_number in range(4, 7):
                try:
                    trace = station.GetRdTimeTraceShowerPlane(int(trace_number - 4))
                    time_binning = station.GetRdTraceShowerPlane(int(trace_number - 4)).GetSamplingRate()
                # TODO: Catch a useful exception and return exemplary a warning
                except:
                    time_binning = station.GetRdTrace(0).GetSamplingRate()
                    sd_shower = self.event.GetSDEvent().GetSdRecShower()
                    from radiotools import CSTransformation

                    e_field_cs = CSTransformation.CSTransformation(sd_shower.GetZenith(), sd_shower.GetAzimuth())
                    vxB_vxvxB_trace = []
                    trace_ew = station.GetRdTimeTrace(0)
                    trace_ns = station.GetRdTimeTrace(1)
                    trace_v = station.GetRdTimeTrace(2)
                    for k in range(len(station.GetRdTimeTrace(0))):
                        vxB_vxvxB_trace.append(
                            e_field_cs.transform_to_vxB_vxvxB(np.array([trace_ew[k], trace_ns[k], trace_v[k]]))[
                                int(trace_number - 4)])
                    trace = vxB_vxvxB_trace
            if trace_number == 7 and station.HasParameter(convertEnums.rdstQ.eEFieldVectorEW) and station.HasParameter(
                    convertEnums.rdstQ.eEFieldVectorNS) and station.HasParameter(convertEnums.rdstQ.eEFieldVectorV):
                sd_shower = self.event.GetRdEvent().GetRdRecShower()
                from radiotools import CSTransformation

                e_field_cs = CSTransformation.CSTransformation(sd_shower.GetZenith(), sd_shower.GetAzimuth())
                try:
                    trace_vxB = station.GetRdTimeTraceShowerPlane(0)
                    trace_vxB_vxvxB = station.GetRdTimeTraceShowerPlane(1)
                    trace_v = station.GetRdTimeTraceShowerPlane(2)
                    time_binning = station.GetRdTraceShowerPlane(1).GetSamplingRate()
                except:
                    trace_vxB = []
                    trace_vxB_vxvxB = []
                    trace_ew = station.GetRdTimeTrace(0)
                    trace_ns = station.GetRdTimeTrace(1)
                    trace_v = station.GetRdTimeTrace(2)
                    time_binning = station.GetRdTrace(1).GetSamplingRate()
                    for l in range(len(trace_ew)):
                        vxB_vxvxB_trace_point = e_field_cs.transform_to_vxB_vxvxB(
                            np.array([trace_ew[l], trace_ns[l], trace_v[l]]))
                        trace_vxB.append(vxB_vxvxB_trace_point[0])
                        trace_vxB_vxvxB.append(vxB_vxvxB_trace_point[1])
                e_field = np.array([station.GetParameter(convertEnums.rdstQ.eEFieldVectorEW),
                                    station.GetParameter(convertEnums.rdstQ.eEFieldVectorNS),
                                    station.GetParameter(convertEnums.rdstQ.eEFieldVectorV)])
                e_field_vxB_vxvxB = e_field_cs.transform_to_vxB_vxvxB(e_field)
                e_field_vxB_vxvxB_abs = math.sqrt(
                    e_field_vxB_vxvxB[0] ** 2 + e_field_vxB_vxvxB[1] ** 2 + e_field_vxB_vxvxB[2] ** 2)
                for k in range(len(trace_vxB_vxvxB)):
                    trace.append(
                        (trace_vxB[k] * e_field_vxB_vxvxB[0] + trace_vxB_vxvxB[k] * e_field_vxB_vxvxB[
                            1]) / e_field_vxB_vxvxB_abs)
            if trace_number == 8:
                trace_ew = station.GetRdTimeTrace(0)
                trace_ns = station.GetRdTimeTrace(1)
                trace_v = station.GetRdTimeTrace(2)
                for k in range(len(trace_ew)):
                    trace.append(np.sqrt(trace_ew[k] ** 2 + trace_ns[k] ** 2 + trace_v[k] ** 2))
            time_array = []
            cut_trace = []
            for i in range(max(0, int((signal_time + time_range[0]) / time_binning)),
                           min(len(trace), int((signal_time + time_range[1]) / time_binning))):
                cut_trace.append(trace[i])
            rd_traces.append(cut_trace)
        for i in range(max(0, int((signal_time + time_range[0]) / time_binning)),
                       min(len(trace), int((signal_time + time_range[1]) / time_binning))):
            time_array.append(i * time_binning)

        if station.HasParameter(self.convert_enums.rdstQ.eSignalSearchWindowStart) and station.HasParameter(self.convert_enums.rdstQ.eSignalSearchWindowStop):
            signal_window_min = station.GetParameter(self.convert_enums.rdstQ.eSignalSearchWindowStart)
            signal_window_max = station.GetParameter(self.convert_enums.rdstQ.eSignalSearchWindowStop)
            #if signal_window_min>time_range[0] and signal_window_max<time_range[1]:
            signal_window=[signal_window_min,signal_window_max]
            #else:
            #    signal_window=[None,None]
        else:
            signal_window=[None,None]
        if station.GetParameter(self.convert_enums.rdstQ.eNoiseWindowStop)>0 and station.GetParameter(self.convert_enums.rdstQ.eNoiseWindowStart)<plt.gca().get_xlim()[1]:
            noise_window_min=max(station.GetParameter(self.convert_enums.rdstQ.eNoiseWindowStart),0)
            noise_window_max=station.GetParameter(self.convert_enums.rdstQ.eNoiseWindowStop)
            #if noise_window_min>time_range[0] and noise_window_max<time_range[1]:
            noise_window=[noise_window_min,noise_window_max]
            #else:
            #    noise_window=[None,None]
        else:
            noise_window=[None,None]

        reply = {
            "signal_traces": rd_traces,
            "time_trace": time_array,
            "labels": labels,
            "signal_window": signal_window,
            "noise_window": noise_window
        }
        return reply


    def get_RD_channel_trace(self, station_id, draw_1, draw_2, plot_width, plot_height, x_range, zoom_level=None):
        try:
            # noinspection PyUnresolvedReferences
            import convertEnums
        except ImportError as e:
            return JSON.dumps({"success": False, 'reason': 2, "info": "Could not import converEnums.py, %s" % e})
        self.convert_enums = convertEnums
        if self.event.GetRdEvent() is None:
            return None
        if len(self.event.GetRdEvent().GetRdStationVector()) == 0:
            return None
        if int(station_id) == 0:
            station_id = self.event.GetRdEvent().GetRdStationVector()[0].GetId()
        plot_x = float(plot_width) / 80.
        plot_y = float(plot_height) / 80.
        fig = plt.figure(figsize=(plot_x, plot_y), dpi=80)
        fig.clear()
        ax = plt.subplot(111)
        station = self.event.GetRdEvent().GetRdStationById(int(station_id))
        labels = ["Ch-1", "Ch-2", "Ch-3"]
        trace_numbers = []
        if draw_1:
            trace_numbers.append(0)
        if draw_2:
            trace_numbers.append(1)
        x_max = min(x_range[1],
                    len(station.GetRdChannelVector()[0].GetRdTrace().GetTimeTrace()) * station.GetRdChannelVector()[
                        0].GetRdTrace().GetSamplingRate())
        time_binning = 0
        trace_drawn = False
        for trace_number in trace_numbers:
            try:
                channel = station.GetRdChannelVector()[int(trace_number)]
                trace = np.array(channel.GetRdTrace().GetTimeTrace())
                new_trace = []
                time_binning = channel.GetRdTrace().GetSamplingRate()
                # TODO: Catch a useful exception and return exemplary a warning
                time_array = []
                for i in range(int(x_range[0] / time_binning), int(x_max / time_binning)):
                    new_trace.append(trace[i])
                    time_array.append(i * time_binning)
                plt.xlabel("t [ns]")
                plt.ylabel("U [V]")
                ax.plot(time_array, new_trace, label=labels[trace_number])
                trace_drawn = True
            except:
                pass
        if trace_drawn:
            plt.legend().set_title("")
        plt.grid(True)
        return fig

    def get_rd_channel_length(self, station_id):
        if self.event.GetRdEvent() is None:
            return 0
        if len(self.event.GetRdEvent().GetRdStationVector()) == 0:
            return 0
        if int(station_id) == 0 or int(station_id) == -1 or station_id is None:
            station_id = self.event.GetRdEvent().GetRdStationVector()[0].GetId()
        station = self.event.GetRdEvent().GetRdStationById(int(station_id))
        channel = station.GetRdChannelVector()[0]
        return len(channel.GetRdTrace().GetTimeTrace()) * channel.GetRdTrace().GetSamplingRate()

    def get_RD_station_spectrum(self, station_id, draw_ew, draw_ns, draw_vert, plot_width, plot_height,
                                zoom_level=None):
        if int(station_id) == 0:
            station_id = self.event.GetRdEvent().GetRdStationVector()[0].GetId()
        plot_x = float(plot_width) / 80.
        plot_y = float(plot_height) / 80.
        RD_station_spec_plot = plt.figure(figsize=(plot_x, plot_y), dpi=80)
        RD_station_spec_plot.clear()
        station = self.event.GetRdEvent().GetRdStationById(int(station_id))
        labels = ["East-West", "North-South", "Vertical"]
        trace_numbers = []
        if draw_ew:
            trace_numbers.append(0)
        if draw_ns:
            trace_numbers.append(1)
        if draw_vert:
            trace_numbers.append(2)
        for trace_number in trace_numbers:
            spectrum = []
            try:
                trace = station.GetRdTrace(trace_number)
                spectrum_0 = trace.GetAbsoluteFreqSpectrum()
                f_min = min(trace.GetMinFreq(), trace.GetMaxFreq())
                f_max = max(trace.GetMinFreq(), trace.GetMaxFreq())
                f_bin = (f_max - f_min) / (int(spectrum_0.size()) - 1)
                f_array = []
                i = 0
                max_entry = 0
                for spectrum_entry in spectrum_0:
                    f_array.append(f_min + i * f_bin - .5 * f_bin)
                    f_array.append(f_min + i * f_bin + .5 * f_bin)
                    spectrum.append(spectrum_entry)
                    spectrum.append(spectrum_entry)
                    if spectrum_entry > 0:
                        max_entry = f_min + i * f_bin
                    i += 1
                plt.plot(f_array, spectrum, label=labels[trace_number])
                plt.xlim([0, max_entry + 10])
            # TODO: Catch a useful exception and return exemplary a warning
            except:
                pass
        plt.legend().set_title("")
        plt.xlabel("f [MHz]")
        plt.ylabel("Signal [microV/m/MHz]")

        if zoom_level is not None:
            plt.xlim([zoom_level[0], zoom_level[1]])
            plt.ylim([zoom_level[2], zoom_level[3]])
        else:
            plt.xlim([30, 90])

        return RD_station_spec_plot

    def get_RD_time_residuals(self, plot_width, plot_height, rd_axis=True, sd_axis=False,mc_axis=False, zoom_level=None):
        residual_array = []
        residual_error_array = []
        radius_array = []
        station_ids = []
        plot_x = plot_width / 80.
        plot_y = plot_height / 80.
        Rd_station_time_residual_plot = plt.figure(figsize=(plot_x, plot_y), dpi=80)
        Rd_station_time_residual_plot.clear()
        for station in self.event.GetRdEvent().GetRdStationVector():
            if (station.HasParameter(self.convert_enums.rdstQ.eTimeResidual) and station.HasParameterError(
                    self.convert_enums.rdstQ.eTimeResidual) and station.HasPulse()):
                if rd_axis:
                    axis_site = self.event.GetRdEvent().GetRdRecShower().GetAxisSiteCS()
                    core_site = self.event.GetRdEvent().GetRdRecShower().GetCoreSiteCS()
                    plt.xlabel("radius to radio shower axis [m]")
                elif sd_axis :
                    axis_site = self.event.GetSDEvent().GetSdRecShower().GetAxisSiteCS()
                    core_site = self.event.GetSDEvent().GetSdRecShower().GetCoreSiteCS()
                    plt.xlabel("radius to SD shower axis [m]")
                elif mc_axis :
                    axis_site = self.event.GetGenShower().GetAxisSiteCS()
                    core_site = self.event.GetGenShower().GetCoreSiteCS()
                    plt.xlabel("radius to MC shower axis [m]")
                else:
                    return None
                # noinspection PyUnboundLocalVariable
                radius = self.geo.GetStationAxisDistance(self.geo.GetRdStationPosition(station.GetId()), axis_site,
                                                         core_site)
                if not math.isnan(radius):
                    radius_array.append(radius)
                    residual_array.append(station.GetParameter(self.convert_enums.rdstQ.eTimeResidual))
                    residual_error_array.append(station.GetParameterError(self.convert_enums.rdstQ.eTimeResidual))
                    station_ids.append(station.GetId())
        if len(residual_array) == 0 or len(residual_error_array) == 0 or len(radius_array) == 0:
            return None
        plt.errorbar(radius_array, residual_array, yerr=residual_error_array, fmt="o", color="k", markersize=4,
                     zorder=1)
        plt.ylabel("time offset [ns]")
        station_identifier = plt.scatter(radius_array, residual_array, s=50, alpha=0)
        mpld3.plugins.connect(Rd_station_time_residual_plot,
                              mpld3_plugins.identify_stations(station_identifier, station_ids))

        if zoom_level is not None:
            plt.xlim([zoom_level[0], zoom_level[1]])
            plt.ylim([zoom_level[2], zoom_level[3]])

        return Rd_station_time_residual_plot

    def get_SD_event_info(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_get_SD_event_info,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_get_SD_event_info(self,tab):
        sdshower = self.event.GetSDEvent().GetSdRecShower()
        energy = sdshower.GetEnergy() / 1e12
        energy_error = sdshower.GetEnergyError() / 1e12
        azimuth = round(sdshower.GetAzimuth() * 180. / math.pi, 2)
        azimuth_error = round(sdshower.GetAzimuthError() * 180. / math.pi, 2)
        zenith = round(sdshower.GetZenith() * 180. / math.pi, 2)
        zenith_error = round(sdshower.GetZenithError() * 180. / math.pi, 2)
        yymmdd = self.event.GetSDEvent().GetYYMMDD()
        yy = yymmdd / 10000
        mm = (yymmdd - yy * 10000) / 100
        dd = yymmdd - yy * 10000 - mm * 100
        hhmmss = self.event.GetHHMMSS()
        hh = hhmmss / 10000
        m_m = (hhmmss - hh * 10000) / 100
        ss = (hhmmss - hh * 10000 - m_m * 100)
        utc_date = str(dd) + "." + str(mm) + "." + str(yy) + "  " + str(hh) + ":" + str(m_m) + ":" + str(ss)
        gps_second = self.event.GetSDEvent().GetGPSSecond()
        event_number = self.event.GetSDEvent().GetEventId()
        stations = self.event.GetSDEvent().GetNumberOfCandidates()
        reply = {
            "energy": energy,
            "energyError": energy_error,
            "azimuth": azimuth,
            "azimuthError": azimuth_error,
            "zenith": zenith,
            "zenithError": zenith_error,
            "utcDate": utc_date,
            "gpsSecond": gps_second,
            "eventNumber": event_number,
            "stations": stations
        }
        fd, path=tempfile.mkstemp()
        os.write(fd,JSON.dumps([reply,tab]))
        os.close(fd)
        vispa.remote.send_topic(self._topic+".getSdEventInfo",window_id=self._window_id,data=path)
        return

    def get_SD_ldf(self, draw_residuals=False):
        sdevent = self.event.GetSDEvent()
        sdshower = sdevent.GetSdRecShower()
        signals = []
        signals_error = []
        distances = []
        distances_error = []
        signals_saturated = []
        signals_saturated_error = []
        distances_saturated = []
        distances_saturated_error = []
        signals_rejected = []
        signals_rejected_error = []
        distances_rejected = []
        distances_rejected_error = []
        ids = []
        ids_saturated = []
        ids_rejected = []
        ldf_distances = []
        ldf_signals = []
        ldf_upper = []
        ldf_lower = []
        ldf = sdshower.GetLDF()
        x_min = 0
        x_max = 0
        y_min = 0
        y_max = 0
        if draw_residuals:
            if not sdevent.HasLDF():
                return None
            for station in sdevent.GetStationVector():
                if station.IsCandidate:
                    signal = station.GetTotalSignal() - ldf.Evaluate(station.GetSPDistance(),
                                                                     self.file_info.GetLDFType())
                    signal_error = station.GetTotalSignalError()
                    distance = station.GetSPDistance()
                    if distance == 0:
                        distance = self.geo.GetStationAxisDistance(station.GetId(), sdshower.GetAxisSiteCS(),
                                                                   sdshower.GetCoreSiteCS())
                    if station.IsLowGainSaturated():
                        signals_saturated.append(signal)
                        signals_saturated_error.append(signal_error)
                        distances_saturated.append(distance)
                        distances_saturated_error.append(station.GetSPDistanceError())
                        ids_saturated.append("station "+str(station.GetId()))
                        if signal > y_max:
                            y_max = signal
                        if signal < y_min:
                            y_min = signal
                        if station.GetSPDistance() > x_max:
                            x_max = station.GetSPDistance()
                        if station.GetSPDistance() < x_min:
                            x_min = station.GetSPDistance()
                    elif station.IsAccidental():
                        signals_rejected.append(signal)
                        signals_rejected_error.append(signal_error)
                        distances_rejected.append(distance)
                        distances_rejected_error.append(station.GetSPDistanceError())
                        ids_rejected.append("station "+str(station.GetId()))
                    else:
                        signals.append(signal)
                        signals_error.append(signal_error)
                        distances.append(distance)
                        distances_error.append(station.GetSPDistanceError())
                        ids.append("station "+str(station.GetId()))
                        if signal > y_max:
                            y_max = signal
                        if signal < y_min:
                            y_min = signal
                        if station.GetSPDistance() > x_max:
                            x_max = station.GetSPDistance()
                        if station.GetSPDistance() < x_min:
                            x_min = station.GetSPDistance()
        else:
            for station in sdevent.GetStationVector():
                distance = station.GetSPDistance()
                if distance == 0:
                    distance = self.geo.GetStationAxisDistance(station.GetId(), sdshower.GetAxisSiteCS(),
                                                               sdshower.GetCoreSiteCS())
                if station.IsCandidate:
                    signal = station.GetTotalSignal()
                    signal_error = station.GetTotalSignalError() / signal / math.log(10)
                    if station.IsLowGainSaturated():
                        signals_saturated.append(math.log10(signal))
                        signals_saturated_error.append(signal_error)
                        distances_saturated.append(distance)
                        distances_saturated_error.append(station.GetSPDistanceError())
                        ids_saturated.append("station "+str(station.GetId()))
                        if math.log10(signal) > y_max:
                            y_max = math.log10(signal)
                        if math.log10(signal) < y_min:
                            y_min = math.log10(signal)
                        if station.GetSPDistance() > x_max:
                            x_max = station.GetSPDistance()
                        if station.GetSPDistance() < x_min:
                            x_min = station.GetSPDistance()
                    elif station.IsAccidental():
                        signals_rejected.append(math.log10(signal))
                        signals_rejected_error.append(signal_error)
                        distances_rejected.append(distance)
                        distances_rejected_error.append(station.GetSPDistanceError())
                        ids_rejected.append("station "+str(station.GetId()))
                        if station.GetSPDistance() > x_max:
                            x_max = station.GetSPDistance()
                        if station.GetSPDistance() < x_min:
                            x_min = station.GetSPDistance()
                    else:
                        signals.append(math.log10(signal))
                        signals_error.append(signal_error)
                        distances.append(distance)
                        distances_error.append(station.GetSPDistanceError())
                        ids.append("station "+str(station.GetId()))
                        if math.log10(signal) > y_max:
                            y_max = math.log10(signal)
                        if math.log10(signal) < y_min:
                            y_min = math.log10(signal)
                        if station.GetSPDistance() > x_max:
                            x_max = station.GetSPDistance()
                        if station.GetSPDistance() < x_min:
                            x_min = station.GetSPDistance()


        if draw_residuals:
            for i in range(int(x_min), int(x_max) + 50, 50):
                ldf_distances.append(i)
                ldf_signals.append(0)
        else:
            max_log_signal=0
            if len(signals)>0:
                max_log_signal=max(max_log_signal,max(signals))
            if len(signals_rejected)>0:
                max_log_signal=max(max_log_signal,max(signals_rejected))
            if len(signals_saturated)>0:
                max_log_signal=max(max_log_signal,max(signals_saturated))
            for i in range(int(x_min + 1), int(x_max) + 50, 50):
                signal = ldf.Evaluate(i, self.file_info.GetLDFType())
                if signal >0 and np.log10(signal)<max_log_signal+1:
                    ldf_distances.append(i)
                    ldf_signals.append(math.log10(signal))
                    ldf_upper.append(math.log10(signal) + sdshower.SignalUncertainty() / math.sqrt(signal) / math.log(10))
                    ldf_lower.append(math.log10(signal) - sdshower.SignalUncertainty() / math.sqrt(signal) / math.log(10))
        ldf_fill_distances=ldf_distances
        ldf_fill_signal=ldf_upper
        j=len(ldf_lower)-1
        while j>=0:
            ldf_fill_distances.append(ldf_distances[j])
            ldf_fill_signal.append(ldf_lower[j])
            j-=1
        return {
            "signals":signals,
            "signals_error":signals_error,
            "distances": distances,
            "distances_error": distances_error,
            "ids": ids,
            "signals_saturated": signals_saturated,
            "signals_saturated_error": signals_saturated_error,
            "distances_saturated": distances_saturated,
            "distances_saturated_error": distances_saturated_error,
            "ids_saturated": ids_saturated,
            "signals_rejected": signals_rejected,
            "signals_rejected_error": signals_rejected_error,
            "distances_rejected": distances_rejected,
            "distances_rejected_error": distances_rejected_error,
            "ids_rejected": ids_rejected,
            "ldf_signals": ldf_signals,
            "ldf_distances": ldf_distances,
            "ldf_fill_distances": ldf_fill_distances,
            "ldf_fill_signal": ldf_fill_signal
        }

    def get_sd_station_position(self, sd_id):
        station_position = self.geo.GetStationPosition(int(sd_id))
        return JSON.dumps([station_position[0] / 1000., station_position[1] / 1000.])

    def get_SD_map(self, only_nearby=True):
        sd_event = self.event.GetSDEvent()
        shower_core = sd_event.GetSdRecShower().GetCoreSiteCS()
        signal_stations = []
        signal_station_ids=[]
        saturated_stations = []
        saturated_station_ids=[]
        rejected_stations = []
        rejected_station_ids=[]
        non_signal_stations = []
        non_signal_stations_ids=[]

        station_ids = self.geo.GetStationIds()
        station_x_positions = self.geo.GetStationXPositions()
        stations_y_positions = self.geo.GetStationYPositions()


        signal_info = []
        signal_labels=[]

        for i in range(len(station_ids)):
            current_station = sd_event.GetStationById(station_ids[i])
            core_dist = math.sqrt(
                (station_x_positions[i] - shower_core[0]) ** 2 + (stations_y_positions[i] - shower_core[1]) ** 2)
            if True:
                #if only_nearby is False or core_dist < 5000:
                if current_station is None or not current_station:
                    non_signal_stations.append(([station_x_positions[i]/1000., stations_y_positions[i]/1000.]))
                    non_signal_stations_ids.append("station "+str(station_ids[i]))
                else:
                    if current_station.IsCandidate():
                        if current_station.IsAccidental():
                            rejected_stations.append([station_x_positions[i]/1000., stations_y_positions[i]/1000.])
                            rejected_station_ids.append("station "+str(station_ids[i]))
                        elif current_station.IsLowGainSaturated():
                            saturated_stations.append([station_x_positions[i]/1000., stations_y_positions[i]/1000.])
                            saturated_station_ids.append("station "+str(station_ids[i]))
                        else:
                            signal_stations.append([station_x_positions[i]/1000., stations_y_positions[i]/1000.])
                            signal_station_ids.append("station "+str(station_ids[i]))
                    elif current_station.IsAccidental():
                        rejected_stations.append([station_x_positions[i]/1000., stations_y_positions[i]/1000.])
                        rejected_station_ids.append("station "+str(station_ids[i]))
                    else:
                        non_signal_stations.append([station_x_positions[i]/1000., stations_y_positions[i]/1000.])
                        non_signal_stations_ids.append("station "+str(station_ids[i]))
                    if current_station.GetTotalSignal()>1:
                        signal_info.append(
                            [station_x_positions[i] / 1000, stations_y_positions[i] / 1000,
                             np.log10(current_station.GetTotalSignal()),current_station.GetTimeSecond()+current_station.GetTimeNSecond()/1.e9])
                        signal_labels.append("signal: "+str(np.around(current_station.GetTotalSignal()))+"VEM")

        if len(signal_stations)>0:
            signal_stations =np.array(signal_stations).T
        else:
            signal_stations=[[],[],[]]
        if len(saturated_stations)>0:
            saturated_stations = np.array(saturated_stations).T
        else:
            saturated_stations=[[],[],[]]
        if len(rejected_stations)>0:
            rejected_stations = np.array(rejected_stations).T
        else:
            rejected_stations=[[],[],[]]
        if len(non_signal_stations)>0:
            non_signal_stations = np.array(non_signal_stations).T
        else:
            non_signal_stations=[[],[],[]]
        if len(signal_info)>0:
            signal_info=np.array(signal_info).T
            time_avg=np.average(signal_info[3])
            for i in range(signal_info[3].size):
                signal_info[3][i]=(signal_info[3][i]-time_avg)*1.e9
        else:
            signal_info=[[],[],[],[]]
        az = sd_event.GetSdRecShower().GetAzimuth()
        shower_axis_x = [shower_core[0] / 1000., shower_core[0] / 1000. + 50 * math.cos(az)]
        shower_axis_y = [shower_core[1] / 1000., shower_core[1] / 1000. + 50 * math.sin(az)]

        # draw FD and HEAT

        eye_names=[]
        eye_positions=[]
        for eye_id in self.geo.GetEyeNumbers():
            eye = self.geo.GetEye(eye_id)
            if eye_id!=5:
                eye_position = eye.GetEyePos()
                eye_positions.append([eye_position[0]/1000.,eye_position[1]/1000.])
                if eye_id!=4:
                    eye_names.append(str(eye.GetEyeName()))
                else:
                    eye_names.append("Coihueco + Heat")
            eye_phi = eye.GetEyePhiZ() * 180. / math.pi + 90.
            for tel_id in eye.GetTelescopeIDs():
                tel = eye.GetTelescope(tel_id)
                for pointing_id in tel.GetPointingIds():
                    min_phi = tel.GetPixelMinPhi(pointing_id)
                    max_phi = tel.GetPixelMaxPhi(pointing_id)
        if len(eye_positions)>0:
            eye_positions=np.array(eye_positions).T
        else:
            eye_positions=[[],[]]
        #draw_ldf
        if sd_event.HasLDF():
            ldf=sd_event.GetSdRecShower().GetLDF()
            zenith=sd_event.GetSdRecShower().GetZenith()
            azimuth=sd_event.GetSdRecShower().GetAzimuth()*180./math.pi
            colors=["green","yellow","#ff9933","red"]
            for i in range(0,4):
                r=ldf.EvaluateInverted(10.**i,self.file_info.GetLDFType())/1000.
                ellipse=mpatches.Ellipse((shower_core[0]/1000.,shower_core[1]/1000.),width=r/np.cos(zenith),height=r,angle=azimuth,fill=False,color=colors[i])
        reply={
            "signal_stations": [list(signal_stations[0]),list(signal_stations[1])],
            "signal_station_ids": signal_station_ids,
            "rejected_stations": [list(rejected_stations[0]),list(rejected_stations[1])],
            "rejected_station_ids": rejected_station_ids,
            "non_signal_stations":[list( non_signal_stations[0]),list(non_signal_stations[1])],
            "non_signal_station_ids": non_signal_stations_ids,
            "saturated_stations": [list(saturated_stations[0]),list(saturated_stations[1])],
            "saturated_station_ids": saturated_station_ids,
            "signal_info": [list(signal_info[0]),list(signal_info[1]),list(signal_info[2]),list(signal_info[3])],
            "signal_labels": signal_labels,
            "shower_axis": [shower_axis_x,shower_axis_y],
            "eye_positions": [list(eye_positions[0]),list(eye_positions[1])],
            "eye_names": eye_names
        }
        return reply

    def get_sd_stations(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_get_sd_stations,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_get_sd_stations(self,tab):
        sd_event = self.event.GetSDEvent()
        reply = []
        for station in sd_event.GetStationVector():
            reply.append({
                "id": station.GetId(),
                "signal": round(station.GetTotalSignal(), 2),
                "trigger": station.GetStationTriggerName(),
                "removalReason": station.GetRemovalReason(),
                "candidate": station.IsCandidate(),
                "rejected": station.IsAccidental(),
                "saturated": station.IsLowGainSaturated()
            })
        fd, path=tempfile.mkstemp()
        os.write(fd,JSON.dumps([reply,tab]))
        os.close(fd)
        vispa.remote.send_topic(self._topic+".getSdStations",window_id=self._window_id,data=path)
        return

    def get_sd_time_residuals(self,plane=True):
        sd_event = self.event.GetSDEvent()
        shower_core = sd_event.GetSdRecShower().GetCoreSiteCS()
        shower_axis = sd_event.GetSdRecShower().GetAxisSiteCS()

        candidates = []
        rejected = []
        saturated = []
        candidates_ids = []
        rejected_ids = []
        saturated_ids = []
        r_max = 0
        r_max_signal = 0
        y_max_signal = 0
        y_min_signal = 0

        for station in sd_event.GetStationVector():
            if station.GetSPDistance() != 0:
                dist = station.GetSPDistance()
                dist_err = station.GetSPDistanceError()
            else:
                dist = self.geo.GetStationAxisDistance(station.GetId(), shower_axis, shower_core)
                dist_err = 0.
            if plane:
                time = -station.GetPlaneTimeResidual()
                rise = station.GetRiseTime() + time
                fall = station.GetFallTime() + time
            else:
                time = -station.GetCurvatureTimeResidual()
                rise = station.GetRiseTime()
                fall = station.GetFallTime()
            time_err = math.sqrt(station.GetSignalTimeSigma2Intrinsic(sd_event.GetSdRecShower().GetZenith()))
            t50 = station.GetTime50() + time
            t50_err = station.GetTime50RMS()
            rise_err = station.GetRiseTimeRMS()
            fall_err = station.GetFallTimeRMS()
            if station.IsCandidate():
                if station.IsLowGainSaturated():
                    saturated.append([dist, dist_err, time, time_err, rise, rise_err, fall, fall_err, t50, t50_err])
                    saturated_ids.append("station "+str(station.GetId()))
                else:
                    candidates.append([dist, dist_err, time, time_err, rise, rise_err, fall, fall_err, t50, t50_err])
                    candidates_ids.append("station "+str(station.GetId()))
                    if dist > r_max_signal:
                        r_max_signal = dist
                    if time > y_max_signal:
                        y_max_signal = time + 1.5 * time_err
                    if time < y_min_signal:
                        y_min_signal = time - 1.5 * time_err
            if station.IsAccidental():
                rejected.append([dist, dist_err, time, time_err, rise, rise_err, fall, fall_err, t50, t50_err])
                rejected_ids.append("station "+str(station.GetId()))
            if dist > r_max:
                r_max = dist

        if len(saturated)>0:
            saturated=np.array(saturated).T
        else:
            saturated=[[],[],[],[],[],[],[],[],[],[]]
        if len(candidates)>0:
            candidates = np.array(candidates).T
        else:
            candidates=[[],[],[],[],[],[],[],[],[],[]]
        if len(rejected)>0:
            rejected=np.array(rejected).T
        else:
            rejected=[[],[],[],[],[],[],[],[],[],[]]

        radius_array=[]
        fit_value=[]
        fit_lower=[]
        fit_upper=[]
        fit_fill_radius=[]
        if plane:
            try:
                rc = sd_event.GetSdRecShower().GetRadiusOfCurvature()
                rc_error = sd_event.GetSdRecShower().GetRadiusOfCurvatureError()
                for r in range(0, int(r_max+100), 50):
                    rcUp = rc + rc_error
                    rcLow = rc - rc_error
                    radius_array.append(r)
                    fit_value.append((math.sqrt(r * r + rc * rc) - rc) / 2.99 * 10.)
                    fit_upper.append((math.sqrt(r * r + rcUp * rcUp) - rcUp) / 2.99 * 10.)
                    fit_lower.append((math.sqrt(r * r + rcLow * rcLow) - rcLow) / 2.99 * 10.)
                    fit_fill_radius.append(r)
                for i in range(len(radius_array)):
                    fit_fill_radius.append(radius_array[len(radius_array)-1-i])
                    fit_lower.append(fit_upper[len(fit_upper)-1-i])
            # TODO: Catch a useful exception and return exemplary a warning
            except:
                pass

        return {
            "candidates": [list(candidates[0]),list(candidates[1]),list(candidates[2]),list(candidates[3]),
                           list(candidates[4]),list(candidates[5]),list(candidates[6]),list(candidates[7]),
                           list(candidates[8]),list(candidates[9])],
            "saturated": [list(saturated[0]),list(saturated[1]),list(saturated[2]),list(saturated[3]),
                          list(saturated[4]),list(saturated[5]),list(saturated[6]),list(saturated[7]),
                          list(saturated[8]),list(saturated[9])],
            "rejected": [list(rejected[0]),list(rejected[1]),list(rejected[2]),list(rejected[3]),list(rejected[4]),
                         list(rejected[5]),list(rejected[6]),list(rejected[7]),list(rejected[8]),list(rejected[9])],
            "fit_radius": radius_array,
            "fit_value": fit_value,
            "fit_fill": fit_lower,
            "fit_fill_radius": fit_fill_radius
        }

    def get_sd_vem_trace(self, plot_width, plot_height, station_id=-1, pmt_1=True, pmt_2=False, pmt_3=False,
                         zoom_level=None):
        if self.event.GetSDEvent() is None:
            return None
        if len(self.event.GetSDEvent().GetStationVector()) == 0:
            return None
        if int(station_id) <= 0:
            station = self.event.GetSDEvent().GetStationVector()[0]
        else:
            station = self.event.GetSDEvent().GetStationById(int(station_id))
        if station is None:
            return None
        vem_trace = 0
        if pmt_1:
            vem_trace = station.GetVEMTrace(1)
        if pmt_2:
            vem_trace = station.GetVEMTrace(2)
        if pmt_3:
            vem_trace = station.GetVEMTrace(3)
        if len(vem_trace) == 0:
            return None
        plot_x = plot_width / 80.
        plot_y = plot_height / 80.
        fig = plt.figure(figsize=(plot_x, plot_y), dpi=80)
        fig.clear()
        ax = fig.add_subplot(111)
        time = range(len(vem_trace))
        ax.plot(time, vem_trace)
        plt.xlabel("t[25ns]")
        plt.ylabel("Signal [VEM peak]")
        if zoom_level is not None:
            plt.xlim([zoom_level[0], zoom_level[1]])
            plt.ylim([zoom_level[2], zoom_level[3]])

        return fig

    def get_sd_electrode_trace(self, plot_width, plot_height, station_id=-1, pmt_1=True, pmt_2=False, pmt_3=False,
                               high_gain=True, zoom_level=None):
        if self.event.GetSDEvent() is None:
            return None
        if len(self.event.GetSDEvent().GetStationVector()) == 0:
            return None
        pmt = 1  # as the default for pmt_1 is True and all others are false
        if pmt_1:
            pmt = 1
        if pmt_2:
            pmt = 2
        if pmt_3:
            pmt = 3
        if int(station_id) <= 0:
            station = self.event.GetSDEvent().GetStationVector()[0]
        else:
            station = self.event.GetSDEvent().GetStationById(int(station_id))
        if station is None:
            return None
        if high_gain:
            trace = station.GetHighGainTrace(pmt)
        else:
            trace = station.GetLowGainTrace(pmt)
        if len(trace) == 0:
            return None
        plot_x = plot_width / 80.
        plot_y = plot_height / 80.
        fig = plt.figure(figsize=(plot_x, plot_y), dpi=80)
        fig.clear()
        ax = fig.add_subplot(111)
        time = range(len(trace))
        ax.plot(time, trace)
        plt.xlabel("t[25s]")
        plt.ylabel("ADC counts")
        if zoom_level is not None:
            plt.xlim([zoom_level[0], zoom_level[1]])
            plt.ylim([zoom_level[2], zoom_level[3]])

        return fig

    def get_fd_eyes(self):
        if self.event.GetFDEvents() is None:
            return None
        reply = []
        for i in range(self.event.GetNEyes()):
            reply.append({
                "name": str(self.geo.GetEye(self.event.GetFDEvents()[i].GetEyeId()).GetEyeName()),
                "id": self.event.GetFDEvents()[i].GetEyeId()
            })
        return JSON.dumps(reply)

    def get_fd_event_info(self,*args,**kwargs):
        thread=threading.Thread(target=self._do_get_fd_event_info,args=args,kwargs=kwargs)
        thread.deamon=True
        thread.running=True
        thread.start()
        return

    def _do_get_fd_event_info(self, eye,tab):
        if len(self.event.GetFDEvents()) == 0:
            fd, path=tempfile.mkstemp()
            os.write(fd,JSON.dumps([None,tab]))
            os.close(fd)
            vispa.remote.send_topic(self._topic+".getFdEventInfo",window_id=self._window_id,data=path)
            return
        if eye == -1:
            fd_event = self.event.GetFDEvents()[0]
        else:
            fd_event = self.event.GetEye(int(eye))
        fd_shower = fd_event.GetFdRecShower()

        event_id = fd_event.GetEventId()
        event_run = fd_event.GetRunId()
        gps_second = fd_event.GetGPSSecond()
        gps_nano_second = fd_event.GetGPSNanoSecond()
        YYMMDD = fd_event.GetYYMMDD()
        YY = YYMMDD / 10000
        MM = (YYMMDD - YY * 10000) / 100
        DD = YYMMDD - YY * 10000 - MM * 100
        HHMMSS = fd_event.GetHHMMSS()
        HH = HHMMSS / 10000
        mm = (HHMMSS - HH * 10000) / 100
        SS = (HHMMSS - HH * 10000 - mm * 100)
        utc_date = str(DD) + "." + str(MM) + "." + str(YY) + "  " + str(HH) + ":" + str(mm) + ":" + str(SS)
        azimuth = round(fd_shower.GetAzimuth() * 180. / math.pi, 2)
        azimuth_error = round(fd_shower.GetAzimuthError() * 180. / math.pi, 2)
        zenith = round(fd_shower.GetZenith() * 180. / math.pi, 2)
        zenith_error = round(fd_shower.GetZenithError() * 180. / math.pi, 2)
        energy = fd_shower.GetEnergy()
        energy_error = fd_shower.GetEnergyError()
        core_x = round(fd_shower.GetCoreSiteCS()[0] / 1000., 3)
        core_y = round(fd_shower.GetCoreSiteCS()[1] / 1000., 3)
        event_type = fd_event.GetEventType()
        event_class = fd_event.GetEventClass()
        x_max = round(fd_shower.GetXmax(), 1)
        x_max_error = round(fd_shower.GetXmaxError(), 1)
        dEdX_max = round(fd_shower.GetdEdXmax(), 2)
        dedX_max_error = round(fd_shower.GetdEdXmaxError(), 2)
        eyes = []
        eye_indices = []
        for i in range(self.event.GetNEyes()):
            eyes.append(self.event.GetFDEvents()[i].GetEyeId())
            eye_indices.append(i)
        if fd_event.GetRecLevel() >= 10:
            gh_type = int(fd_shower.GetGHType())
            if gh_type == 1:
                shower_profile = "2-parameter Gaisser-Hillas"
            elif gh_type == 2:
                shower_profile = "4-parameter Gaisser-Hillas (classic)"
            elif gh_type == 3:
                shower_profile = "4-paramter Gaisser-Hillas (fwhm)"
            else:
                shower_profile = "unknown"
        else:
            shower_profile = "none"
        reply = {
            "eventId": event_id,
            "runId": event_run,
            "gpsSecond": gps_second,
            "gpsNanoSecond": gps_nano_second,
            "utcDate": utc_date,
            "azimuth": azimuth,
            "azimuthError": azimuth_error,
            "zenith": zenith,
            "zenithError": zenith_error,
            "energy": energy,
            "energyError": energy_error,
            "coreX": core_x,
            "coreY": core_y,
            "eventType": event_type,
            "eventClass": event_class,
            "xMax": x_max,
            "xMaxError": x_max_error,
            "dEdXMax": dEdX_max,
            "dEdXMaxError": dedX_max_error,
            "eyes": eyes,
            "eyeIndices": eye_indices,
            "showerProfile": shower_profile
        }
        fd, path=tempfile.mkstemp()
        os.write(fd,JSON.dumps([reply,tab]))
        os.close(fd)
        vispa.remote.send_topic(self._topic+".getFdEventInfo",window_id=self._window_id,data=path)
        return

    def get_fd_time_fit(self, plot_width, plot_height, eye=-1, draw_charge=True, draw_residuals=True, zoom_level=None):
        if len(self.event.GetFDEvents()) == 0:
            return None
        if eye == -1 or eye is None:
            fd_event = self.event.GetFDEvents()[0]
        else:
            if self.event.HasEye(int(eye)):
                fd_event = self.event.GetEye(int(eye))
            else:
                if len(self.event.GetFDEvents()) == 0:
                    return None
                else:
                    fd_event = self.event.GetFDEvents()[0]
        pixel = fd_event.GetFdRecPixel()
        if pixel.GetNumberOfPixels() == 0:
            return None
        pixels = []
        pixel_ids = []
        fd_gen = fd_event.GetGenGeometry()
        chi_min = np.amin(pixel.GetChi())
        chi_max = np.amax(pixel.GetChi())
        fit = []
        if draw_residuals is False and self.file_info.HasMC() is True:
            for j in range(100):
                chi = (chi_min + j / 100. * (chi_max - chi_min)) * 180. / math.pi
                time = fd_gen.GetRp() / 3.e1 * math.tan(
                    (fd_gen.GetChi0() - chi * math.pi / 180.) / 2.) + fd_gen.GetT0() / 100.
                fit.append([chi, time])

        for i in range(len(pixel.GetStatus())):
            if pixel.GetStatus()[i] > 1:
                chi = pixel.GetChi()[i] * 180. / math.pi
                if draw_residuals and self.file_info.HasMC():
                    time = pixel.GetTime()[i] - fd_gen.GetRp() / 3.e1 * math.tan(
                        (fd_gen.GetChi0() - chi * math.pi / 180.) / 2.) - fd_gen.GetT0() / 100.
                else:
                    time = pixel.GetTime()[i]
                time_err = pixel.GetTimeErr()[i]
                charge = math.log10(pixel.GetCharge()[i])
                pixels.append([time, time_err, chi, charge])
                pixel_ids.append(pixel.GetPixelId(i))

        plot_x = plot_width / 80.
        plot_y = plot_height / 80.
        fig = plt.figure(figsize=(plot_x, plot_y), dpi=80)
        fig.clear()
        ax = fig.add_subplot(111)

        pixels = np.array(pixels)
        fit = np.array(fit)
        if pixels.size > 0:
            ax.errorbar(pixels.T[2], pixels.T[0], yerr=pixels.T[1], fmt=" ")
            if draw_charge:
                color = pixels.T[3]
            else:
                color = pixels.T[0]
            c_min = np.amin(color)
            c_max = np.amax(color)
            sc = ax.scatter(pixels.T[2], pixels.T[0], c=color, cmap=plt.get_cmap("rainbow"), vmin=c_min, vmax=c_max,
                            s=100,
                            marker="o", alpha=.5)
            mpld3.plugins.connect(fig, mpld3_plugins.identify_stations(sc, pixel_ids))
            if draw_residuals and self.file_info.HasMC():
                ax.plot([0, chi_max * 180. / math.pi + 5], [0, 0], c=".5", linestyle="--")
            elif fit.size > 0:
                ax.plot(fit.T[0], fit.T[1])
        plt.xlabel("chi [deg]")
        plt.ylabel("time [100ns]")
        if zoom_level is not None:
            plt.xlim([zoom_level[0], zoom_level[1]])
            plt.ylim([zoom_level[2], zoom_level[3]])

        return fig

    def get_fd_shower_profile(self, plot_height, plot_width, eye=-1, use_shower_age=False, use_n_e=False,
                              draw_gaisser_hillas=True, zoom_level=None):
        if len(self.event.GetFDEvents()) == 0:
            return None
        if eye == -1 or eye is None:
            fd_event = self.event.GetFDEvents()[0]
        else:
            if self.event.HasEye(int(eye)):
                fd_event = self.event.GetEye(int(eye))
            else:
                if len(self.event.GetFDEvents()) == 0:
                    return None
                else:
                    fd_event = self.event.GetFDEvents()[0]
        fd_shower = fd_event.GetFdRecShower()

        if use_shower_age:
            x = fd_shower.GetShowerAge()
            x_label = "shower age"
        else:
            x = fd_shower.GetDepth()
            x_label = "slant depth [g/cm^2]"
        if use_n_e:
            e = fd_shower.GetElectrons()
            y = []
            for _e in e:
                y.append(_e / 1e6)
            e_err = fd_shower.GetElectronsError()
            y_err = []
            for _e_err in e_err:
                y_err.append(_e_err / 1e6)
            y_label = "N_e/1e6"
        else:
            y = fd_shower.GetEnergyDeposit()
            y_err = fd_shower.GetEnergyDepositError()
            y_label = "dE/dX [PeV/(g/cm^2)]"

        if len(x) == 0 and len(y) == 0:
            return None

        plot_x = plot_width / 80.
        plot_y = plot_height / 80.
        fig = plt.figure(figsize=(plot_x, plot_y), dpi=80)
        fig.clear()
        ax = fig.add_subplot(111)

        ax.errorbar(x, y, yerr=y_err, fmt="o")
        if draw_gaisser_hillas:
            gh_params = [fd_shower.GetXmax(), fd_shower.GetX1(), fd_shower.GetdEdXmax(), fd_shower.GetLambda()]
            gh_x = []
            gh_y = []
            kdEdXPerElectron = 2.55827e-3
            if not use_shower_age:
                for x in range(int(min(x) - 50), int(max(x) + 50)):
                    gh_x.append(x)
                    if use_n_e:
                        gh_y.append(_get_gaisser_hillas(x, gh_params) / kdEdXPerElectron)
                    else:
                        gh_y.append(_get_gaisser_hillas(x, gh_params))
                ax.plot(gh_x, gh_y, linestyle="-", c="r")
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        if zoom_level is not None:
            plt.xlim([zoom_level[0], zoom_level[1]])
            plt.ylim([zoom_level[2], zoom_level[3]])

        return fig

    def get_fd_camera(self, plot_height, plot_width, eye=-1, draw_charge=True, draw_all_pixels=True, zoom_level=None):
        if eye == -1 or eye is None:
            if len(self.event.GetFDEvents()) == 0:
                return None
            else:
                fd_event = self.event.GetFDEvents()[0]
        else:
            if self.event.HasEye(int(eye)):
                fd_event = self.event.GetEye(int(eye))
            else:
                if len(self.event.GetFDEvents()) == 0:
                    return None
                else:
                    fd_event = self.event.GetFDEvents()[0]
        eye_id = fd_event.GetEyeId()
        eye = self.geo.GetEye(fd_event.GetEyeId())
        pixels = fd_event.GetFdRecPixel()
        if pixels.GetNumberOfPixels() == 0:
            return None
        plot_x = plot_width / 80.
        plot_y = plot_height / 80.
        fig = plt.figure(figsize=(plot_x, plot_y), dpi=80)
        fig.clear()
        ax = fig.add_subplot(111)
        hex_commands = [Path.MOVETO, Path.LINETO, Path.LINETO, Path.LINETO, Path.LINETO, Path.LINETO, Path.CLOSEPOLY]
        mirrors_in_event = fd_event.GetMirrorsInEvent()
        signal_patches = []
        signal_tooltips = []
        signal_tooltip_positions = []
        signal_ids = []
        extra_tooltips = []
        extra_tooltip_positions = []
        extra_ids = []
        colors = []
        n_pix = pixels.GetNumberOfPixels()
        for i_tel in range(1, eye.GetNumberOfMirrors() + 1):
            tel = eye.GetTelescope(i_tel)
            if mirrors_in_event.TestBitNumber(i_tel):
                pointing_id = self.event.GetDetector().GetPointingId(eye_id, i_tel)
                azimuth = tel.GetAzimuth(pointing_id) * 180. / math.pi
                alpha_m = tel.GetElevation(pointing_id)
                for i in range(tel.GetNumberOfPixels()):
                    verts = _get_pixel_vertices(i, alpha_m, azimuth)
                    path = Path(verts, hex_commands)
                    patch = mpatches.PathPatch(path, facecolor="white", zorder=1)
                    ax.add_patch(patch)
        for i in range(n_pix):
            if pixels.GetStatus(i) >= 2 or draw_all_pixels:
                pix_id = pixels.GetPixelId(i)
                tel_id = pixels.GetTelescopeId(i)
                pointing_id = self.event.GetDetector().GetPointingId(eye_id, tel_id)
                azimuth = eye.GetTelescope(tel_id).GetAzimuth(pointing_id) * 180. / math.pi
                elevation = eye.GetTelescope(tel_id).GetElevation(pointing_id)
                verts = _get_pixel_vertices(pix_id - 1, elevation, azimuth)
                if pixels.GetStatus(i) > 2 and pixels.GetCharge(i) > 0:
                    if draw_charge:
                        colors.append(pixels.GetCharge(i))
                    else:
                        colors.append(pixels.GetTime(i))
                    pix_id = pixels.GetPixelId(i)
                    signal_patches.append(mpatches.PathPatch(Path(verts, hex_commands)))
                    signal_tooltips.append("pixel " + str(pix_id))
                    signal_tooltip_positions.append([verts[0][0], (verts[0][1] + verts[3][1]) / 2.])
                    signal_ids.append(pix_id)
                elif pixels.GetStatus(i) == 2 and pixels.GetCharge(i) > 0:
                    pix_id = pixels.GetPixelId(i)
                    path = Path(verts, hex_commands)
                    patch = mpatches.PathPatch(path, facecolor="black")
                    ax.add_patch(patch)
                    extra_tooltips.append("pixel " + str(pix_id))
                    extra_tooltip_positions.append([verts[0][0], (verts[0][1] + verts[3][1]) / 2.])
                    extra_ids.append(pix_id)
                elif draw_all_pixels:
                    pix_id = pixels.GetPixelId(i)
                    path = Path(verts, hex_commands)
                    patch = mpatches.PathPatch(path, facecolor="grey")
                    ax.add_patch(patch)
                    extra_tooltips.append("pixel " + str(pix_id))
                    extra_tooltip_positions.append([verts[0][0], (verts[0][1] + verts[3][1]) / 2.])
                    extra_ids.append(pix_id)

        if len(signal_patches) > 0:
            p = PatchCollection(signal_patches, cmap=plt.get_cmap("rainbow"), zorder=2)
            p.set_array(np.array(colors))
            ax.add_collection(p)
            signal_tooltip_positions = np.array(signal_tooltip_positions)
            signal_tooltip_plot = ax.scatter(signal_tooltip_positions.T[0], signal_tooltip_positions.T[1], s=100,
                                             alpha=0,
                                             zorder=3)
            mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(signal_tooltip_plot, signal_tooltips))
            mpld3.plugins.connect(fig, mpld3_plugins.identify_stations(signal_tooltip_plot, signal_ids))
        if len(extra_tooltips) > 0:
            extra_tooltip_positions = np.array(extra_tooltip_positions)
            extra_tooltip_plot = ax.scatter(extra_tooltip_positions.T[0], extra_tooltip_positions.T[1], s=100, alpha=0,
                                            zorder=3)
            mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(extra_tooltip_plot, extra_tooltips))
            mpld3.plugins.connect(fig, mpld3_plugins.identify_stations(extra_tooltip_plot, extra_ids))
        if zoom_level is not None:
            plt.xlim([zoom_level[0], zoom_level[1]])
            plt.ylim([zoom_level[2], zoom_level[3]])
        plt.xlabel("azimuth [deg]")
        plt.ylabel("elevation [deg]")
        return fig

    def get_fd_pixel_ids(self, eye=-1):
        if len(self.event.GetFDEvents()) == 0:
            return JSON.dumps(None)
        if eye == -1 or eye is None:
            fd_event = self.event.GetFDEvents()[0]
        else:
            if self.event.HasEye(int(eye)):
                fd_event = self.event.GetEye(int(eye))
            else:
                fd_event=self.event.GetFDEvents()[0]

        pixel = fd_event.GetFdRecPixel()
        ids = []
        for i in range(pixel.GetNumberOfPixels()):
            ids.append(pixel.GetPixelId(i))
        ids.sort()
        reply = []
        for fd_id in ids:
            reply.append({
                "id": fd_id
            })
        return JSON.dumps(reply)

    def get_fd_first_pixel_id(self, eye):
        if len(self.event.GetFDEvents()) == 0:
            return JSON.dumps(None)
        if eye == -1 or eye is None:
            fd_event = self.event.GetFDEvents()[0]
        else:
            if self.event.HasEye(int(eye)):
                fd_event = self.event.GetEye(int(eye))
            else:
                return None
        pixel = fd_event.GetFdRecPixel()
        fd_1_pixel_id = pixel.GetPixelId(0)
        return JSON.dumps({"id": fd_1_pixel_id})

    # noinspection PyUnboundLocalVariable
    def get_fd_pixel_histogram(self, plot_width, plot_height, eye=-1, pixel_id=-1, all_time_slots=False,
                               zoom_level=None):
        if eye == -1 or eye is None:
            if len(self.event.GetFDEvents()) == 0:
                return None
            else:
                fd_event = self.event.GetFDEvents()[0]
        else:
            if self.event.HasEye(int(eye)):
                fd_event = self.event.GetEye(int(eye))
            else:
                if len(self.event.GetFDEvents()) == 0:
                    return None
                else:
                    fd_event = self.event.GetFDEvents()[0]
        pixel = fd_event.GetFdRecPixel()
        if pixel.GetNumberOfPixels() == 0:
            return None
        plot_x = plot_width / 80.
        plot_y = plot_height / 80.
        fig = plt.figure(figsize=(plot_x, plot_y), dpi=80)
        fig.clear()
        ax = fig.add_subplot(111)
        i_pix = 0
        if pixel_id == -1:
            i_pix = 0
        else:
            for i in range(pixel.GetNumberOfPixels()):
                if pixel.GetPixelId(i) == int(pixel_id):
                    i_pix = i
        trace_0 = pixel.GetTrace(i_pix)
        tel = self.geo.GetEye(fd_event.GetEyeId()).GetTelescope(pixel.GetTelescopeId(i_pix))
        bin_width = tel.GetFADCBinning() / 100.
        bins_0 = [bin_width * n for n in range(len(trace_0))]
        if all_time_slots:
            trace = trace_0
            bins = bins_0
        else:
            i_start = pixel.GetPulseStart(i_pix)
            i_stop = pixel.GetPulseStop(i_pix)
            if i_start < i_stop:
                trace = []
                bins = []
                for i in range(i_start, i_stop):
                    trace.append(trace_0[i])
                    bins.append(bins_0[i])
            else:
                trace = trace_0
                bins = bins_0
        ax.bar(bins, trace, bin_width, zorder=2)
        bins_extended = []
        trace_extended = []
        if not all_time_slots:
            if i_start > 30:
                i_start -= 30
            if i_stop < len(trace_0) - 30:
                i_stop += 30
            for i in range(i_start, i_stop):
                trace_extended.append(trace_0[i])
                bins_extended.append(bins_0[i])
            ax.bar(bins_extended, trace_extended, fill=False, zorder=1)
        plt.xlabel("time slot [100ns]")
        plt.ylabel("detected light [photons/100ns]")

        if zoom_level is not None:
            plt.xlim([zoom_level[0], zoom_level[1]])
            plt.ylim([zoom_level[2], zoom_level[3]])
        return fig

    def get_sd_sim_particle_types(self,plot_width,plot_height,zoom_level=None,station_id=None,momentum_range=[0,1000000],max_time_diff=100):
        sd_event=self.event.GetSDEvent()
        if len(sd_event.GetSimStationVector())==0:
            return None
        if int(station_id) <= 0 or station_id is None:
            station = self.event.GetSDEvent().GetSimStationVector()[0]
        else:
            station = self.event.GetSDEvent().GetSimStationById(int(station_id))
        if station is None:
            return None
        particle_count=[0,0,0,0,0,0,0,0,0,0,0,0]
        particle_names=[" photon"," electron"," positron"," muon"," anti-muon"," tauon"," anti-tauon"," neutral pion"," + pion"," - pion"," neutron or anti-neutron"," proton or anti-proton"]
        if station.GetParticles()==None:
            return None
        reference_second=sd_event.GetSdRecShower().GetCoreTimeSecond()
        reference_nano_second=sd_event.GetSdRecShower().GetCoreTimeNanoSecond()
        gen_shower=self.event.GetGenShower()
        if reference_nano_second==0 and reference_second==0:
            reference_second=sd_event.GetStationVector()[0].GetTimeSecond()
            reference_nano_second=sd_event.GetStationVector()[0].GetTimeNSecond()
        for particle in station.GetParticles():
            momentum_abs=particle.GetMomentum().Mag()
            time_diff=gen_shower.GetCoreTimeNanoSecond()+particle.GetNSecond()-reference_nano_second+(gen_shower.GetCoreTimeSecond()+particle.GetSecond()-reference_second)*1.e9
            if abs(time_diff)<max_time_diff or max_time_diff==101:
                if momentum_abs>momentum_range[0]*1.e6 and (momentum_abs<momentum_range[1]*1.e6 or momentum_range[1]==1000000):
                    type=particle.GetType()
                    if type==22:        #photon
                        particle_count[0]+=1
                    elif type==11:      #electron
                        particle_count[1]+=1
                    elif type==-11:     #anti electron
                        particle_count[2]+=1
                    elif type==14:      #muon
                        particle_count[3]+=1
                    elif type==-14:     #anti muon
                        particle_count[4]+=1
                    elif type==15:      #tau
                        particle_count[5]+=1
                    elif type==-15:     #anti tau
                        particle_count[6]+=1
                    elif type==111:     #pi 0
                        particle_count[7]+=1
                    elif type==211:      #pi+
                        particle_count[8]+=1
                    elif type==-211:    #pi-
                        particle_count[9]+=1
                    elif abs(type)==2122: #neutron or anti neutron
                        particle_count[10]+=1
                    elif abs(type)==2212:   #proton or anti proton
                        particle_count[11]+=1
        plot_x = plot_width / 80.
        plot_y = plot_height / 80.
        fig = plt.figure(figsize=(plot_x, plot_y), dpi=80)
        fig.clear()
        ax = fig.add_subplot(111)
        ax.xaxis.grid(linestyle="dotted",alpha=.5)
        ax.set_yticks([])
        ax.barh(np.arange(.5,12.5,1),particle_count,align="center",alpha=.8)
        for i in range(len(particle_names)):
            if particle_count[i]>.75*max(particle_count):
                x_pos=.5*max(particle_count)
            else:
                x_pos=particle_count[i]+.05*max(particle_count)
            ax.annotate(particle_names[i],xy=(x_pos,i+.5),fontsize=5)
        return fig

    def get_sd_sim_particle_momentum(self,plot_width,plot_height,zoom_level=None,station_id=None,draw_photons=True,draw_electrons=True,draw_muons=True,draw_other=True,draw_contour=True,max_time_diff=100):
        sd_event=self.event.GetSDEvent()
        if len(sd_event.GetSimStationVector())==0:
            return None
        if int(station_id) <= 0 or station_id is None:
            station = self.event.GetSDEvent().GetSimStationVector()[0]
        else:
            station = self.event.GetSDEvent().GetSimStationById(int(station_id))
        if station is None:
            return None
        if station.GetParticles()==None:
            return None
        momentum_array=[]
        photon_momentum=[]
        electron_momentum=[]
        muon_momentum=[]
        other_momentum=[]
        reference_second=sd_event.GetSdRecShower().GetCoreTimeSecond()
        reference_nano_second=sd_event.GetSdRecShower().GetCoreTimeNanoSecond()
        gen_shower=self.event.GetGenShower()
        if reference_nano_second==0 and reference_second==0:
            reference_second=sd_event.GetStationVector()[0].GetTimeSecond()
            reference_nano_second=sd_event.GetStationVector()[0].GetTimeNSecond()

        for particle in station.GetParticles():
            time_diff=gen_shower.GetCoreTimeNanoSecond()+particle.GetNSecond()-reference_nano_second+(gen_shower.GetCoreTimeSecond()+particle.GetSecond()-reference_second)*1.e9
            if abs(time_diff)<max_time_diff or max_time_diff==101:
                momentum=particle.GetMomentum()
                momentum_abs=np.log10(np.sqrt(momentum[0]**2+momentum[1]**2+momentum[2]**2))-6.
                momentum_array.append(momentum_abs)
                type=particle.GetType()
                if type==22:
                    if draw_photons:
                        photon_momentum.append(momentum_abs)
                elif abs(type)==11:
                    if draw_electrons:
                        electron_momentum.append(momentum_abs)
                elif abs(type)==14:
                    if draw_muons:
                        muon_momentum.append(momentum_abs)
                else:
                    if draw_other:
                        other_momentum.append(momentum_abs)
        plot_x = plot_width / 80.
        plot_y = plot_height / 80.
        fig = plt.figure(figsize=(plot_x, plot_y), dpi=80)
        fig.clear()
        ax = fig.add_subplot(111)
        if draw_contour:
            histtype="step"
            alpha=1
        else:
            histtype="stepfilled"
            alpha=.5
        if len(photon_momentum)>0:
            ax.hist(photon_momentum,bins=np.arange(0,6,.2),facecolor="yellow",edgecolor="yellow",histtype=histtype,alpha=alpha,label="photons")
        if len(electron_momentum)>0:
            ax.hist(electron_momentum,bins=np.arange(0,6,.2),facecolor="red",edgecolor="red",histtype=histtype,alpha=alpha,label="electrons")
        if len(muon_momentum)>0:
            ax.hist(muon_momentum,bins=np.arange(0,6,.2),facecolor="green",edgecolor="green",histtype=histtype,alpha=alpha,label="muons")
        if len(other_momentum)>0:
            ax.hist(other_momentum,bins=np.arange(0,6,.2),facecolor="blue",edgecolor="blue",histtype=histtype,alpha=alpha,label="other")
        ax.hist(momentum_array,bins=np.arange(0,6,.2),edgecolor="k",histtype="step",label="all",linestyle="dashed")
        plt.legend(loc="best",framealpha=0).set_title("")
        ax.grid(linestyle="dotted",alpha=.5)
        ax.set_xlabel("log10(p/MeV)")
        ax.set_ylabel("entries")
        return fig

    def get_sd_sim_particle_time_momentum_distribution(self,plot_width,plot_height,zoom_level=None,station_id=None,draw_photons=True,draw_electrons=True,draw_muons=True,draw_other=True,max_time_diff=100):
        sd_event=self.event.GetSDEvent()
        if len(sd_event.GetSimStationVector())==0:
            return None
        if int(station_id) <= 0 or station_id is None:
            station = self.event.GetSDEvent().GetSimStationVector()[0]
        else:
            station = self.event.GetSDEvent().GetSimStationById(int(station_id))
        if station is None:
            return None
        if station.GetParticles()==None:
            return None
        reference_second=sd_event.GetSdRecShower().GetCoreTimeSecond()
        reference_nano_second=sd_event.GetSdRecShower().GetCoreTimeNanoSecond()
        if reference_nano_second==0 and reference_second==0:
            reference_second=sd_event.GetStationVector()[0].GetTimeSecond()
            reference_nano_second=sd_event.GetStationVector()[0].GetTimeNSecond()
        momentum_array=[]
        time_array=[]
        gen_shower=self.event.GetGenShower()
        for particle in station.GetParticles():
            type=particle.GetType()
            time=gen_shower.GetCoreTimeNanoSecond()+particle.GetNSecond()-reference_nano_second+(gen_shower.GetCoreTimeSecond()+particle.GetSecond()-reference_second)*1.e9
            if ((type==22 and draw_photons) or (abs(type)==11 and draw_electrons) or (abs(type)==14 and draw_muons) or (type!=22 and abs(type)!=11 and abs(type)!=14 and draw_other)) and (abs(time)<max_time_diff or max_time_diff==101):
                momentum=particle.GetMomentum().Mag()
                momentum_abs=np.log10(momentum)-6.
                momentum_array.append(momentum_abs)
                time_array.append(time)
        plot_x = plot_width / 80.
        plot_y = plot_height / 80.
        fig = plt.figure(figsize=(plot_x, plot_y), dpi=80)
        fig.clear()
        ax = fig.add_subplot(111)

        ax.scatter(time_array,momentum_array,alpha=.5,color="blue")
        ax.set_xlabel("t [ns]")
        ax.set_ylabel("log10(p/MeV)")
        return fig

    def get_incoming_directions(self,galactic_coordinates=True, horizon_coordinates=False,equatorial_coordinates=False, sd=True,rd=False):
        if not sd and not rd:
            return None
        sd_theta=[]
        sd_r=[]
        rd_r=[]
        rd_theta=[]
        if horizon_coordinates:
            coordinates="hor"
            for i in range(self.file.GetNEvents()):
                self.file.ReadEvent(i)
                if sd:
                    sd_shower=self.event.GetSDEvent().GetSdRecShower()
                    a=sd_shower.GetAzimuth()*180./np.pi
                    z=sd_shower.GetZenith()*180./np.pi
                    if not np.isnan(a) and not np.isnan(z):
                        sd_theta.append(a)
                        sd_r.append(z)
                if rd:
                    rd_shower=self.event.GetRdEvent().GetRdRecShower()
                    a=rd_shower.GetAzimuth()*180./np.pi
                    z=rd_shower.GetZenith()*180./np.pi
                    if not np.isnan(a) and not np.isnan(z):
                        rd_theta.append(a)
                        rd_r.append(z)
        elif galactic_coordinates:
            coordinates="gal"
            for i in range(self.file.GetNEvents()):
                self.file.ReadEvent(i)
                if sd:
                    sd_shower=self.event.GetSDEvent().GetSdRecShower()
                    a=sd_shower.GetGalacticLatitude()*180./np.pi
                    z=sd_shower.GetGalacticLongitude()*180./np.pi
                    if not np.isnan(a) and not np.isnan(z):
                        sd_theta.append(a)
                        sd_r.append(z)
                if rd:
                    rd_shower=self.event.GetRdEvent().GetRdRecShower()
                    a=rd_shower.GetGalacticLatitude()*180./np.pi
                    z=rd_shower.GetGalacticLongitude()*180./np.pi
                    if not np.isnan(a) and not np.isnan(z):
                        rd_theta.append(a)
                        rd_r.append(z)
        reply={
            "sd_r": sd_r,
            "sd_theta": sd_theta,
            "rd_r": rd_r,
            "rd_theta": rd_theta,
            "coordinates": coordinates
        }
        self.file.ReadEvent(self.__event_number)
        return reply

    def get_event_display(self):
        sd_event=self.event.GetSDEvent()
        station_ids=self.geo.GetStationIds()
        station_x_positions = self.geo.GetStationXPositions()
        station_y_positions = self.geo.GetStationYPositions()
        station_z_positions = self.geo.GetStationZPositions()
        non_signal_x=[]
        non_signal_y=[]
        non_signal_z=[]
        non_signal_names=[]
        candidate_x=[]
        candidate_y=[]
        candidate_z=[]
        candidate_signal=[]
        candidate_names=[]
        shower_core=sd_event.GetSdRecShower().GetCoreSiteCS()
        for i in range(len(station_ids)):
            current_station = sd_event.GetStationById(station_ids[i])
            core_dist = math.sqrt(
                (station_x_positions[i] - shower_core[0]) ** 2 + (station_y_positions[i] - shower_core[1]) ** 2)
            if core_dist < 10000:
                non_signal_x.append(station_x_positions[i]/1000.)
                non_signal_y.append(station_y_positions[i]/1000.)
                non_signal_z.append(station_z_positions[i]/1000.)
                non_signal_names.append("station "+str(station_ids[i]))

                if current_station is not None and current_station:
                    if current_station.IsCandidate() and current_station.GetTotalSignal()>1:
                        candidate_x.append(station_x_positions[i]/1000.)
                        candidate_y.append(station_y_positions[i]/1000.)
                        candidate_z.append(station_z_positions[i]/1000.)
                        candidate_signal.append(np.log10(current_station.GetTotalSignal()))
                        candidate_names.append("station "+str(station_ids[i]))
        az = sd_event.GetSdRecShower().GetAzimuth()
        zen=sd_event.GetSdRecShower().GetZenith()
        shower_axis_x = [shower_core[0] / 1000., shower_core[0] / 1000. + 10. * math.cos(az)*np.sin(zen)]
        shower_axis_y = [shower_core[1] / 1000., shower_core[1] / 1000. + 10. * math.sin(az)*np.sin(zen)]
        shower_axis_z=[shower_core[2]/1000.,shower_core[2]/1000.+5.*math.cos(zen)]
        eye_names=[]
        eye_x=[]
        eye_y=[]
        eye_z=[]
        fd_lines_x=[]
        fd_lines_y=[]
        fd_lines_z=[]
        fov_names=[]
        for eye_id in self.geo.GetEyeNumbers():
            eye = self.geo.GetEye(eye_id)
            eye_position = eye.GetEyePos()
            if np.sqrt((eye_position[0]-shower_core[0])**2+(eye_position[1]-shower_core[1])**2)<20000:
                if eye_id!=5:
                    eye_x.append(eye_position[0]/1000.)
                    eye_y.append(eye_position[1]/1000.)
                    eye_z.append(eye_position[2]/1000.)
                    if eye_id!=4:
                        eye_names.append(str(eye.GetEyeName()))
                    else:
                        eye_names.append("Coihueco + Heat")
                eye_phi = eye.GetEyePhiZ()+np.pi/2.
                eye_lines_x=[eye_position[0]/1000.]
                eye_lines_y=[eye_position[1]/1000.]
                eye_lines_z=[eye_position[2]/1000.]
                fov_names.append(str(eye.GetEyeName())+" field of view")
                for tel_id in eye.GetTelescopeIDs():
                    tel = eye.GetTelescope(tel_id)
                    for pointing_id in tel.GetPointingIds():
                        min_phi = tel.GetPixelMinPhi(pointing_id)
                        max_phi = tel.GetPixelMaxPhi(pointing_id)
                        el=tel.GetElevation(pointing_id)
                        eye_lines_x.append(eye_position[0]/1000.+np.cos(eye_phi+min_phi/180.*np.pi)*np.cos(el)*5.)
                        eye_lines_x.append(eye_position[0]/1000.+np.cos(eye_phi+max_phi/180.*np.pi)*np.cos(el)*5.)
                        eye_lines_x.append(eye_position[0]/1000.)
                        eye_lines_y.append(eye_position[1]/1000.+np.sin(eye_phi+min_phi/180.*np.pi)*np.cos(el)*5.)
                        eye_lines_y.append(eye_position[1]/1000.+np.sin(eye_phi+max_phi/180.*np.pi)*np.cos(el)*5.)
                        eye_lines_y.append(eye_position[1]/1000.)
                        eye_lines_z.append(eye_position[2]/1000.+np.sin(el)*5.)
                        eye_lines_z.append(eye_position[2]/1000.+np.sin(el)*5.)
                        eye_lines_z.append(eye_position[2]/1000.)
                fd_lines_x.append(eye_lines_x)
                fd_lines_y.append(eye_lines_y)
                fd_lines_z.append(eye_lines_z)
        x_length=max([max(non_signal_x),max(eye_x)])-min([min(non_signal_x),min(eye_x)])
        y_length=max([max(non_signal_y),max(eye_y)])-min([min(non_signal_y),min(eye_y)])
        axis_length=(max(y_length,x_length))
        x_center=(max([max(non_signal_x),max(eye_x)])+min([min(non_signal_x),min(eye_x)]))/2.
        y_center=(max([max(non_signal_y),max(eye_y)])+min([min(non_signal_y),min(eye_y)]))/2.
        reply={
            "non_signal_x": non_signal_x,
            "non_signal_y": non_signal_y,
            "non_signal_z": non_signal_z,
            "non_signal_names": non_signal_names,
            "candidate_x": candidate_x,
            "candidate_y": candidate_y,
            "candidate_z": candidate_z,
            "candidate_signal": candidate_signal,
            "candidate_names": candidate_names,
            "shower_axis_x": shower_axis_x,
            "shower_axis_y": shower_axis_y,
            "shower_axis_z": shower_axis_z,
            "eye_x": eye_x,
            "eye_y": eye_y,
            "eye_z": eye_z,
            "eye_names": eye_names,
            "fd_lines_x": fd_lines_x,
            "fd_lines_y": fd_lines_y,
            "fd_lines_z": fd_lines_z,
            "fov_names": fov_names,
            "axis_length": axis_length,
            "x_center": x_center,
            "y_center": y_center
        }
        return reply
