from mpld3 import plugins
from mpld3 import utils
import logging

logger = logging.getLogger(__name__)


class identify_stations(plugins.PluginBase):
    def __init__(self, points, station_numbers):
        self.dict_ = {"type": "identifyStations",
                      "id": [utils.get_id(points), station_numbers]}
        return
